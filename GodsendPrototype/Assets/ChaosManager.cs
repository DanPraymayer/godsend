﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaosManager : MonoBehaviour
{

    float chaosMeter;
    public float ChaosMeter
    {
        get { return chaosMeter; }
        set
        {
            chaosMeter = Mathf.Clamp(value, 0, 100);
            if (chaosMeterBar != null)
                chaosMeterBar.fillAmount = chaosMeter / 100;
            ChaosTier = (int)((chaosMeter - chaosMeter % 10) / 10);
        }
    }
    int chaosTier;
    public int ChaosTier
    {
        get { return chaosTier; }
        set
        {
            if (chaosTier < value && ChaosTierIncreased != null)
                ChaosTierIncreased();

            chaosTier = value;
        }
    }
    public delegate void ChaosEvent();
    public event ChaosEvent ChaosTierIncreased;

    public UnityEngine.UI.Image chaosMeterBar;

    public bool debug_demoMeter;

    void Start()
    {
        if (debug_demoMeter)
            InvokeRepeating("autoIncrMeter", 1, 1);
    }

    void autoIncrMeter()
    {
        ChaosMeter += 1;
    }
}
