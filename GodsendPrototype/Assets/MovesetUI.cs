﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EnemyGangAffiliations
{
    none,
    green,
    orange,
    pink
}

public class MovesetUI : MonoBehaviour
{
    public Image health, armour;
    public GameObject armorStuff;

    public BodyType abilitySource;
    public MovesetUIIcon AbilityStandard;
    public MovesetUIIcon AbilityDown;

    public EnemyGangAffiliations gangAffilliation;
    public Image FactionIcon;

    public Sprite tricksterNorm, gruntNorm, hamNorm, hamDown, dashNorm, throwNorm, throwDown;
    public Sprite factGreen, factOrange, factPink;

    public void SetBodyInfo(CharacterMoveset body)
    {
        SetBody(body.CharacterBody.bodyType);
        SetGang(body.CharacterBody.EnemyGangAfiliation);
    }

    public void SetGang(EnemyGangAffiliations source)
    {
        gangAffilliation = source;
        switch (source)
        {
            case (EnemyGangAffiliations.orange):
                //hammer
                FactionIcon.color = Color.white;
                FactionIcon.sprite = factOrange;
                break;
            case (EnemyGangAffiliations.pink):
                //dash
                FactionIcon.color = Color.white;
                FactionIcon.sprite = factPink;
                break;
            case (EnemyGangAffiliations.green):
                //bomb
                FactionIcon.color = Color.white;
                FactionIcon.sprite = factGreen;
                break;
            default:
                FactionIcon.color = Color.clear;
                break;
        }
    }

    public void SetBody(BodyType source)
    {
        abilitySource = source;

        switch (source)
        {
            case (BodyType.Player):
                SetArmor(false);
                AbilityStandard.SetBodyAbilities(tricksterNorm);
                AbilityDown.SetUIIconInactive();
                break;
            case (BodyType.Grunt):
                SetArmor(true);
                AbilityStandard.SetBodyAbilities(gruntNorm);
                AbilityDown.SetUIIconInactive();
                break;
            case (BodyType.Hammer):
                SetArmor(true);
                AbilityStandard.SetBodyAbilities(hamNorm);
                AbilityDown.SetBodyAbilities(hamDown);
                break;
            case (BodyType.Dasher):
                SetArmor(true);
                AbilityStandard.SetBodyAbilities(dashNorm);
                AbilityDown.SetUIIconInactive();
                break;
            case (BodyType.Thrower):
                SetArmor(true);
                AbilityStandard.SetBodyAbilities(throwNorm);
                AbilityDown.SetBodyAbilities(throwDown, false, "X");
                break;
            default:
                SetArmor(false);
                AbilityStandard.SetUIIconInactive();
                AbilityDown.SetUIIconInactive();
                break;
        }
    }

    void SetArmor(bool active)
    {
        if (armorStuff != null)
            armorStuff.SetActive(active);
        if (armour != null)
            armour.fillAmount = 1;
    }
}
