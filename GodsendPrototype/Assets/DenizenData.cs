﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DenizenData", menuName = "Scriptable Objects/DenizenData")]
public class DenizenData : ScriptableObject
{
    public DenizenInfo[] Denizens;
}

[System.Serializable]
public class DenizenInfo
{
    public string usage;
    public DenizenType denizenType;
    public Material denizenMat;
}

[System.Flags]
public enum DenizenType
{
    none = 1 << 0,
    Player = 1 << 1,
    square = 1 << 2,
    circle = 1 << 3,
    triangle = 1 << 4,
    diamond = 1 << 5,
    D4 = 1 << 6,
    D6 = 1 << 7,
    D8 = 1 << 8,
    D10 = 1 << 9,
    D12 = 1 << 10,
    D20 = 1 << 11,
    D100 = 1 << 12,
    red = 1 << 13,
    orange = 1 << 14,
    yellow = 1 << 15,
    green = 1 << 16,
    blue = 1 << 17,
    indigo = 1 << 18,
    violet = 1 << 19,
    white = 1 << 20,
    grey = 1 << 21,
    beige = 1 << 22,
    black = 1 << 23,
    fushia = 1 << 24
}



