﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour {

    public static SpawnPointManager SPM;
    public bool useDebugStartPoint;
    public Transform startPoint, debugStartPoint;
    public List<Transform> spawnPoints = new List<Transform>();

    private void Awake()
    {
        if (SPM != null)
        {
            Debug.LogWarning("duplicate manager detected");
            this.enabled = false;
        }
        else
            SPM = this;
    }

    void Start () {
        if (spawnPoints.Count == 0)
            spawnPoints.Add(transform);
	}

    public void RequestSpawnPoint(CharacterController root)
    {
        if (useDebugStartPoint)
        {
            //spawn player at "debug start point"
            if(debugStartPoint!=null)
                root.transform.position = debugStartPoint.position;
        }
        else
        {
            if (debugStartPoint != null)
                root.transform.position = startPoint.position; //spawn player at "start point"
            else
                root.transform.position = spawnPoints[0].position; //spawn player at the first spawn point in the list
        }

        root.Respawn();
    }

    public void RequestRespawnPoint(CharacterController root)
    {
        root.transform.position = spawnPoints[0].position;
        root.Respawn();
    }
}
