﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class CommandManager {

    PlayerCommand delayedCommand;

    List<PlayerCommand> pressedCommands;

    Dictionary<PlayerCommand, PlayerCommand> lockedCommands;

    List<PlayerCommand> commands;
    List<PlayerCommand> chords;
    List<PlayerCommand> sequences;
    List<PlayerCommand> combos;

    protected CommandManager()
    {
        commands = new List<PlayerCommand>();
        chords = new List<PlayerCommand>();
        sequences = new List<PlayerCommand>();
        combos = new List<PlayerCommand>();

        pressedCommands = new List<PlayerCommand>();


        lockedCommands = new Dictionary<PlayerCommand, PlayerCommand>();
    }

    public void Update()
    {
        UpdateStates();
        CheckForCommandsForLocks();
        UpdateLockedCommands();
    }

    void UpdateStates()
    {
        UpdatePlayersCommands(commands);
        UpdatePlayersCommands(chords);
        UpdatePlayersCommands(sequences);
        UpdatePlayersCommands(combos);
    }

    void UpdatePlayersCommands(List<PlayerCommand> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].UpdateState();

            if (CheckIfCommandIsPressed(list[i]))
                pressedCommands.Add(list[i]);
            else
                pressedCommands.Remove(list[i]);
        }
    }



    bool CheckIfCommandIsPressed(PlayerCommand command)
    {
        return command.ReturnAttachedCommand().IsPressed && !pressedCommands.Contains(command);
    }

    void CheckForCommandsForLocks()
    {
        CheckForPotentialLockedCommands(chords);
        CheckForPriorityLocks(chords);
    }

    void CheckForPotentialLockedCommands(List<PlayerCommand> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            for (int j = 0; j < pressedCommands.Count; j++)
            {

                if (list[i].ContainsCommand(pressedCommands[j].ReturnAttachedCommand()) && !list[i].TimeOut)
                {
                    AddToLockedCommands(pressedCommands[j], list[i]);
                }
                    
            }    
        }
    }

    void CheckForPriorityLocks(List<PlayerCommand> list)
    {
       int higestPriority = -1;

        for (int i = list.Count - 1; i >= 0; i--)
        {
            if ((higestPriority == -1 && list[i].IsPressed) || higestPriority != -1 && (list[i].IsPressed && list[i].CommandCount >= list[higestPriority].CommandCount && list[i].Priority >= list[higestPriority].Priority))
            {
                higestPriority = i;
            }
            else
                list[i].Locked = false;
        }
        if(higestPriority != -1)
        {
            for (int i = 0; i < list.Count; i++)
            {              
                list[i].Locked = true;
            }

            list[higestPriority].Locked = false;
        }
    }

    void AddToLockedCommands(PlayerCommand newLockedCommand, PlayerCommand newLockerCommand)
    {

        if (!lockedCommands.ContainsKey(newLockedCommand))
            lockedCommands.Add(newLockedCommand, newLockerCommand);
        else
        {
            lockedCommands[newLockedCommand] = newLockerCommand;    
        }
    }

    void UpdateLockedCommands()
    {
        List<PlayerCommand> toRemove = new List<PlayerCommand>();

        foreach (var pair in lockedCommands)
        {
            if (pair.Value.IsPressed || !pair.Value.TimeOut)
            {
                pair.Key.Locked = true;
            }
            else
            {
                pair.Key.StartUnlockTimer();
                toRemove.Add(pair.Key);
            }
        }

        for (int i = 0; i < toRemove.Count; i++)
        {
            lockedCommands.Remove(toRemove[i]);
        }
    }

    public PlayerCommand CreateNewCommand()
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Command());
        commands.Add(playerCommand);

        return playerCommand;
    }

    public PlayerCommand CreateNewAxisCommand()
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new AxisCommand());
        commands.Add(playerCommand);

        return playerCommand;
    }

    public PlayerCommand CreateNewChord()
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Chord());
        chords.Add(playerCommand);

        return playerCommand;
    }
    public PlayerCommand CreateNewChord(float bufferTime)
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Chord(bufferTime));
        chords.Add(playerCommand);

        return playerCommand;
    }

    public PlayerCommand CreateNewSequence()
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Sequence());
        sequences.Add(playerCommand);

        return playerCommand;
    }
    public PlayerCommand CreateNewSequence(float bufferTime)
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Sequence(bufferTime));
        sequences.Add(playerCommand);

        return playerCommand;
    }

    public PlayerCommand CreateNewCombo()
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Combo());
        combos.Add(playerCommand);

        return playerCommand;
    }
    public PlayerCommand CreateNewCombo(float bufferTime)
    {
        PlayerCommand playerCommand = new PlayerCommand();

        playerCommand.SetupNewPlayerCommand(new Combo());
        combos.Add(playerCommand);

        return playerCommand;
    }
}
