﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using System;

public class PlayerCommand : IInputState, IAddCommands, ILockCommands, IHandledMultiInput{

    IInputState inputCommand;
    IAddCommands commandAdder;
    IHandledMultiInput mutliInput;


    public bool IsPressed { get { return inputCommand.IsPressed && !isLocked; } }
    public bool WasPressed { get { return inputCommand.WasPressed && !isLocked; } }
    public bool WasReleased { get { return inputCommand.WasReleased && !isLocked; } }
    public bool HasChanged { get { return inputCommand.WasReleased && !isLocked; } }

    public bool TimeOut { get { return mutliInput.TimeOut; } }
    public bool Reset { get { return mutliInput.Reset; } }
    public int CommandCount{ get { return mutliInput.CommandCount; } }

    bool isLocked;
    public bool Locked { set { isLocked = value; } get { return isLocked; } }

    int priority;
    public int Priority { set { priority = value; } get { return priority; } }

    bool startUnlock;

    float unlockTimer;
    float unlockDelay = .01f;


    public void UpdateState()
    {
        inputCommand.UpdateState();

        if (startUnlock)
            UnlockTimer();
    }

    public void AddInputBinding(IInputControl newInput)
    {
        commandAdder.AddInputBinding(newInput);
    }

    public void AddAxisBinding(OneAxisInputControl newInput)
    {
        commandAdder.AddAxisBinding(newInput);
    }

    public virtual void AddCommandBinding(IInputState binding)
    {
        commandAdder.AddCommandBinding(binding);
    }

    public virtual void AddCommandBinding(PlayerCommand binding)
    {
        commandAdder.AddCommandBinding(binding.ReturnAttachedCommand());
    }

    public void FireCommand()
    {
        inputCommand.FireCommand();
    }

    public void SetupNewPlayerCommand(Command newCommand)
    {
        inputCommand = newCommand as IInputState;
        commandAdder = newCommand as IAddCommands;
        mutliInput = newCommand as IHandledMultiInput;

    }

    public IInputState ReturnAttachedCommand()
    {
        return inputCommand;
    }

    public bool ContainsCommand(IInputState command)
    {
        return inputCommand.ContainsCommand(command);
    }

    public void StartUnlockTimer()
    {
        unlockTimer = unlockDelay;
        startUnlock = true;
    }

    void UnlockTimer()
    {
        unlockTimer -= Time.deltaTime;
        if (unlockTimer < 0)
        {
            isLocked = false;
            startUnlock = false;
        }
            
    }



}
