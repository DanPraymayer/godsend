﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using System;

public class AxisCommand : Command {

    OneAxisInputControl mappedAxis;

    float pressThershold = .8f;

    protected override InputState ReturnMappedButtonState()
    {

        if (mappedAxis.Value >= pressThershold && currentState == InputState.Released)
            return InputState.JustPressed;

        else if (mappedAxis.Value < pressThershold && currentState == InputState.Pressed)
            return InputState.JustReleased;

        if (lastState == InputState.JustPressed && currentState == InputState.JustPressed)
            return InputState.Pressed;

        if (lastState == InputState.JustReleased && currentState == InputState.JustReleased)
            return InputState.Released;


        return currentState;
    }
    public override void AddAxisBinding(OneAxisInputControl newInput)
    {
        mappedAxis = newInput;
    }
}
