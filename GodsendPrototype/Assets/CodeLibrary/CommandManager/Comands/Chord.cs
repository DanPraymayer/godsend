﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chord : Command, IHandledMultiInput {

    protected List<IInputState> commandBindings = new List<IInputState>();

    public float completionTimer;
    protected float completionTime = 0.12f;

    protected bool timedOut = true;
    protected bool commandReset = true;

    public bool TimeOut { get { return timedOut; } }
    public bool Reset { get { return commandReset; } }
    public int CommandCount { get { return commandBindings.Count; } }


    public Chord() { }

    public Chord(float completionTime)
    {
        this.completionTime = completionTime;
    }

    public Chord(float completionTime, List<IInputState> bindings)
    {
        SetCompletionTime(completionTime);
        commandBindings = bindings;
    }

    public void SetCompletionTime(float completionTime)
    {
        this.completionTime = completionTime;
    }

    public override void AddCommandBinding(IInputState binding)
    {
        commandBindings.Add(binding);
    }

    protected override InputState ReturnMappedButtonState()
    {

        if (commandReset && CheckIfAnyCommandsWerePressed())
            StartCompletionCountdown();

        if (!timedOut)
            DecreaseCompletionCountdown();

        if (CheckIfAllCommandsAreReleased())
            ResetCommand();

        if (CheckIfAnyCommandsHasChanged())
        {
            if (!timedOut && CheckIfAllCommandsArePressed())
            {
                return InputState.JustPressed;
            }
            else if (lastState == InputState.Pressed && CheckIfAnyCommandsAreReleased())
            {
                return InputState.JustReleased;
            }
        }
        if (lastState == InputState.JustPressed && currentState == InputState.JustPressed)
            return InputState.Pressed;

        if (lastState == InputState.JustReleased && currentState == InputState.JustReleased)
            return InputState.Released;

        return currentState;
    }
   
    bool CheckIfAnyCommandsHasChanged()
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (commandBindings[i].HasChanged)
                return true;
        }

        return false;
    }

    bool CheckIfAnyCommandsAreReleased()
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (commandBindings[i].WasReleased)
                return true;
        }

        return false;
    }

    bool CheckIfAllCommandsArePressed()
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (!commandBindings[i].IsPressed)
                return false;
        }

        return true;
    }

    bool CheckIfAnyCommandsWerePressed()
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (commandBindings[i].WasPressed)
                return true;
        }

        return false;
    }

    bool CheckIfAllCommandsAreReleased()
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (commandBindings[i].IsPressed)
                return false;
        }

        return true;
    }

    virtual protected void ResetCommand()
    {
        commandReset = true;
    }

    protected void StartCompletionCountdown()
    {
        timedOut = false;
        completionTimer = 0;
    }

    protected void DecreaseCompletionCountdown()
    {
        completionTimer += Time.deltaTime;
        if (completionTimer >= completionTime && !IsPressed)
            EndCompletionTimer();
    }

    protected void EndCompletionTimer()
    {
        timedOut = true;
        commandReset = false;
    }

    public override bool ContainsCommand(IInputState command)
    {
        for (int i = 0; i < commandBindings.Count; i++)
        {
            if (commandBindings.Contains(command))
                return true;

        }
        return false;
    }

}
