﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum SequenceProgress
{
    //NotStarted,
    WaitingForPress,
    WaitingForRelease,
    Completed
}

public class Sequence : Chord {

    SequenceProgress progress;

    public float bufferTimer;
    protected float bufferTime = .2f;
    bool buffering = false;


    public int currentCommand = 0;
    public int previousCommand = 0;

    public Sequence() { }

    public Sequence(string name)
    {

    }

    public Sequence(float completionTime)
    {
        SetCompletionTime(completionTime);
    }

    public Sequence(float completionTime, float bufferTime)
    {
        SetCompletionTime(completionTime);
        SetBufferTime(bufferTime);
    }

    public Sequence(float completionTime, List<IInputState> bindings)
    {
        SetCompletionTime(completionTime);
        commandBindings = bindings;

    }

    public Sequence(float completionTime, float bufferTime, List<IInputState> bindings)
    {
        SetCompletionTime(completionTime);
        SetBufferTime(bufferTime);
        commandBindings = bindings;

    }

    void SetBufferTime(float bufferTime)
    {
        this.bufferTime = bufferTime;
    }

    override protected InputState ReturnMappedButtonState()
    {

        if (commandReset && CheckIfFirstCommandWasPressed())
        {
            commandReset = false;
            progress = SequenceProgress.WaitingForPress;
            StartCompletionCountdown();
        }
        if (!timedOut)
            DecreaseCompletionCountdown();
        else
            ResetCommand();

        if(buffering && CheckIfCommandAtIndexHasChanged(Mathf.Clamp(currentCommand + 1, 0, commandBindings.Count - 1)))
        {
            currentCommand = Mathf.Clamp(currentCommand + 1, 0, commandBindings.Count - 1);
            progress = SequenceProgress.WaitingForRelease;
        }

        if (buffering)
            DecreaseBufferCountdown();



        if (CheckIfCommandAtIndexHasChanged(currentCommand))
        {

            if(CheckIfCommandAtIndexIsPressed(currentCommand))
            {
                if (currentCommand != commandBindings.Count - 1)
                {
                    progress = SequenceProgress.WaitingForRelease;
                    StartBufferCountdown();
                }
                else if(!timedOut && currentCommand == commandBindings.Count - 1 && previousCommand == currentCommand)
                {
                    progress = SequenceProgress.Completed;
                    return InputState.JustPressed;
                }
            }
            else if (!CheckIfCommandAtIndexIsPressed(currentCommand))
            {
                if (progress == SequenceProgress.WaitingForRelease)
                {
                    currentCommand = Mathf.Clamp(currentCommand + 1, 0, commandBindings.Count - 1);
                    
                    progress = SequenceProgress.WaitingForPress;
                }
                else if (progress == SequenceProgress.Completed)
                {
                    return InputState.JustReleased;
                }
            }
        }

        if (CheckIfCommandAtIndexHasChanged(previousCommand) && !CheckIfCommandAtIndexIsPressed(previousCommand))
            previousCommand = currentCommand;

        if (lastState == InputState.JustPressed && currentState == InputState.JustPressed)
            return InputState.Pressed;

        if (lastState == InputState.JustReleased && currentState == InputState.JustReleased)
        {
            ResetCommand();
            return InputState.Released;
        }

        return currentState;
    }

    bool CheckIfFirstCommandWasPressed()
    {
        return commandBindings[0].WasPressed;
    }

    bool CheckIfCommandAtIndexIsPressed(int index)
    {
        return commandBindings[index].IsPressed;
    }

    bool CheckIfCommandAtIndexHasChanged(int index)
    {
        return commandBindings[index].HasChanged;
    }

    protected override void ResetCommand()
    {
        base.ResetCommand();
        currentCommand = 0;
        previousCommand = 0;
    }

    protected void StartBufferCountdown()
    {
        buffering = true;
        bufferTimer = bufferTime;
    }

    protected void DecreaseBufferCountdown()
    {
        bufferTimer -= Time.deltaTime;
        if (bufferTimer <= 0)
            EndBufferTimer();
    }

    protected void EndBufferTimer()
    {
        buffering = false;
    }

}
