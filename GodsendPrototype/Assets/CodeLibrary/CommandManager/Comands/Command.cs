﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using System;

public enum InputState
{
    Released,
    JustReleased,
    JustPressed,
    Pressed
}

public class Command : IInputState, IAddCommands { 

    public InputState currentState;
    public InputState lastState;

    public bool IsPressed { get{ return currentState == InputState.Pressed; }}
    public bool WasPressed { get { return currentState == InputState.JustPressed; } }
    public bool WasReleased { get { return currentState == InputState.JustReleased; } }
    public bool HasChanged { get { return currentState != lastState; } }

    IInputControl mappedInput;

    public Command() {}

    public Command(IInputControl button)
    {
        mappedInput = button;
    }

    public void AddInputBinding(IInputControl newInput)
    {
        mappedInput = newInput;
    }

    public virtual void AddCommandBinding(IInputState binding)
    {
        throw new NotImplementedException();
    }
    public virtual void AddAxisBinding(OneAxisInputControl newInput)
    {
        throw new NotImplementedException();
    }
    virtual protected InputState ReturnMappedButtonState()
    {
        if (mappedInput.HasChanged)
        {
            if(mappedInput.WasPressed)
            {
                return InputState.JustPressed;
            }
            else if (mappedInput.WasReleased)
            {
                return InputState.JustReleased;
            }
        }
        if (lastState == InputState.JustPressed && currentState == InputState.JustPressed)
            return InputState.Pressed;

        if (lastState == InputState.JustReleased && currentState == InputState.JustReleased)
            return InputState.Released;

        return currentState;
    }

    public void UpdateState()
    {
        lastState = currentState;

        currentState = ReturnMappedButtonState();

    }

    public void FireCommand()
    {
        lastState = currentState = InputState.JustPressed;
    }

    public virtual bool ContainsCommand(IInputState command)
    {
        return this as IInputState == command;
    }

}
