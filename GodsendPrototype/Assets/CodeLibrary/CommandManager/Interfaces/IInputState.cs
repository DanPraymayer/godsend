﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputState {

    bool IsPressed { get; }
    bool WasPressed { get; }
    bool WasReleased { get; }
    bool HasChanged { get; }

    bool ContainsCommand(IInputState command);
    void UpdateState();
    void FireCommand();
}
