﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILockCommands {

    bool Locked { set; get; }
    int Priority { set; get; }

}
