﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public interface IAddCommands {

    void AddInputBinding(IInputControl newInput);
    void AddAxisBinding(OneAxisInputControl newInput);
    void AddCommandBinding(IInputState binding);

}
