﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHandledMultiInput {

    bool TimeOut { get; }
    bool Reset { get; }

    int CommandCount { get; }
}
