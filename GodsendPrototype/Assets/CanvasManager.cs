﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour {
    public static CanvasManager CM;
    public MovesetUI movesetUI;
    public QuestManager questManager;
    private void Awake()
    {
        if (CM != null)
        {
            Debug.LogWarning("duplicate canvas manager detected. Turning script off");
            this.enabled = false;
        }
        else
            CM = this;
    }
}
