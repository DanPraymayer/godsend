﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovesetUIIcon : MonoBehaviour
{
    public Image baseImage;
    public GameObject activeOverlay, depletedOverlay;
    //public Text counterOverlay;
    public GameObject InputHintIcon;

    public void SetBodyAbilities(Sprite s, bool abilityActive = false, string overlay2 = "")
    {
        if (s == null)
            baseImage.enabled = false;
        else
        {
            baseImage.enabled = true;
            baseImage.sprite = s;
        }
        activeOverlay.SetActive(abilityActive);
        depletedOverlay.SetActive(false);
        //counterOverlay.text = overlay2;
        InputHintIcon.SetActive(true);
    }

    public void SetUIIconInactive()
    {
        baseImage.enabled = false;
        activeOverlay.SetActive(false);
        depletedOverlay.SetActive(false);
        InputHintIcon.SetActive(false);
        //counterOverlay.text = "";
    }

    public void SetAbilityActive(bool isActive)
    {
        activeOverlay.SetActive(isActive);
    }

    public void SetAbilityDepleted(bool isActive)
    {
        depletedOverlay.SetActive(isActive);
    }

    public void SetAbilityCounter(int i)
    {
        //counterOverlay.text = i.ToString();
    }
}
