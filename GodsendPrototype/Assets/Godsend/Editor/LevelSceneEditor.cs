﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelSceneReferences))]
public class LevelSceneEditor : Editor
{
    LevelSceneReferences LSR;
    public EditorBuildSettingsScene[] EBSS;
    [SerializeField]
    SceneAsset sa;
    string heck;
    int tempInt;
    bool dskfjdkl;
    SceneInfo temp_info;

    SceneAsset unityScene;

    private void OnEnable()
    {
        LSR = (LevelSceneReferences)target;
        temp_info = new SceneInfo("");
    }

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        EBSS = EditorBuildSettings.scenes;
        LSR.ScenesInBuildSettings.Clear();
        foreach (EditorBuildSettingsScene eb in EBSS)
        {
            string s = eb.path;//.Replace(" ", "");
            s = s.Substring(s.LastIndexOf("/") + 1);
            LSR.ScenesInBuildSettings.Add(s.Remove(s.LastIndexOf(".")));
        }

        

        EditorGUILayout.LabelField(UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings + " Scenes in Build Settings", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Will not take into account unchecked levels", EditorStyles.miniLabel);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Scene Name");
        EditorGUILayout.LabelField("Build Index");
        EditorGUILayout.EndHorizontal();

        foreach (string s in LSR.ScenesInBuildSettings)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(" -" + s);
            EditorGUILayout.IntField(LSR.ScenesInBuildSettings.FindIndex(npcString => npcString == s));
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Important Scenes", EditorStyles.boldLabel);

        //index
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.ToggleLeft("Index", LSR.ScenesInBuildSettings[0] != null);
        EditorGUILayout.TextField(LSR.ScenesInBuildSettings[0] != null ? LSR.ScenesInBuildSettings[0] : "");
        EditorGUILayout.IntField(0);
        EditorGUILayout.EndHorizontal();
        //menu
        RefreshBuildIndex(LSR.MenuScene);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.ToggleLeft("Menu Scene", LSR.MenuScene.BuildIndex != -1);
        EditorGUILayout.TextField(LSR.MenuScene.SceneName);
        EditorGUILayout.IntField(LSR.MenuScene.BuildIndex);
        EditorGUILayout.EndHorizontal();
        //loading scene
        RefreshBuildIndex(LSR.LoadingScene);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.ToggleLeft("Load Splash Scene", LSR.LoadingScene.BuildIndex != -1);
        EditorGUILayout.TextField(LSR.LoadingScene.SceneName);
        EditorGUILayout.IntField(LSR.LoadingScene.BuildIndex);
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Fill values with scene asset", EditorStyles.boldLabel);
        sa = (SceneAsset)EditorGUILayout.ObjectField("Scene Asset to Use", sa, typeof(SceneAsset), false); //(UnityEditor.SceneAsset)

        if (sa != null)
        {
            FillSceneInfo(temp_info);
            EditorGUILayout.LabelField(temp_info.SceneName + (temp_info.BuildIndex == -1 ? " has not added to the build scenes list" : " is valid."));
            dskfjdkl = EditorGUILayout.Toggle("Scene is gameplay", dskfjdkl);

            if (dskfjdkl)
            {
                tempInt = EditorGUILayout.IntField("array index", tempInt);
                tempInt = Mathf.Max(0, tempInt);

                if (GUILayout.Button("Fill info by index") && dskfjdkl)
                {
                    if (tempInt < LSR.GameplayScenes.Count)
                        FillSceneInfo(LSR.GameplayScenes[tempInt], tempInt);
                    else
                    {
                        LSR.GameplayScenes.Add(new SceneInfo(""));
                        FillSceneInfo(LSR.GameplayScenes[LSR.GameplayScenes.Count - 1], LSR.GameplayScenes.Count - 1);
                    }
                }
            }
            else
            {
                //Menu scene  
                if (GUILayout.Button("Fill menu info"))
                    FillSceneInfo(LSR.MenuScene);

                //loading scene
                if (GUILayout.Button("Fill loading info"))
                    FillSceneInfo(LSR.LoadingScene);

                //current scene
                if (GUILayout.Button("Fill current scene info"))
                    FillSceneInfo(LSR.GameScene_Current);
            }
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        DrawDefaultInspector();
        //heck1 = EditorGUILayout.TextField(heck1.Remove(heck1.LastIndexOf(".")));
    }

    void FillSceneInfo(SceneInfo t_SI, int arrayIndex = -1)
    {
        t_SI.SceneName = sa.name;
        if (LSR.ScenesInBuildSettings.Contains(t_SI.SceneName))
        {
            //LSR.ScenesInBuildSettings.Find(t_SI.SceneName);
            RefreshBuildIndex(t_SI);
        }
        else
            t_SI.BuildIndex = -1;

        if (arrayIndex > -1)
            t_SI.ElementName = arrayIndex + ". " + t_SI.SceneName;
        else
            t_SI.ElementName = t_SI.SceneName;
    }

    void RefreshBuildIndex(SceneInfo t_SI)
    {
        t_SI.BuildIndex = LSR.ScenesInBuildSettings.FindIndex(npcString => npcString == t_SI.SceneName);
    }
}
