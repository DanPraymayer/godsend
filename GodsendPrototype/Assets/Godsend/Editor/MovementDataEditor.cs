﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovementData))]
public class MovementDataEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MovementData myScript = (MovementData)target;
        if (GUILayout.Button("Recalculate Jump"))
        {
            myScript.CalulateMovementValues();
        }
    }
}
