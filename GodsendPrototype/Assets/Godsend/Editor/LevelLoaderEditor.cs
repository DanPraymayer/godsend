﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelLoader))]
public class LevelLoaderEditor : Editor
{
    Object TMXFile;
    public override void OnInspectorGUI()
    {


        LevelLoader myScript = (LevelLoader)target;

        EditorGUILayout.LabelField("Optional:", EditorStyles.boldLabel);

        TMXFile = EditorGUILayout.ObjectField("Get TMX name from file", TMXFile, typeof(DefaultAsset), false);
        if (TMXFile != null)
            myScript.TMXFilename = TMXFile.name + ".tmx";

        DrawDefaultInspector();

        if (GUILayout.Button("Load Level"))
        {
            myScript.LoadLevel();
        }

    }
}
