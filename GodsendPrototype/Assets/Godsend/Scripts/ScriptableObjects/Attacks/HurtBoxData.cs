﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "HurtBox", menuName = "Scriptable Objects/Combat/HurtBox")]
public class HurtBoxData : ScriptableObject {

    public Vector3 offset;
    public Vector3 size;

    public float damage;
    public Vector2 knockBack;

    [MinMaxRange(0f, 1f)]
    // inspector slider can move between these values
    public MinMaxRange hurtBoxDuration;


}
