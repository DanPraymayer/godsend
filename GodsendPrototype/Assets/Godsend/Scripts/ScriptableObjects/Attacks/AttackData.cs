﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Attack", menuName = "Scriptable Objects/Combat/Attack")]
public class AttackData : ScriptableObject {

    public float attackCooldown;
    public float attackDelay;
    public float attackDuration;

    public bool preventXVelocity;
    public bool preventYVelocity;


}
