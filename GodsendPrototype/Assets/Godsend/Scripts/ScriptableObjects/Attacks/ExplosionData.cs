﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Explosion", menuName = "Scriptable Objects/Combat/Explosion")]
public class ExplosionData : ScriptableObject {

    public float startRadius;
    public float endRadius;

    public float explosionDamage;
    public float explosionDurarion;

    public bool reduceDamageOverDistance;

}
