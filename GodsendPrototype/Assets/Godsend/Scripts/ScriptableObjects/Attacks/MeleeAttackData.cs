﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MeleeAttack", menuName = "Scriptable Objects/Combat/MeleeAttack")]
public class MeleeAttackData : AttackData{

    public Vector2 attackForce;

    public List<HurtBoxData> hurtBoxes;



}
