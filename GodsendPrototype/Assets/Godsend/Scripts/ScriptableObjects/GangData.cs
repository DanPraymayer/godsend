﻿using UnityEngine;

[CreateAssetMenu(fileName = "GangData", menuName = "Scriptable Objects/GangStats")]
public class GangData : ScriptableObject
{
    public GangInfo[] GangTypes;
}

[System.Serializable]
public class GangInfo
{
    [HideInInspector]
    public string Name;
    //public GangClassifications gang;
    public Material gangColor;
    public string gangLayer;
}
