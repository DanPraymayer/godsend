﻿using UnityEngine;

[CreateAssetMenu(fileName = "AIAttackData", menuName = "Scriptable Objects/AIAttackSpecs")]
public class AISingleAttackData : ScriptableObject
{
    public PartialAttack attackInfo;
    public bool retreatAfterAttack;
}

[CreateAssetMenu(fileName = "AICompoundAttackData", menuName = "Scriptable Objects/AICompoundAttackSpecs")]
public class AICompoundAttackData : ScriptableObject
{
    public PartialAttack[] partialAttackParts;
}

[System.Serializable]
public class PartialAttack
{
    public float attackRange = 1;
    public float attackCooldown = 1;
    public float followUpDelay = 0.5f;
}