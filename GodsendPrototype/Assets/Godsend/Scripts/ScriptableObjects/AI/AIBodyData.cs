﻿using UnityEngine;

[CreateAssetMenu(fileName = "AIBodyData", menuName = "Scriptable Objects/AIBodySpecs")]
public class AIBodyData : ScriptableObject
{
    public float characterHeight = 1.5f;
    public Vector3 eyeBallHeight = new Vector3(0, 1.3f);

    //Vector3 jumpHeightMin = new Vector3(0, 1.5f);
    public Vector3 jumpHeightMax = new Vector3(0, 3.75f);

    public float visionCapsuleForward = 4, visionCapsuleBackward = 0, visionCapsuleRadius = 2;
    public float wallCheckDistance = 1.5f;
    public float holeCheckDepth = 2;
    public LayerMask ObstacleMask;

    public int startingAggression = 5;
    public float globalAttackCooldown = 2;
    public float walkUrgency = 0.2f, jogUrgency = 0.5f, runUrgency = 1;

    [Header("Roam State")]
    public bool Roam_allowJumping = true;
    public bool Roam_allowFalling = true;
    public float Roam_preferredRadius = 6;
    public float Roam_minWalkTime = 1.5f, Roam_maxWalkTime = 3.5f;

    [Header("Rage State")]
    public float Rage_rageStartDelay = 2;
    public float Rage_rageDuration = 8;
}
