﻿using UnityEngine;

[CreateAssetMenu(fileName = "MovementData", menuName = "Scriptable Objects/Movement")]
public class MovementData : ScriptableObject {

    public float moveSpeed = 6;

    public float airborneAccelerationTime = .2f;
    public float groundedAccelerationTime = .1f;


    public bool canJump = true;

    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .4f;

    public float fallMultiplier = .5f;
    public float lowJumpMultiplier = .2f;

    public float gravity;
    public float Gravity { get { return gravity; } }
    float maxJumpVelocity;
    public float MaxJumpVelocity { get { return maxJumpVelocity; } }
    float minJumpVelocity;
    public float MinJumpVelocity { get { return minJumpVelocity; } }


    public void CalulateMovementValues()
    {
        if (canJump)
        {
            gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
            minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        }
        else
        {
            maxJumpVelocity = 0;
            minJumpVelocity = 0;
        }

    }
}
