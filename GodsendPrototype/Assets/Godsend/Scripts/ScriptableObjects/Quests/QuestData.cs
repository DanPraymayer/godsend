﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestBodyType
{
    Main,
    Hammer,
    Thrower,
    Dasher,
    Grunt,
    Misc
}
public enum QuestActionType
{
    Kill,
    Bring,
    Misc
}

[CreateAssetMenu(fileName = "QuestData", menuName = "Scriptable Objects/Quests/Quest")]
public class QuestData : ScriptableObject {
    public BodyType possessedBodyRequired;
    public BodyType targetBodyRequired;
    public string questName;
    public string flavorText;
    public string progressPrefix = "Do ", progressSuffix = " things";
    public int chaosLevelRequired;
    public int ObjectivesToFinish;
    public int ObjectivesCompleted;

    public bool CheckCompleted { get { return ObjectivesCompleted >= ObjectivesToFinish; } } 

    public bool questCompleted;

    public void ResetQuest()
    {
        ObjectivesCompleted = 0;
        questCompleted = false;
    }


}
