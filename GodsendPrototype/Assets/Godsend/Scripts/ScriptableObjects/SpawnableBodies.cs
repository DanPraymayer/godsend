﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnableBodies", menuName = "Scriptable Objects/SpawnableBodies")]
public class SpawnableBodies : ScriptableObject {
    public GameObject[] prefabs;
}
