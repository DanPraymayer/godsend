﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : Attack
{

    public MeleeAttackData m_attackData;

    protected List<HurtBox> hurtBoxes = new List<HurtBox>();

    protected List<ITakeDamage> m_alreadyHit = new List<ITakeDamage>();

    protected override void Start()
    {
        base.Start();

        CreateHurtBoxes();

    }

    void CreateHurtBoxes()
    {
        foreach (HurtBoxData hurtBoxData in m_attackData.hurtBoxes)
        {
            hurtBoxes.Add(new HurtBox(hurtBoxData, m_attackData.attackDuration));
        }
    }

    protected override void DoAttack(LayerMask targetLayer, float dir)
    {
        m_canAttack = false;
        StartCoroutine(Attacking(targetLayer, dir));
    }

    protected override void LockMovement()
    {
        if (m_attackData.preventXVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventXVelocity = true;

        if (m_attackData.preventYVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventYVelocity = true;
    }

    protected virtual void AddAttackForce()
    {
        Vector2 forceDir = new Vector2(m_attackData.attackForce.x * m_characterMoveset.CharacterBody.CurrentController.FaceDirection, m_attackData.attackForce.y);

        m_characterMoveset.CharacterBody.PlatformerMovement.AddForce(forceDir);
    }

    protected void UpdateHurtBoxes(float dir, float timer)
    {
        m_hitBoxDrawer.hurtBoxes.Clear();
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.UpdateOnOffState(timer);
            hurtBox.UpdateOffset(dir);
            hurtBox.UpdateBounds(transform.position);

            if(hurtBox.On)
                m_hitBoxDrawer.hurtBoxes.Add(hurtBox);

        }

    }

    protected void ResetHurtBoxes()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.ResetStates();
        }
    }

    protected virtual void DamageAction(LayerMask targetLayer)
    {
        CheckForAllHurtBoxsForHits(targetLayer);
    }

    protected virtual void DelayedAttackAction(float timer, float dir)
    {

    }

    protected virtual void CheckForAllHurtBoxsForHits(LayerMask targetLayer)
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            if(hurtBox.On)
                DealDamageToDamageableTargets(hurtBox, CheckForHurtBoxForHits(hurtBox, targetLayer));
        }
    }

    protected List<Collider> CheckForHurtBoxForHits(HurtBox hurtBox, LayerMask targetLayer)
    {
        List<Collider> hurtBoxHitColliders = new List<Collider>();

        Collider[] hitColliders = new Collider[30];
        int numberHitTargets = Physics.OverlapBoxNonAlloc(hurtBox.bounds.center, hurtBox.bounds.size / 2, hitColliders, Quaternion.identity, targetLayer);

        if (numberHitTargets > 0)
        {
            List<Collider> currentHitColliders = new List<Collider>();
            currentHitColliders.AddRange(hitColliders);

            foreach (Collider collider in m_characterMoveset.CharacterBody.HitColldiers)
            {
                if (currentHitColliders.Contains(collider))
                    currentHitColliders.Remove(collider);
            }

            foreach (Collider collider in currentHitColliders)
            {
                if (collider != null && !hurtBoxHitColliders.Contains(collider))
                    hurtBoxHitColliders.Add(collider);
            }
        }

        return hurtBoxHitColliders;
    }

    protected virtual void DealDamageToDamageableTargets(HurtBox hurtBox, List<Collider> colliders)
    {

        foreach (Collider collider in colliders)
        {
            ITakeDamage damageable = collider.GetComponentInParent<ITakeDamage>();

            if (damageable != null && !m_alreadyHit.Contains(damageable))
            {
                DealDamage(hurtBox, damageable);
            }
        }

    }

    protected void DealDamage(HurtBox hurtBox, ITakeDamage damageable)
    {
        Vector2 dirKnockBack = new Vector2(((m_attackData.attackForce.x + hurtBox.KnockBack.x) * m_characterMoveset.CharacterBody.CurrentController.FaceDirection), hurtBox.KnockBack.y);

        damageable.KnockBack(dirKnockBack);
        damageable.TakeDamage(hurtBox.AttackDamage, m_characterMoveset.CharacterBody);
        m_alreadyHit.Add(damageable);
    }

    protected override IEnumerator Attacking(LayerMask targetLayer, float dir)
    {
        LockMovement();
        PreDelayAction();

        yield return new WaitForSeconds(m_attackData.attackDelay);

        PostDelayAction();
        AddAttackForce();

        PreAttackAction();
        float timer = 0;
        while (timer < m_attackData.attackDuration)
        {
            UpdateHurtBoxes(m_characterMoveset.CharacterBody.CurrentController.FaceDirection, timer);
            DamageAction(targetLayer);

            timer += Time.deltaTime;
            DelayedAttackAction(timer, dir);
            yield return null;
        }

        PostAttackAction();
        ResetHurtBoxes();
        EndAttack(m_attackData.attackCooldown);
    }

    protected override void EndAttack(float cooldown)
    {
        base.EndAttack(cooldown);

        if (m_attackData.preventXVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventXVelocity = false;

        if (m_attackData.preventYVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventYVelocity = false;

        m_hitBoxDrawer.hurtBoxes.Clear();
        m_alreadyHit.Clear();
    }

}
