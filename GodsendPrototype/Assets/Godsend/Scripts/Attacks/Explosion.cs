﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    public ExplosionData m_explosionData;
    LayerMask m_targetLayer;

    float growthRate;
    float currentRadius;

    List<ITakeDamage> m_alreadyHit = new List<ITakeDamage>();

    public void SetUpExplosion(LayerMask targetLayer)
    {

        m_targetLayer = targetLayer;

        growthRate = m_explosionData.endRadius / m_explosionData.explosionDurarion * Time.deltaTime;
        currentRadius = m_explosionData.startRadius;

        transform.localScale = new Vector3(currentRadius, currentRadius, currentRadius);

    }

    public void ExplosionStart()
    {
        StartCoroutine(ContinueExplosion());
    }

    IEnumerator ContinueExplosion()
    {

        float timer = 0;

        while (timer < m_explosionData.explosionDurarion)
        {

            currentRadius += growthRate;

            transform.localScale = new Vector3(transform.localScale.x + growthRate, transform.localScale.y + growthRate, transform.localScale.z + growthRate);

            CheckForHits(m_targetLayer);

            timer += Time.deltaTime;
            yield return null;
        }

        ExplosionEnd();

        yield return null;
    }


    void CheckForHits(LayerMask targetLayer)
    {

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, currentRadius / 2, targetLayer);

        DealDamageToDamageableTargets(hitColliders);
    }

    void DealDamageToDamageableTargets(Collider[] hitColliders)
    {
        foreach (Collider collider in hitColliders)
        {
            ITakeDamage damageable = TransformHelper.FindHighestParentObject(collider.gameObject).GetComponentInChildren<ITakeDamage>();

            if (damageable != null && !m_alreadyHit.Contains(damageable))
            {

                damageable.TakeDamage(m_explosionData.explosionDamage);
                m_alreadyHit.Add(damageable);

            }
        }

    }

    public void ExplosionEnd()
    {
        Destroy(this.gameObject);
    }
}
