﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAttack : Attack {

    BomberMoveset m_throwerGirlMoveSet;
    public bool AbilityIsReady { get { return m_throwerGirlMoveSet.GrabTarget!=null; } }

    protected override void Start()
    {
        base.Start();
        m_throwerGirlMoveSet = GetComponentInParent<BomberMoveset>();
    }

    protected override void DoAttack(LayerMask targetLayer, float dir)
    {
        m_throwerGirlMoveSet.GrabTarget.SetParentInactive();

        m_throwerGirlMoveSet.GrabTarget.AddThrowForce(new Vector3(75 * m_throwerGirlMoveSet.CharacterBody.CurrentController.FaceDirection, 15));

        m_throwerGirlMoveSet.GrabTarget = null;
    }


}
