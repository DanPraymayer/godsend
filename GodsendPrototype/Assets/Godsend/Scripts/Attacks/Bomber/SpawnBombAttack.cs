﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBombAttack : Attack
{

    public AttackData m_attackData;
    public GameObject bombPrefab;

    public int maxNumberOfBombs;
    int currentNumberOfBombs;
    public int CurrentNumberOfBombs
    {
        get { return currentNumberOfBombs; }
        set
        {
            currentNumberOfBombs = value;
            if (m_characterMoveset.transform.root.gameObject.layer == NamingConstants.PLAYER_LAYER)
            {
                CanvasManager.CM.movesetUI.AbilityDown.SetAbilityCounter(maxNumberOfBombs - currentNumberOfBombs);
            }
        }
    }

    protected override void DoAttack(LayerMask targetLayer, float dir)
    {
        m_canAttack = false;
        StartCoroutine(Attacking(targetLayer, dir));
    }

    protected override void LockMovement()
    {
        if (m_attackData.preventXVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventXVelocity = true;

        if (m_attackData.preventYVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventYVelocity = true;
    }

    protected override IEnumerator Attacking(LayerMask targetLayer, float dir)
    {
        LockMovement();
        PreDelayAction();

        yield return new WaitForSeconds(m_attackData.attackDelay);

        PostDelayAction();

        PreAttackAction();
        float timer = 0;
        while (timer < m_attackData.attackDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        PostAttackAction();
        EndAttack(m_attackData.attackCooldown);
    }

    protected override void PreAttackAction()
    {
        if (CurrentNumberOfBombs < maxNumberOfBombs)
            SpawnBomb();
    }

    void SpawnBomb()
    {
        GameObject newBomb = (GameObject)Instantiate(bombPrefab, transform.position, Quaternion.identity);
        newBomb.GetComponent<Bomb>().spawnSource = this;
        CurrentNumberOfBombs++;
        Instantiate(bombPrefab, transform.position, Quaternion.identity);
    }

    public void BombDestoried()
    {
        CurrentNumberOfBombs--;
    }

}
