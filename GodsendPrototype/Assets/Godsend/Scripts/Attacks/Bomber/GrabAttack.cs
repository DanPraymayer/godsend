﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabAttack : MeleeAttack
{

    BomberMoveset m_throwerGirlMoveSet;
    public LayerMask BombLayer;

    protected override void Start()
    {
        base.Start();
        m_throwerGirlMoveSet = GetComponentInParent<BomberMoveset>();
    }

    protected override void DamageAction(LayerMask targetLayer)
    {
        GrabEnemy(CheckForHurtBoxForGrabHits(targetLayer | BombLayer));
    }

    protected List<Collider> CheckForHurtBoxForGrabHits(LayerMask targetLayer)
    {
        List<Collider> hurtBoxHitColliders = new List<Collider>();

        foreach (HurtBox hurtBox in hurtBoxes)
        {
            Collider[] hitColliders = new Collider[30];
            int numberHitTargets = Physics.OverlapBoxNonAlloc(hurtBox.bounds.center, hurtBox.bounds.size / 2, hitColliders, Quaternion.identity, targetLayer);

            if (numberHitTargets > 0)
            {
                List<Collider> currentHitColliders = new List<Collider>();
                currentHitColliders.AddRange(hitColliders);

                foreach (Collider collider in m_characterMoveset.CharacterBody.HitColldiers)
                {
                    if (currentHitColliders.Contains(collider))
                        currentHitColliders.Remove(collider);
                }

                foreach (Collider collider in currentHitColliders)
                {
                    if (collider != null && !hurtBoxHitColliders.Contains(collider))
                        hurtBoxHitColliders.Add(collider);
                }
            }

        }
        return hurtBoxHitColliders;
    }

    void GrabEnemy(List<Collider> colliders)
    {
        IThrowable grabTarget = null;

        List<IThrowable> m_alreadyHit = new List<IThrowable>();

        foreach (Collider collider in colliders)
        {
            IThrowable throwable = collider.gameObject.GetComponentInParent<IThrowable>();
            if (throwable == null)
            {
               
            }

            if (grabTarget == null || Vector3.Distance(transform.position, collider.transform.position) < Vector3.Distance(transform.position, grabTarget.ReturnPosition()))
            {
                grabTarget = throwable;
            }

        }
        if (grabTarget != null)
        {
            m_throwerGirlMoveSet.GrabTarget = grabTarget;
            grabTarget.SetParentInactive();
        }


    }

}
