﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour, IThrowable
{

    Rigidbody rigidbody;
    Explosion explosion;
    public SpawnBombAttack spawnSource;

    public LayerMask targetLayer;

    public float timeToExplode;

    void Start()
    {
        rigidbody = GetComponentInChildren<Rigidbody>();
        explosion = GetComponentInChildren<Explosion>();

        StartCoroutine(ExposionTimer());
    }

    public void AddThrowForce(Vector3 force)
    {
        rigidbody.velocity = force * .25f;
    }

    public Vector3 ReturnPosition()
    {
        return transform.position;
    }

    public void SetParentInactive()
    {
        rigidbody.velocity = Vector3.zero;
    }

    public void SetParentActive()   {    }

    public void UpdatePosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    void StartExplode()
    {
        if (spawnSource != null)
            spawnSource.BombDestoried();

        rigidbody.useGravity = false;
        rigidbody.velocity = Vector2.zero;

        GetComponent<Collider>().isTrigger = true;
        explosion.SetUpExplosion(targetLayer);
        explosion.ExplosionStart();
    }

    IEnumerator ExposionTimer()
    {
        yield return new WaitForSeconds(timeToExplode);
        StartExplode();
    }


}
