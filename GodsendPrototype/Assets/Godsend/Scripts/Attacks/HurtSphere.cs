﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtSphere  {

    public Vector3 offset;
    Vector3 dirOffset;
    public Vector3 size;

    public Vector3 center;

    public HurtSphere(Vector3 offset, Vector3 size)
    {
        this.offset = offset;
        this.size = size;
        this.dirOffset = offset;
    }

    public void UpdateOffset(float dir)
    {
        dirOffset.x = offset.x * dir;
    }

    public void UpdatePosition(Vector3 position)
    {
        center = position + dirOffset;
    }

}
