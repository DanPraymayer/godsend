﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RallyAttack : Attack
{

    public MeleeAttackData m_attackData;

    public EnemyController[] summons;
    LayerMask attackMask;

    List<HurtBox> hurtBoxes = new List<HurtBox>();

    List<ITakeDamage> m_alreadyHit = new List<ITakeDamage>();

    int numberOfAttacksInAir;

    protected override void Start()
    {
        base.Start();
        attackMask = LayerMask.GetMask(LayerMask.LayerToName(m_characterMoveset.CharacterBody.gameObject.layer));
    }

    protected override void DoAttack(LayerMask targetLayer, float dir)
    {
        StartCoroutine(Attacking(targetLayer, dir));
    }

    IEnumerator Attacking(LayerMask targetLayer, float dir)
    {
        if (m_attackData.preventXVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventXVelocity = true;

        if (m_attackData.preventYVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventYVelocity = true;

        yield return new WaitForSeconds(m_attackData.attackDelay);

        SummonCreatures();

        float timer = 0;

        while (timer < m_attackData.attackDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        EndAttack(m_attackData.attackCooldown);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, 6);
    }

    void SummonCreatures()
    {
        int numResults = 5;
        //get all colliders in an area
        Collider[] nearbyCreatures = Physics.OverlapSphere(m_characterMoveset.CharacterBody.transform.position, 6, attackMask);
        //remove duplicates
        List<EnemyController> nearbyCreatureRoots = new List<EnemyController>();
        foreach (Collider c in nearbyCreatures)
        {
            if (c.transform.root != m_characterMoveset.CharacterBody.transform.root)
            {
                EnemyController ecsD = c.transform.root.gameObject.GetComponent<EnemyController>();
                if (ecsD != null && !nearbyCreatureRoots.Contains(ecsD))
                    nearbyCreatureRoots.Add(ecsD);
            }
        }

        if (nearbyCreatureRoots.Count == 0)
        {
            Debug.Log("no one");
            return;
        }

        if (numResults > nearbyCreatureRoots.Count)
        {
            Debug.Log("uh not enough people in the area");
        }
        //
        IList<EnemyController> sortedRoots = new List<EnemyController> { nearbyCreatureRoots[0].GetComponent<EnemyController>() };

        if (nearbyCreatureRoots.Count > 1)
        {
            for (int a = 1; a < nearbyCreatureRoots.Count; a++)
            {
                for (int b = 0; b < sortedRoots.Count; b++)
                {
                    if ((nearbyCreatureRoots[a].transform.position - m_characterMoveset.CharacterBody.transform.position).sqrMagnitude < (sortedRoots[b].transform.position - m_characterMoveset.CharacterBody.transform.position).sqrMagnitude)
                    {
                        if (b >= sortedRoots.Count - 1)
                            sortedRoots.Add(nearbyCreatureRoots[a].GetComponent<EnemyController>());
                        else
                            sortedRoots.Insert(b, nearbyCreatureRoots[a].GetComponent<EnemyController>());
                    }
                }
                if (a >= numResults)
                    break;
            }
        }

        summons = sortedRoots.ToArray();

        if (summons.Length > 0)
        {
            foreach (EnemyController ec in summons)
            {
                ec.TailTransform(m_characterMoveset.CharacterBody.transform);
            }
        }
        else
            Debug.Log("I'm mr lonely");
    }

    protected override void EndAttack(float cooldown)
    {
        base.EndAttack(cooldown);

        if (m_attackData.preventXVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventXVelocity = false;

        if (m_attackData.preventYVelocity)
            m_characterMoveset.CharacterBody.PlatformerMovement.preventYVelocity = false;

        m_alreadyHit.Clear();
    }
}
