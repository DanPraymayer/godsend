﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnrageAttack : MeleeAttack
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void DamageAction(LayerMask targetLayer)
    {
        EnrageEnemy(CheckForHurtBoxForGrabHits(targetLayer));
    }

    protected List<Collider> CheckForHurtBoxForGrabHits(LayerMask targetLayer)
    {
        List<Collider> hurtBoxHitColliders = new List<Collider>();

        foreach (HurtBox hurtBox in hurtBoxes)
        {
            Collider[] hitColliders = new Collider[30];
            int numberHitTargets = Physics.OverlapBoxNonAlloc(hurtBox.bounds.center, hurtBox.bounds.size / 2, hitColliders, Quaternion.identity, targetLayer);

            if (numberHitTargets > 0)
            {
                List<Collider> currentHitColliders = new List<Collider>();
                currentHitColliders.AddRange(hitColliders);

                foreach (Collider collider in m_characterMoveset.CharacterBody.HitColldiers)
                {
                    if (currentHitColliders.Contains(collider))
                        currentHitColliders.Remove(collider);
                }

                foreach (Collider collider in currentHitColliders)
                {
                    if (collider != null && !hurtBoxHitColliders.Contains(collider))
                        hurtBoxHitColliders.Add(collider);
                }
            }

        }
        return hurtBoxHitColliders;
    }

    void EnrageEnemy(List<Collider> colliders)
    {
        EnemyController enrageTarget = null;
        List<EnemyController> m_alreadyHit = new List<EnemyController>();

        foreach (Collider collider in colliders)
        {
            EnemyController enrageable = collider.GetComponentInParent<EnemyController>();
            if (enrageable != null)
                if (enrageTarget == null || Vector3.Distance(transform.position, collider.transform.position) < Vector3.Distance(transform.position, enrageTarget.transform.parent.transform.position))
                {
                    enrageTarget = enrageable;
                    enrageTarget.TargetOfInterest = transform.parent.gameObject;
                    enrageTarget.GoToState(AIStates.Raging);
                    break;
                }
        }
    }

}
