﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAttack : MeleeAttack {

    bool resetAttack = false;

    Vector2 dashDirection;

    public void SetDashDirection(Vector3 dir)
    {
        dashDirection = dir * m_attackData.attackForce.x;
    }

    protected override void AddAttackForce()
    {
        m_characterMoveset.CharacterBody.PlatformerMovement.AddForce(dashDirection);
    }

    protected override void DealDamageToDamageableTargets(HurtBox hurtBox, List<Collider> colliders)
    {
        foreach (Collider collider in colliders)
        {
            ITakeDamage damageable = collider.GetComponentInParent<ITakeDamage>();

            if (damageable != null && !m_alreadyHit.Contains(damageable))
            {
                DealDamage(hurtBox, damageable);
            }

            if (collider.gameObject.layer == NamingConstants.RESETER_LAYER)
            {
                resetAttack = true;
            }
        }
    }

    protected override void EndAttack(float cooldown)
    {
        base.EndAttack(cooldown);
        if (resetAttack)
        {
            StopAllCoroutines();
            m_canAttack = true;
            resetAttack = false;
            if (transform.root.gameObject.layer == NamingConstants.PLAYER_LAYER)
            {
                CanvasManager.CM.movesetUI.AbilityStandard.SetAbilityDepleted(false);
            }
        }
        CanvasManager.CM.movesetUI.AbilityStandard.SetAbilityDepleted(false); //TODO: remove, this should be triggered when the cooldown ends
    }
}
