﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSlamAttack : MeleeAttack {

    public GameObject pillarPrefab;

    HammerPillar pillar;

    public float spawnDistance;

    public bool PillarIsAlive { get { return pillar != null && pillar.gameObject.activeInHierarchy; } }
    public Vector3 PillarPosition { get { return pillar != null ? pillar.transform.position : Vector3.zero; } }

    protected override void Start()
    {
        base.Start();

        pillar = Instantiate(pillarPrefab).GetComponent<HammerPillar>();
        pillar.ownerBody = GetComponentInParent<CharacterBody>();

        pillar.gameObject.SetActive(false);
    }

    protected override void DelayedAttackAction(float timer, float dir)
    {
        if (timer < m_attackData.attackDuration * .5f)
            SummonPillar(dir);
    }

    void SummonPillar(float dir)
    {
        pillar.gameObject.SetActive(true);
        pillar.SetPosition(transform.position + Vector3.right * dir * spawnDistance);
        pillar.ResetPillar();
    }

}
