﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class HurtBox  {

    HurtBoxData hurtBoxData;

    public float AttackDamage { get { return hurtBoxData.damage; } }
    public Vector2 KnockBack { get { return hurtBoxData.knockBack; } }

    float startTime;
    float endTime;

    bool m_on = false;
    bool done = false;

    public bool On { get { return m_on; } }

    Vector3 dirOffset;
    public Bounds bounds;

    public HurtBox(HurtBoxData hurtBoxData, float attackDuration)
    {
        this.hurtBoxData = hurtBoxData;
        this.dirOffset = hurtBoxData.offset;

        startTime = hurtBoxData.hurtBoxDuration.rangeStart * attackDuration;
        endTime = hurtBoxData.hurtBoxDuration.rangeEnd * attackDuration;

        bounds = new Bounds(hurtBoxData.offset, hurtBoxData.size);
    }
    
    public void UpdateOffset(float dir)
    {
        dirOffset.x = hurtBoxData.offset.x *dir;
    }

    public void UpdateBounds(Vector3 position)
    {
        bounds.center = position + dirOffset;
    }

    public void UpdateOnOffState(float timer)
    {
        if (!done && !m_on && timer >= startTime)
            m_on = true;
        else if(m_on && timer >= endTime)
        {
            m_on = false;
            done = true;
        }
    }

    public void ResetStates()
    {
        m_on = false;
        done = false;
    }
}
