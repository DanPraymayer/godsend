﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {
     
    protected bool m_canAttack = true;
    public virtual bool CanAttack { get { return m_canAttack; } }

    protected CharacterMoveset m_characterMoveset;

    protected HitBoxDrawer m_hitBoxDrawer;

    public bool AbilityIsReady { get { return true; } }

    protected virtual void Start()
    {
        m_characterMoveset = GetComponentInParent<CharacterMoveset>();


        m_hitBoxDrawer = GetComponentInParent<HitBoxDrawer>();
    }

    public void StartAttack(LayerMask targetLayer, float dir)
    {
        DoAttack(targetLayer, dir);
    }

    protected virtual void DoAttack(LayerMask targetLayer, float dir){}

    protected virtual void LockMovement(){ }

    protected virtual void PreDelayAction()
    {

    }

    protected virtual void PostDelayAction()
    {

    }

    protected virtual void PreAttackAction()
    {

    }


    protected virtual IEnumerator Attacking(LayerMask targetLayer, float dir)
    {
        yield return null;
    }



    protected virtual void PostAttackAction()
    {

    }

    protected virtual void EndAttack(float cooldown)
    {
        m_characterMoveset.CurrentAttackType = CharacterMoveset.AttackType.NotAttacking;
        StartCooldown(cooldown);
    }

    void StartCooldown(float cooldown)
    {
        m_canAttack = false;
        StartCoroutine(Cooldown(cooldown));
    }

    IEnumerator Cooldown(float cooldown)
    {
        yield return new WaitForSeconds(cooldown);
        EndCooldown();
    }

    void EndCooldown()
    {
        m_canAttack = true;
    }

    

}
