﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public float damage;
    public Vector3 direction;
    public float speed;

    public Vector3 colliderSize;

    public LayerMask targetLayer;
    public LayerMask colliderLayer;

    protected float collisionDelay = .1f;


    protected virtual void Start()
    {
        colliderSize = GetComponent<BoxCollider>().size * .5f;
    }

    protected virtual void Update()
    {
        collisionDelay -= Time.deltaTime;
        if (collisionDelay < 0)
            CheckForHits(targetLayer, colliderLayer);
        Move();
    }

    public void Setup(Vector2 direction)
    {
        this.direction = direction;
    }

    public void Move()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }

    protected virtual void CheckForHits(LayerMask targetLayer, LayerMask colliderLayer)
    {

        Collider[] hitColliders = Physics.OverlapBox(transform.position, colliderSize, Quaternion.identity, targetLayer | colliderLayer);

        if (hitColliders.Length > 0)
        {
            if (TransformHelper.ContainsLayer(targetLayer, hitColliders[0].gameObject.layer))
            {
                hitColliders[0].gameObject.GetComponentInParent<ITakeDamage>().TakeDamage(damage);
                Destroy(this.gameObject);

            }
            if (TransformHelper.ContainsLayer(colliderLayer, hitColliders[0].gameObject.layer))
            {
                Destroy(this.gameObject);
            }
        }

    }
}
