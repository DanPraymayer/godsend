﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class QuestSlot
{
    public QuestData questData;
    public Text Name;
    public Text Flavor;
    public Text Progress;
    public bool inUse;
    public bool isMainQuest;

    public void FillSlot(QuestData quest)
    {
        questData = quest;
        inUse = true;
        Name.text = quest.questName;
        SetFlavorText();
        SetProgressText();
    }
    void SetFlavorText()
    {
        if (isMainQuest)
        {
            if (questData != null)
                Flavor.text = questData.flavorText;
            else
                Flavor.text = "";
        }
    }
    public void SetProgressText()
    {
        if (questData != null)
            Progress.text = questData.progressPrefix + " " + questData.ObjectivesCompleted + "/" + questData.ObjectivesToFinish + " " + questData.progressSuffix;
        else
            Progress.text = "";
    }

    public void ClearSlot()
    {
        inUse = false;
        questData = null;
        if (isMainQuest)
            Name.text = "Main Quest Completed!";
        else
            Name.text = "Find a side quest available";
        SetFlavorText();
        SetProgressText();
    }
}

public class QuestManager : MonoBehaviour
{
    public List<QuestData> mainQuestLine;
    public List<QuestData> sideQuestsAcquired;

    public QuestSlot mainQuestSlot;
    public QuestSlot[] sideQuestSlots;


    private void Start()
    {
        mainQuestSlot.isMainQuest = true;
        FillQuestSlots();
    }


    /*
        public bool HasRoomForQuest { get { return displayedQuests.Count < sidequests.Length; } }


        private void Start()
        {
            notStartedQuests.AddRange(allQuests);

            ResetQuests();
            for (int i = 0; i < sidequests.Length; i++)
            {
                PickNewQuest();
            }
            //UpdateQuestText();
        }

        

        public void UpdateQuest(QuestData questData)
        {
            if (displayedQuests.Contains(questData))
            {
                questData.ObjectivesCompleted++;
                if (questData.CheckCompleted)
                    CompletedQuest(questData);
            }
            UpdateQuestText();
        }
        public void UpdateQuestText(QuestData sideQuest, SidequestUIText UIText)
        {
            UIText.Name.text = sideQuest.questName;
            UIText.Progress.text += sideQuest.progressPrefix + sideQuest.ObjectivesCompleted + "/" + sideQuest.ObjectivesToFinish + sideQuest.progressSuffix;
        }

        private void ResetQuests()
        {
            foreach (QuestData quest in allQuests)
            {
                //quest.ResetQuest();

            }
        }

        void SwitchQuest()
        {
            QuestData quest = notStartedQuests[Random.Range(0, notStartedQuests.Count)];

            notStartedQuests.Remove(quest);
            displayedQuests.Add(quest);
            sidequestUI.
        }



        public void AddQuest(QuestData questData)
        {
            displayedQuests.Add(questData);

            UpdateQuestText();
        }

        */
    public void AddSidequest(QuestData quest)
    {
        if (!sideQuestsAcquired.Contains(quest))
            sideQuestsAcquired.Add(quest);
    }

    bool UsingCorrectBody(BodyType enemyType)
    {
        return mainQuestSlot.questData.targetBodyRequired == BodyType.None || mainQuestSlot.questData.targetBodyRequired == enemyType;
    }

    bool KillingCorrectBody(BodyType enemyType)
    {
        return (mainQuestSlot.questData as KillQuestData).possessedBodyRequired == BodyType.None || (mainQuestSlot.questData as KillQuestData).possessedBodyRequired == enemyType;
    }

    public void EnemyKilled(BodyType enemyType)
    {
        if (mainQuestSlot.inUse && UsingCorrectBody(enemyType) && KillingCorrectBody(enemyType))
        {
            MakeQuestProgress(mainQuestSlot);
        }

        foreach (QuestSlot slot in sideQuestSlots)
        {
            if (slot.inUse && UsingCorrectBody(enemyType) && KillingCorrectBody(enemyType))
            {
                MakeQuestProgress(slot);
            }
        }
    }

    void MakeQuestProgress(QuestSlot slot)
    {
        slot.questData.ObjectivesCompleted++;
        if (slot.questData.CheckCompleted)
        {
            slot.Progress.text = "Quest complete!";
            slot.questData.questCompleted = true;
            slot.inUse = false;
            FillQuestSlots();
        }
        else
            slot.SetProgressText();
    }

    void FillQuestSlots()
    {
        FillMainQuestSlot();
        FillSideQuestSlots();
    }

    public void FillMainQuestSlot()
    {
        if (!mainQuestSlot.inUse)
        {
            if (mainQuestLine.Count > 0)
            {
                mainQuestSlot.FillSlot(mainQuestLine[0]);
                mainQuestLine.RemoveAt(0);
            }
            else
                mainQuestSlot.ClearSlot();
        }
    }

    public void FillSideQuestSlots()
    {
        foreach (QuestSlot s in sideQuestSlots)
        {
            if (!s.inUse)
            {
                if (sideQuestsAcquired.Count > 0)
                {
                    s.FillSlot(sideQuestsAcquired[0]);
                    sideQuestsAcquired.RemoveAt(0);
                }
                else
                    s.ClearSlot();
            }
        }
    }
}
