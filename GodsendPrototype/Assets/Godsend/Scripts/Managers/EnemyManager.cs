﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager EM;
    //[HideInInspector]
    ChaosManager chaosManager;
    PlayerController player;

    public SpawnableBodies spawnableBodies;
    public Dictionary<BodyType, GameObject> SpawnEnemyByBodyType = new Dictionary<BodyType, GameObject>();
    public DenizenData denizenTypesData;
    protected Dictionary<DenizenType, DenizenInfo> DenizenClassInfoByReferenceDictionary { get; set; }
    //public Dictionary<GangClassifications, string> EnemyLayerByReference = new Dictionary<GangClassifications, string>();

    public List<EnemyController> CurrentEnemies = new List<EnemyController>();
    public List<GameObject> NPCSpawners;
    public List<GameObject> PointsOfInterest;
    public AIEmotes AIEmoteList;

    public delegate void EnemyEvent();
    public static event EnemyEvent wantedLevelCleared;
    public static event EnemyEvent wantedLevelRaised;
    public static event EnemyEvent PlayerEscaped;
    //public static event EnemyEvent BodyKilled;
    public static event EnemyEvent ConfirmKillTarget;

    public delegate void EnemyObjective(BodyType enemyType);
    public static event EnemyObjective EnemyKilled;

   /* #region wanted level
    int wantedLevel;
    public int WantedLevel
    {
        get { return wantedLevel; }
        private set
        {
            value = Mathf.Clamp(value, MinWantedLevel, MaxWantedLevel);

            if (wantedRatingText != null)
                wantedRatingText.text = "Wanted Level:\n" + value;

            if (value != wantedLevel)
            {
                if (value == 0)
                {
                    KillTarget = null;
                    if (wantedLevelCleared != null)
                        wantedLevelCleared();
                }
                if (value > wantedLevel)
                {
                    if (wantedLevelRaised != null)
                        wantedLevelRaised();
                    DecreaseWantedLevelTimestamp = Time.time;
                }

                wantedLevel = value;
            }

            switch (wantedLevel)
            {
                case (0):
                    //relax
                    break;
                //panic!
                default:
                    break;
            }
        }
    }
    int MinWantedLevel { get { return PlayerPossessingBody ? 0 : 1; } }
    int MaxWantedLevel = 5;
    int EqualizedWantedLevel { get { return Mathf.Clamp(WantedLevel, MinWantedLevel, MaxWantedLevel); } }
    float DecreaseWantedLevelTimestamp;
    //public float secondsToLoseOneWantedLevelStar = 3;
    #endregion*/

    GameObject WatchTarget { get { return player.CurrentBody.gameObject; } }
    bool playerPossessingBody;
    public bool PlayerPossessingBody
    {
        get { return playerPossessingBody; }
        private set
        {
            if (!playerPossessingBody && value)
            {
                if (ConfirmKillTarget != null)
                    ConfirmKillTarget();
            }

            playerPossessingBody = value;
        }
    }

    public bool HasKillTarget { get; private set; }
    GameObject killTarget;
    public GameObject KillTarget
    {
        get { return killTarget; }
        private set
        {
            killTarget = value;
            HasKillTarget = (killTarget != null);
            //if(!HasKillTarget)+
            if (ConfirmKillTarget != null)
                ConfirmKillTarget();
        }
    }

    UnityEngine.UI.Text wantedRatingText;
    //public UnityEngine.UI.Text 

    private void OnEnable()
    {
        PlayerController.AcquiredBodyAtGameStart += DelayedStart;
        Possession.EnteredBodyFromSelf += PlayerPossessedBody;
        //Possession.EnteredBodyFromBody += PlayerSwappedBody;
    }
    private void OnDisable()
    {
        Possession.EnteredBodyFromSelf -= PlayerPossessedBody;
        //Possession.EnteredBodyFromBody -= PlayerSwappedBody;
        Possession.ExittedBody -= PlayerLeftBody;
    }

    private void Awake()
    {
        if (EM == null)
            EM = this;
        else
        {
            Debug.LogWarning(gameObject.name + " != " + EM.gameObject.name);
            this.enabled = false;
        }

        chaosManager = GetComponent<ChaosManager>();

        DenizenClassInfoByReferenceDictionary = new Dictionary<DenizenType, DenizenInfo>();
        for (int i = 0; i < System.Enum.GetValues(typeof(DenizenType)).Length; i++)
        {
            foreach (DenizenInfo gi in denizenTypesData.Denizens)
            {
                DenizenType VET = (DenizenType)System.Enum.GetValues(typeof(DenizenType)).GetValue(i);
                if (gi.denizenType == VET)
                    DenizenClassInfoByReferenceDictionary.Add(VET, gi);
            }
        }

        if(spawnableBodies!=null)
        for (int i = 0; i < System.Enum.GetValues(typeof(BodyType)).Length; i++)
        {
            SpawnEnemyByBodyType[(BodyType)System.Enum.GetValues(typeof(BodyType)).GetValue(i)] = spawnableBodies.prefabs[i];
        }
    }

    void Start()
    {
        foreach (EnemyController EC in GameObject.FindObjectsOfType<EnemyController>())
        {
            CurrentEnemies.Add(EC);
        }
    }

    public DenizenInfo DenizenClassInfoByReference(DenizenType denizenType)
    {
        DenizenInfo relatedInfo;
        if (DenizenClassInfoByReferenceDictionary.TryGetValue(denizenType, out relatedInfo))
            return DenizenClassInfoByReferenceDictionary[denizenType];
        else
        {
            if (denizenType == 0)
                Debug.LogWarning("Starting denizen info not assigned in inspector");
            else
                Debug.LogWarning("AllDenizens Data asset referenced by Enemy Manager does not have " + denizenType + " info. Click this message to fix.", EnemyManager.EM);
            return null;
        }
    }

    void DelayedStart()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        PlayerPossessingBody = false;
        KillTarget = WatchTarget; //sets kill target, confirms kill target
        //WantedLevel = 1; //increases wanted level, confirms kill target
        //ConfirmKillTarget(); //confirms kill target but does nothing
        Debug.Log("Let the games begin");
        PlayerController.AcquiredBodyAtGameStart -= DelayedStart;
        Possession.ExittedBody += PlayerLeftBody;
    }

    void Update()
    {
        //WantedLevel = EqualizedWantedLevel;

        /* if (WantedLevel > MinWantedLevel)
         {
             if (Time.time > DecreaseWantedLevelTimestamp + secondsToLoseOneWantedLevelStar)
             {
                 WantedLevel--;
                 DecreaseWantedLevelTimestamp = Time.time;
             }
         }*/
    }

    public void SpawnCreature(BodyType enemyType, Vector3 position)
    {
        if (SpawnEnemyByBodyType[enemyType] == null)
            return;

        GameObject newEnemy = (GameObject)Instantiate(SpawnEnemyByBodyType[enemyType], position, Quaternion.identity);
        EnemyController newEnemyScript = newEnemy.GetComponent<EnemyController>();
        //newEnemyScript.CurrentAIState = EnemyController.AIStates.roaming;
        CurrentEnemies.Add(newEnemyScript);
    }

    public GameObject ClosestPointOfInterest(Vector3 askerPosition)
    {
        if (PointsOfInterest.Count <= 0)
            return null;

        GameObject closestAttraction = PointsOfInterest[0];

        if (PointsOfInterest.Count > 1)
        {
            float compareDistance = Vector3.Distance(askerPosition, closestAttraction.transform.position);
            for (int i = 1; i < PointsOfInterest.Count; i++)
            {
                if (Vector3.Distance(askerPosition, PointsOfInterest[i].transform.position) < compareDistance)
                {
                    closestAttraction = PointsOfInterest[i];
                    compareDistance = Vector3.Distance(askerPosition, closestAttraction.transform.position);
                }
            }
            return closestAttraction;
        }
        else
        {
            return closestAttraction;
        }
    }

    public EnemyController ClosestEnemy(Vector3 startPosition, float range, LayerMask targetMask)
    {
        return ClosestEnemy(startPosition, range, targetMask, 1)[0];
    }
    public EnemyController[] ClosestEnemy(Vector3 startPosition, float range, LayerMask targetMask, int numResults)
    {
        //get all colliders in an area
        Collider[] nearbyCreatures = Physics.OverlapSphere(startPosition, range, ~targetMask);
        //remove duplicates
        List<Transform> nearbyCreatureRoots = new List<Transform>();
        foreach (Collider c in nearbyCreatures)
        {
            if (!nearbyCreatureRoots.Contains(c.transform.root))
                nearbyCreatureRoots.Add(c.transform.root);
        }
        
        if (numResults > nearbyCreatureRoots.Count)
        {
            Debug.Log("uh not enough people in the area");
        }
        //
        IList<EnemyController> sortedRoots = new List<EnemyController> { nearbyCreatureRoots[0].GetComponent<EnemyController>() };
        for (int a = 1; a < nearbyCreatureRoots.Count; a++)
        {
            for (int b = 0; b < sortedRoots.Count; b++)
            {
                if ((nearbyCreatureRoots[a].position - startPosition).sqrMagnitude < (sortedRoots[b].transform.position - startPosition).sqrMagnitude)
                {
                    if (b >= sortedRoots.Count - 1)
                        sortedRoots.Add(nearbyCreatureRoots[a].GetComponent<EnemyController>());
                    else
                        sortedRoots.Insert(b, nearbyCreatureRoots[a].GetComponent<EnemyController>());
                }
            }
            if (a >= numResults)
                break;
        }

        return sortedRoots.ToArray();
    }

    Transform GetClosestEnemy(Transform[] enemies)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in enemies)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }

    void PlayerPossessedBody() //from self to body
    {
        //start countdown to reset kill target;
        PlayerPossessingBody = true;
        KillTarget = null;

    }
    void PlayerSwappedBody() //from body to body
    {
        //start countdown to reset kill target;
        PlayerPossessingBody = true;
        ConfirmKillTarget();
    }

    void PlayerLeftBody()
    {
        //ResetWatchTarget();
        PlayerPossessingBody = false;
        //WantedLevel = 1;
        //KillTarget = WatchTarget; WantedLevel == 0 && 
        if (!HasKillTarget)
        {
            KillTarget = WatchTarget;
            //WantedLevel++;
        }
    }

    public void EnemyWounded(CharacterBody damageSource)
    {
        //Debug.Log(damageSource.CurrentController);

        if (damageSource.CurrentController == player)
        {
            Debug.Log("dsfsd");
            //WantedLevel++;
            KillTarget = WatchTarget;
        }
        else
        {
            //damageSource.CurrentController.GetComponent<EnemyController>().DecreaseStunDuration();
            //increase sobriety of grunt
            //grunt killing ends when grunt is no longer stunned
        }
    }

    public void EnemyDied(EnemyController newCorpse)
    {
        //Debug.Log(newCorpse);
        if (CurrentEnemies.Contains(newCorpse))
            CurrentEnemies.Remove(newCorpse);

        CanvasManager.CM.questManager.EnemyKilled(newCorpse.CurrentBody.bodyType);

        //BodyKilled();
        //EnemyKilled(newCorpse.myType);



    }

    public void EnemyRecoveredFromStun(EnemyController target)
    {
        if (target == KillTarget)
            KillTarget = null;
        ConfirmKillTarget();
    }
}

[System.Serializable]
public class AIEmotes
{
    public Sprite blank;
    public Sprite fighting;
    public Sprite following;
    public Sprite fleeing;
    public Sprite raging;
    public Sprite stunned;
    public Sprite thinking;

    public Sprite GetEmote(AIEmotions emotion)
    {
        switch (emotion)
        {
            case (AIEmotions.fight):
                return fighting;
            case (AIEmotions.follow):
                return following;
            case (AIEmotions.flee):
                return fleeing;
            case (AIEmotions.rage):
                return raging;
            case (AIEmotions.stunned):
                return stunned;
            case (AIEmotions.thinking):
                return thinking;
            default:
                return blank;
        }
        return blank;
    }
}
