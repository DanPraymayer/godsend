﻿using UnityEngine;
using System.Collections;

public class PlayAnimOnKeyUp : MonoBehaviour {

    public GameObject mainProjectile;
    public ParticleSystem mainParticle;

    public Transform spawnPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetKeyUp(KeyCode.Space)){
            mainProjectile.SetActive(true);
        }

        if(mainParticle.IsAlive() == false)
        {
            mainProjectile.SetActive(false);
        }
	}
}
