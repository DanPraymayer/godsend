﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugGameSceneManager : MonoBehaviour
{

    InGameUIListener UIListener;
    public KeyCode PauseKey = KeyCode.Escape, HelpKey = KeyCode.F1;
    public KeyCode SlowTimeKey = KeyCode.LeftBracket, FastTimeKey = KeyCode.RightBracket;
    public KeyCode ReloadScene = KeyCode.Backslash, Suicide = KeyCode.Delete;
    public static bool DebugPaused { private set; get; }

    void Start()
    {
        if (GameSceneManager.GSM != null || !Application.isEditor)
            this.enabled = false;

        UIListener = GameObject.FindObjectOfType<InGameUIListener>().GetComponent<InGameUIListener>();
    }

    void Update()
    {
        if (Input.GetKeyDown(HelpKey))
        {
            UIListener.ToggleHelpScreen();
        }
        DebugControlChecks();
    }

    void DebugControlChecks()
    {
        {
            /*if (Input.GetKeyDown(KeyCode.O)) //set speed to normal
            {
                Time.timeScale = 1;
                DebugPaused = false;
            }
            else if (Input.GetKeyDown(KeyCode.P)) //Pause and unpause
            {
                if (Time.timeScale == 0)
                {
                    Time.timeScale = 1;
                    DebugPaused = false;
                }
                else
                {
                    Time.timeScale = 0;
                    DebugPaused = true;
                }
            }*/
            if (Input.GetKeyDown(PauseKey))
            {
                Debug.Break();
            }
            else if (Input.GetKeyDown(Suicide))
            {
                SpawnPointManager.SPM.RequestRespawnPoint(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>());
            }
            else if(Input.GetKeyDown(SlowTimeKey)) // 1/3 speed
            {
                if (Time.timeScale == 0.3f)
                    Time.timeScale = 1;
                else
                    Time.timeScale = 0.3f;
            }
            else if (Input.GetKeyDown(FastTimeKey)) //2x speed
            {
                if (Time.timeScale == 2)
                    Time.timeScale = 1;
                else
                    Time.timeScale = 2;
            }
            else if (Input.GetKeyDown(ReloadScene)) //reload level
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}
