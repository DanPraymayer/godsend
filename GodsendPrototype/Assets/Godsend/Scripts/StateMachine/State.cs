﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class State{

    public State() { }

    public virtual void UpdateState(StateMachine stateMachine, PlayerInput input) { }

    public virtual void UpdateState(StateMachine stateMachine) { }

    public virtual void Enter(StateMachine stateMachine) { }

    public virtual void Exit(StateMachine stateMachine) { }

}
