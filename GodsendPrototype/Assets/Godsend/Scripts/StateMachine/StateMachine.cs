﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine{

    protected Dictionary<int, State> states;

    public State currentState { get; protected set; }
    public State previousState { get; protected set; }

    public virtual void Update(PlayerInput input) { currentState.UpdateState(this, input); }

    public virtual void Update() { currentState.UpdateState(this); }

    public virtual void ChangeState(State nextState, State oldState)
    {
        currentState = nextState;
        previousState = oldState;

        currentState.Enter(this);
        oldState.Exit(this);
    }

    public virtual void ReturnToPreviousState(State nextPreviousState)
    {
        this.ChangeState(previousState, nextPreviousState);
    }

    public virtual State GetState(int key){ return states[key]; }
}
