﻿using System.Collections;
using System;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpawnableBodies))] //enemy manager
public class SpawnableBodiesEditor : Editor
{
    SpawnableBodies spawnableBodies;
    bool showTypeAssignments;
    //UnityObject a = null;
    //GameObject[] editorPossiblePrefabs;

    private void OnEnable()
    {
        spawnableBodies = (SpawnableBodies)target;

        if (spawnableBodies.prefabs.Length < Enum.GetValues(typeof(BodyType)).Length) {
            GameObject[] ggagds = new GameObject[Enum.GetValues(typeof(BodyType)).Length];
            for (int i = 0; i < spawnableBodies.prefabs.Length; i++)
            {
                ggagds[i] = spawnableBodies.prefabs[i];
            }
            spawnableBodies.prefabs = ggagds;
        }
    }

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        EditorGUILayout.LabelField("Spawnable Enemy prefabs");
        if (spawnableBodies != null)
        {
            for (int i = 0; i < Enum.GetValues(typeof(BodyType)).Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                BodyType b = (BodyType)Enum.GetValues(typeof(BodyType)).GetValue(i);
                EditorGUILayout.EnumPopup(b);
                //EnemyTypeInfoByReference[b]
                //if (EnemyTypeInfoByReference.ContainsKey(b))
                //EnemyTypeInfoByReference[b]
                spawnableBodies.prefabs[i] = (GameObject)EditorGUILayout.ObjectField("", spawnableBodies.prefabs[i], typeof(GameObject), false);


                //else
                //    editorPossiblePrefabs[i] = (GameObject)EditorGUILayout.ObjectField("", editorPossiblePrefabs[i], typeof(GameObject), false);

                // if (cachedItems[i] != null)
                //    EnemyTypeInfoByReference[b] = cachedItems[i];

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}
