﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour {
    public BodyType triggerType;
    public QuestData questData;

    public bool questGiven;

    private void OnTriggerEnter(Collider other)
    {
        if (!questGiven)
        {
            PlayerController player = other.GetComponentInParent<PlayerController>();

            if (player != null && player.CurrentBody.bodyType == triggerType)
            {
                questGiven = true;
                CanvasManager.CM.questManager.AddSidequest(questData);

            }
        }
    }

}
