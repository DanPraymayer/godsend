﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager GSM;

    [SerializeField]

    bool usingLoadingScreen;
    public MenuSceneScript.GameMenuCodeName StartingMenu = MenuSceneScript.GameMenuCodeName.MainMenu;
    public LevelSceneReferences LevelSceneAsset;

    public KeyCode PauseKey = KeyCode.Escape, HelpKey = KeyCode.F1;

    List<int> LoadedSceneIndicies = new List<int>();

    public bool InGame { get; private set; }
    public static bool GamePaused { get; private set; }
    public static bool HelpShowing { get; private set; }
    public delegate void UIEvent();
    public static event UIEvent ToggleHelpScreen;

    private void Awake()
    {
        if (GSM != null)
        {
            Debug.LogWarning("Duplicate level manager removed", gameObject);
            this.enabled = false;
        }
        else
        {
            GSM = this;
            DontDestroyOnLoad(this.transform);
        }
    }

    private void Start()
    {
        if (LevelSceneAsset == null)
            Debug.LogError("Scene management asset not assigned to game scene manager.");

        if (LevelSceneAsset.GameScene_Current.BuildIndex < 0)
            for (int i = 0; i < LevelSceneAsset.GameplayScenes.Count; i++)
            {
                if (LevelSceneAsset.GameplayScenes[i].BuildIndex > 0)
                {
                    LevelSceneAsset.GameScene_Current = ValidateScene(LevelSceneAsset.GameplayScenes[i]);
                    break;
                }
            }

        SceneManager.LoadScene(LevelSceneAsset.MenuScene.SceneName);
    }

    SceneInfo ValidateScene(SceneInfo checkScene)
    {
        if (checkScene.BuildIndex > 0)
            return checkScene;
        else
            return null;
    }

    private void Update()
    {
        if (InGame)
        {
            if (GamePaused)
            {
                if (Input.GetKeyDown(PauseKey)) //UnPause game
                {
                    Time.timeScale = 1;
                    DropScene(LevelSceneAsset.MenuScene.SceneName);
                    GamePaused = false;
                }
            }
            else if(HelpShowing)
            {
                if (Input.GetKeyDown(HelpKey)) //Hide help screen
                {
                    Time.timeScale = 1;
                    HelpShowing = false;
                    if (ToggleHelpScreen != null)
                        ToggleHelpScreen();
                }
            }
            else
            {
                if (Input.GetKeyDown(PauseKey)) //Pause game
                {
                    Time.timeScale = 0;
                    StartingMenu = MenuSceneScript.GameMenuCodeName.PauseMenu;
                    AddScene(LevelSceneAsset.MenuScene.SceneName);
                    GamePaused = true;
                }
                if (Input.GetKeyDown(HelpKey)) //Show help screen
                {
                    Time.timeScale = 0;
                    HelpShowing = true;
                    if (ToggleHelpScreen != null)
                        ToggleHelpScreen();
                }
            }
        }
    }

    /*
    *
    * Load scenes
    * 
    */

    public void LoadSpecificGameplayLevel(int levelIndexInArray)
    {
        if (levelIndexInArray < 0 || levelIndexInArray > LevelSceneAsset.GameplayScenes.Count)
            return;

        LevelSceneAsset.GameScene_Current = ValidateScene(LevelSceneAsset.GameplayScenes[levelIndexInArray]);
        LoadGameplay();
    }

    //current system to load a scene with a loading screen
    public void LoadGameplay()
    {
        usingLoadingScreen = true;
        InGame = true;
        SceneManager.LoadScene(LevelSceneAsset.LoadingScene.SceneName);
        StartCoroutine(LoadYourAsyncScene(LevelSceneAsset.GameScene_Current.SceneName));
    }


    #region LoadScenesAsyncronously
    //load scenes without a loading screen
    public void AddScene()
    {
        AddScene(LevelSceneAsset.GameScene_Current.SceneName);
    }

    public void AddScene(string sceneName)
    {
        StartCoroutine(LoadYourAsyncScene(sceneName));
    }

    IEnumerator LoadYourAsyncScene(string sceneName)
    {
        //Coroutine sourced from http://blog.teamtreehouse.com/make-loading-screen-unity
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the scene by build //number.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        LoadedSceneIndicies.Add(SceneManager.GetSceneByName(sceneName).buildIndex);

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        if (usingLoadingScreen)
        {
            DropScene(LevelSceneAsset.LoadingScene.SceneName);
            usingLoadingScreen = false;
        }
    }
    #endregion

    /*
     *
     * Unload scenes
     * 
     */

    //out of the list of currently loaded scenes, unload the most recently loaded one
    public void UnloadLastScene()
    {
        if (LoadedSceneIndicies.Count > 0)
            DropScene(SceneManager.GetSceneByBuildIndex(LoadedSceneIndicies[LoadedSceneIndicies.Count - 1]));
        else
            Debug.Log("idk fam");
    }

    #region Unload Scenes Asyncronisously
    //overload method for Unload Scene, so you can reference via Scene name instead of scene reference
    public void DropScene(string sceneName)
    {
        DropScene(SceneManager.GetSceneByName(sceneName));
    }

    //start async load via coroutine
    public void DropScene(Scene sceneName)
    {
        StartCoroutine(UnLoadYourAsyncScene(sceneName));
    }

    IEnumerator UnLoadYourAsyncScene(Scene sceneName)
    {
        //Coroutine sourced from http://blog.teamtreehouse.com/make-loading-screen-unity
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the scene by build //number.
        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(sceneName);
        LoadedSceneIndicies.RemoveAt(LoadedSceneIndicies.Count - 1);

        //Wait until the last operation fully loads to return anything
        while (!asyncUnload.isDone)
        {
            yield return null;
        }
    }
    #endregion

    public void StopGameplay()
    {
        DropScene(LevelSceneAsset.GameScene_Current.SceneName);
        GamePaused = false;
        InGame = false;
    }
}


