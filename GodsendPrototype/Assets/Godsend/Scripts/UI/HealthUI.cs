﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    public void UpdateHealth(float percent)
    {
        CanvasManager.CM.movesetUI.health.fillAmount = percent;
    }

    public void HideArmour()
    {
        CanvasManager.CM.movesetUI.armour.fillAmount = 0;
    }

    public void UpdateArmour(float percent)
    {
        CanvasManager.CM.movesetUI.armour.fillAmount = percent;
    }

}
