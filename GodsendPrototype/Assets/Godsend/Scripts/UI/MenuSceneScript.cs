﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSceneScript : MonoBehaviour
{
    //allowed menu screens
    public enum GameMenuCodeName
    {
        MainMenu = 0,
        SaveMenu = 1,
        LoadMenu = 2,
        PauseMenu = 3,
        LvlSelectMenu = 4
    }

    [System.Serializable]
    public struct GameMenuInfo
    {
        public GameMenuCodeName MenuCodeName;
        public GameObject MenuObject;
    }

    //list of menu screens and their related game objects
    [SerializeField]
    GameMenuInfo[] gameMenuList;

    //Reference the menu's game object based on the menu enum
    //currently rebuilt every time the menu loads, not sure if that should be changed;
    Dictionary<GameMenuCodeName, GameObject> gameMenuDictionary = new Dictionary<GameMenuCodeName, GameObject>();

    GameMenuCodeName previousMenu;

    void Start()
    {
        for (int i = 0; i < gameMenuList.Length; i++)
        {
            gameMenuDictionary.Add(gameMenuList[i].MenuCodeName, gameMenuList[i].MenuObject);
        }

        HideAllMenus();
        ShowMenu(GameSceneManager.GSM.StartingMenu);
    }

    //Hide all menu objects so the canvas shows nothing
    void HideAllMenus()
    {
        for (int i = 0; i < gameMenuList.Length; i++)
        {
            gameMenuList[i].MenuObject.SetActive(false);
        }
    }

    //show the selected menu
    public void ShowMenu(GameMenuCodeName codeName)
    {
        gameMenuDictionary[codeName].SetActive(true);
    }

    //load the game scene
    public void LoadMainGame()
    {
        GameSceneManager.GSM.LoadGameplay();
    }

    public void QuitGame()
    {
        Debug.Break();
        Application.Quit();
    }

    public void LoadGameplayLevelByIndex(int numberInArray)
    {
        GameSceneManager.GSM.LoadSpecificGameplayLevel(numberInArray);
    }
}
