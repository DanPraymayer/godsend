﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjectOnStart : MonoBehaviour {
    void Start()
    {
        gameObject.SetActive(false);
    }
}
