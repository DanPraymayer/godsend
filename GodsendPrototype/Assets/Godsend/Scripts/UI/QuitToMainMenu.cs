﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitToMainMenu : MonoBehaviour
{
    //When in game, unload game scene
    public void QuitGame()
    {
        GameSceneManager.GSM.StopGameplay();
    }
}
