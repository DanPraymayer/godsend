﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameUIListener : MonoBehaviour
{
    public GameObject HelpScreen;
    public bool HelpScreenActive
    {
        get
        {
            return HelpScreen != null ? HelpScreen.activeInHierarchy : false;
        }
        set
        {
            if (HelpScreen != null)
            {
                if (HelpScreen.activeInHierarchy)
                {
                    HelpScreen.SetActive(false);
                    Time.timeScale = 1;
                }
                else
                {
                    HelpScreen.SetActive(true);
                    Time.timeScale = 1;
                }
            }
        }
    }

    private void Start()
    {
        GameSceneManager.ToggleHelpScreen += ToggleHelpScreen;

        HelpScreen.SetActive(false);
    }

    public void ToggleHelpScreen()
    {
        if (Application.isEditor)
        {
            if (HelpScreen != null)
            {
                if (HelpScreen.activeInHierarchy)
                {
                    HelpScreen.SetActive(false);
                    Time.timeScale = 1;
                }
                else
                {
                    HelpScreen.SetActive(true);
                    Time.timeScale = 0;
                }
            }
        }
        else
        {
            if (GameSceneManager.HelpShowing)
            {
                if (HelpScreen != null)
                    HelpScreen.SetActive(true);
            }
            else
            {
                if (HelpScreen != null)
                    HelpScreen.SetActive(false);
            }
        }
    }
}
