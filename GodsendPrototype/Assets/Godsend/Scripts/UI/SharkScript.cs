﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkScript : MonoBehaviour {

    public bool Haunting;
    public float roti;
    SpriteRenderer sharkSprite;
    UnityEngine.UI.Image sharkImage;
    Transform player;
    Vector2 destin;
    Rigidbody2D myRigi;
    public float speed = 2;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        myRigi = GetComponent<Rigidbody2D>();
        destin = transform.position;
        sharkSprite = GetComponent<SpriteRenderer>(); //sharkImage = GetComponent<UnityEngine.UI.Image>();

        TurnOffGhost();
    }

   public void TurnOnGhost()
    {
        Haunting = true;
        sharkSprite.enabled = true;
        //sharkImage.enabled = true;
        destin = player.position + new Vector3(Random.Range(-7, 7), Random.Range(-7, 7));
    }

   public void TurnOffGhost()
    {
        Haunting = false;
        myRigi.velocity = Vector2.zero;
        sharkSprite.enabled = false;
        //sharkImage.enabled = false;
    }

    void Update () {
        if (Haunting)
        {
            roti = transform.rotation.eulerAngles.z % 360;
            if (roti > 90 && roti < 270)
                transform.localScale = new Vector3(1,1,-1);
            else
                transform.localScale = Vector3.one;

            if (Vector2.Distance(transform.position, destin) < 0.2f)
            {
                destin = player.position + new Vector3(Random.Range(-7, 7), Random.Range(-7, 7));
                transform.right = (destin - (Vector2)transform.position);
            }
            else
            {
                myRigi.velocity = (destin - (Vector2)transform.position).normalized * speed;
            }
        }
	}
}
