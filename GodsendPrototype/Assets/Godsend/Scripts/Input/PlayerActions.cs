﻿using InControl;
using UnityEngine;

public class PlayerActions : PlayerActionSet{

    public PlayerAction Attack;
    public PlayerAction Attack2;
    public PlayerAction Jump;
    public PlayerAction SoulShift;
    public PlayerAction ExitBody;
    public PlayerAction SlowTime;

    public PlayerAction MoveUp;
    public PlayerAction MoveDown;
    public PlayerAction MoveRight;
    public PlayerAction MoveLeft;

    public PlayerAction AimUp;
    public PlayerAction AimDown;
    public PlayerAction AimRight;
    public PlayerAction AimLeft;

    public PlayerTwoAxisAction Move;
    public PlayerTwoAxisAction Aim;

    public PlayerActions()
    {

        Attack = CreatePlayerAction("Fire");
        Attack2 = CreatePlayerAction("Fire2");
        Jump = CreatePlayerAction("Jump");
        SoulShift = CreatePlayerAction("Soulshift");
        ExitBody = CreatePlayerAction("Exitbody");
        SlowTime = CreatePlayerAction("SlowTime");

        MoveUp = CreatePlayerAction("MoveUp");
        MoveDown = CreatePlayerAction("MoveDown");
        MoveLeft = CreatePlayerAction("MoveLeft");
        MoveRight = CreatePlayerAction("MoveRight");

        AimUp = CreatePlayerAction("AimUp");
        AimDown = CreatePlayerAction("AimDown");
        AimLeft = CreatePlayerAction("AimLeft");
        AimRight = CreatePlayerAction("AimRight");

        Move = CreateTwoAxisPlayerAction(MoveLeft, MoveRight, MoveDown, MoveUp);
        Aim = CreateTwoAxisPlayerAction(AimLeft, AimRight, AimDown, AimUp);
    }


    public static PlayerActions CreateWithDefaultBindings()
    {
        var playerActions = new PlayerActions();

        playerActions.Attack.AddDefaultBinding(Mouse.LeftButton);
        playerActions.Attack.AddDefaultBinding(InputControlType.Action3);

        playerActions.Attack2.AddDefaultBinding(Key.E);
        //playerActions.Attack.AddDefaultBinding(InputControlType.Action3);

        playerActions.Jump.AddDefaultBinding(Key.Space);
        playerActions.Jump.AddDefaultBinding(InputControlType.Action1);

        playerActions.SoulShift.AddDefaultBinding(Mouse.RightButton);
        playerActions.SoulShift.AddDefaultBinding(InputControlType.LeftBumper);

        playerActions.ExitBody.AddDefaultBinding(Key.LeftShift);
        playerActions.ExitBody.AddDefaultBinding(InputControlType.Action4);

        //playerActions.SlowTime.AddDefaultBinding(Key.LeftShift);
        //playerActions.SlowTime.AddDefaultBinding(InputControlType.LeftTrigger);



        playerActions.MoveUp.AddDefaultBinding(Key.W);
        playerActions.MoveUp.AddDefaultBinding(InputControlType.LeftStickUp);

        playerActions.MoveDown.AddDefaultBinding(Key.S);
        playerActions.MoveDown.AddDefaultBinding(InputControlType.LeftStickDown);

        playerActions.MoveLeft.AddDefaultBinding(Key.A);
        playerActions.MoveLeft.AddDefaultBinding(InputControlType.LeftStickLeft);

        playerActions.MoveRight.AddDefaultBinding(Key.D);
        playerActions.MoveRight.AddDefaultBinding(InputControlType.LeftStickRight);

        
        playerActions.AimUp.AddDefaultBinding(InputControlType.RightStickUp);

        playerActions.AimDown.AddDefaultBinding(InputControlType.RightStickDown);

        playerActions.AimLeft.AddDefaultBinding(InputControlType.RightStickLeft);

        playerActions.AimRight.AddDefaultBinding(InputControlType.RightStickRight);


        playerActions.ListenOptions.IncludeUnknownControllers = true;
        playerActions.ListenOptions.MaxAllowedBindings = 4;


        playerActions.ListenOptions.OnBindingFound = (action, binding) => {
            if (binding == new KeyBindingSource(Key.Escape))
            {
                action.StopListeningForBinding();
                return false;
            }
            return true;
        };

        playerActions.ListenOptions.OnBindingAdded += (action, binding) => {
            Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
        };

        playerActions.ListenOptions.OnBindingRejected += (action, binding, reason) => {
            Debug.Log("Binding rejected... " + reason);
        };

        return playerActions;
    }
}

