﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCommands : CommandManager {


    PlayerActions m_playerActions;

    public PlayerActions PlayerActions { get { return m_playerActions; } }


    public PlayerCommand Attack;


    public PlayerCommand MoveUp;
    public PlayerCommand MoveDown;

    public PlayerCommand AttackDown;
    public PlayerCommand AttackUp;
     


    PlayerCommands()
    {

        Attack = CreateNewCommand();

        MoveUp = CreateNewAxisCommand();
        MoveDown = CreateNewAxisCommand();

        AttackDown = CreateNewCommand();//CreateNewChord();
        AttackUp = CreateNewChord();
    }

    public static PlayerCommands CreateCommands()
    {

        var playerCommands = new PlayerCommands();


        playerCommands.m_playerActions = PlayerActions.CreateWithDefaultBindings();
        PlayerActions playerActions = playerCommands.m_playerActions;

        playerCommands.Attack.AddInputBinding(playerActions.Attack);

        playerCommands.MoveUp.AddAxisBinding(playerActions.MoveUp);
        playerCommands.MoveDown.AddAxisBinding(playerActions.MoveDown);

        playerCommands.AttackUp.AddCommandBinding(playerCommands.Attack);
        playerCommands.AttackUp.AddCommandBinding(playerCommands.MoveUp);

        playerCommands.AttackDown.AddInputBinding(playerActions.Attack2);//.AddCommandBinding(playerCommands.AttackDown);
        //playerCommands.AttackDown.AddCommandBinding(playerCommands.MoveDown);

        return playerCommands;

    }


}
