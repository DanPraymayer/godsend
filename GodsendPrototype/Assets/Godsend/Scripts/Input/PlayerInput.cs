﻿using System.Collections;
using System.Collections.Generic;
using InControl;
using UnityEngine;



public class PlayerInput
{

    PlayerController m_Player;

    PlayerActions m_playerActions;

    public Vector2 movementAxis;

    public float angleToAim;
    public Vector3 dirToAim;
     

    public PlayerAction Attack { get { return m_playerActions.Attack; } }
    public PlayerAction Attack2 { get { return m_playerActions.Attack2; } }

    public bool HeldDown { get { return Move.Y < -.9; } }

    public PlayerAction Jump { get { return m_playerActions.Jump; } }
    public PlayerAction SoulShift { get { return m_playerActions.SoulShift; } }
    public PlayerAction ExitBody { get { return m_playerActions.ExitBody; } }
    public PlayerAction SlowTime { get { return m_playerActions.SlowTime; } }

    public PlayerTwoAxisAction Move { get { return m_playerActions.Move; } }
    public PlayerTwoAxisAction Aim { get { return m_playerActions.Aim; } }

    public BindingSourceType LastInputType { get { return m_playerActions.LastInputType; } }

    public PlayerInput(PlayerController player)
    {
        m_Player = player;

        m_playerActions = PlayerActions.CreateWithDefaultBindings();
    }
    public void Update()
    {


        movementAxis = m_playerActions.Move;

        if(m_playerActions.LastInputType == InControl.BindingSourceType.DeviceBindingSource)
        {
            dirToAim = m_playerActions.Aim;

            angleToAim = Mathf.Atan2(m_playerActions.Aim.Y, m_playerActions.Aim.X) * Mathf.Rad2Deg;
        }
        else
        {
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 10f);

            dirToAim = mouseWorldPosition - m_Player.HookPos;

            angleToAim = AngleBetweenPoints(m_Player.HookPos, mouseWorldPosition);
        }

    }

    float AngleBetweenPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

}
