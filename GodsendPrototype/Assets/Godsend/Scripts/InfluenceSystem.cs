﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfluenceSystem : MonoBehaviour {

    public Group[] groups;
    public List<Event> events = new List<Event>();

    int eventTimer;
    public int eventTime;

    float populationChangeTimer;
    public float populationChangeTime;

    public int lastingEventTime; 

    public int startingInfluence;
    public int startingPopulation;

    int numOfAliveGroups;

    public Text groupOneText;
    public Text groupTwoText;
    public Text groupThreeText;

    public Text timeText;
    public Text popUpText;

    public GameObject popUpBox;

    string popUpTextTemp;

    // Use this for initialization
    void Start () {

        for(int i = 0; i < groups.Length; i++)
        {
            groups[i] = new Group();

            groups[i].number = i;
            groups[i].influence = startingInfluence;
            groups[i].influenceStage = 0;

            groups[i].smallEnemyPercentage = 90;
            groups[i].mediumEnemyPercentage = 10;
            groups[i].largeEnemyPercentage = 0;

            groups[i].population = startingPopulation;
        }

        groups[0].color = "red";
        groups[1].color = "blue";
        groups[2].color = "purple";

        eventTimer = eventTime;
	}

    // Update is called once per frame
    void Update(){

        groupOneText.text = "RED \n" +
                            "INFLUENCE : " + groups[0].influence + "\n" +
                            "POPULATION : " + groups[0].population + "\n" +
                            "POPULATION DISTRIBUTION : " + groups[0].smallEnemyPercentage + "% - " + groups[0].mediumEnemyPercentage + "% - " + groups[0].largeEnemyPercentage + "%";

        groupTwoText.text = "BLUE \n" +
                    "INFLUENCE : " + groups[1].influence + "\n" +
                    "POPULATION : " + groups[1].population + "\n" +
                    "POPULATION DISTRIBUTION : " + groups[1].smallEnemyPercentage + "% - " + groups[1].mediumEnemyPercentage + "% - " + groups[1].largeEnemyPercentage + "%";

        groupThreeText.text = "PURPLE \n" +
                    "INFLUENCE : " + groups[2].influence + "\n" +
                    "POPULATION : " + groups[2].population + "\n" +
                    "POPULATION DISTRIBUTION : " + groups[2].smallEnemyPercentage + "% - " + groups[2].mediumEnemyPercentage + "% - " + groups[2].largeEnemyPercentage + "%";

        timeText.text = "" + eventTimer;
    }

    private void FixedUpdate()
    {
        populationChangeTimer += Time.deltaTime;
        if(populationChangeTimer >= populationChangeTime)
        {
            PopulationChange(GetRandomGroup(), Random.Range(0,2),true, 1, 4);
            populationChangeTime = 0;
        }
    }

    public void ReduceTime()
    {
        eventTimer--; 

        if(eventTimer <= 0)
        {
            eventTimer = eventTime;
            RandomizeEvent();
        }

        if (events.Count > 0)
        {
            for (int i = 0; i < events.Count; i++)
            {
                events[i].time--;
                if(events[i].time <= 0)
                {
                    DecreaseAggro(events[i].group);
                    events.RemoveAt(i);
                    CheckPopulationDistribution();
                }
            }
        }
    }

    void RandomizeEvent()
    {
        numOfAliveGroups = groups.Length;
        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].isDead)
            {
                numOfAliveGroups--;
            }
        }

        if (numOfAliveGroups > 1)
        {
            int randomEvent = Random.Range(0,5);

            switch (randomEvent)
            {
                //watchtower taken over
                case 0:
                    int randomTower = GetRandomGroup();
                    Group otherTower = GetOtherGroups(groups[randomTower], 1)[0];
                    popUpText.text = groups[randomTower].color + "'s watchtower has now been taken over by the " + otherTower.color + "'s";

                    popUpBox.SetActive(true);

                    break;

                //influence change
                case 1:
                    int randomInfluenceValue = Random.Range(5, 11);
                    int largeChange = Random.Range(0, 101);
                    if (largeChange > 95){
                        randomInfluenceValue = Random.Range(25, 36);
                    }

                    ReduceInfluence(GetRandomGroup(), randomInfluenceValue);
                    CheckPopulationDistribution();

                    popUpText.text = popUpTextTemp;

                    popUpBox.SetActive(true);

                    break;
                
                //increase/decrease in medium/large enemies
                case 2:
                    int randomGroup = GetRandomGroup();
                    int randomChangeValue = Random.Range(3, 7);
                    int randomChoice = Random.Range(0, 2);

                    int randomPopulationType;
                    if(randomChoice == 0){
                        if (groups[randomGroup].mediumEnemyPercentage > 0){
                            randomPopulationType = Random.Range(0, 2);
                        }
                        else{
                            randomPopulationType = 0;
                        }
                    }

                    else{
                        if (groups[randomGroup].largeEnemyPercentage > 0){
                            randomPopulationType = Random.Range(2, 4);
                        }
                        else{
                            randomPopulationType = 2;
                        }
                    }

                    PopulationEvent newPopulationEvent = new PopulationEvent();
                    newPopulationEvent.group = randomGroup;
                    newPopulationEvent.type = randomPopulationType;
                    newPopulationEvent.changeValue = randomChangeValue;
                    newPopulationEvent.time = lastingEventTime;

                    events.Add(newPopulationEvent);

                    ChangePopulationDistribution(groups[newPopulationEvent.group]);
                    
                    popUpBox.SetActive(true);

                    break;

                case 3:
                    int randomAggroType = Random.Range(0, 3);

                    AggroEvent newAggroEvent = new AggroEvent();
                    newAggroEvent.group = GetRandomGroup();
                    newAggroEvent.type = randomAggroType;
                    newAggroEvent.time = lastingEventTime;

                    events.Add(newAggroEvent);
                    IncreaseAggro();

                    popUpBox.SetActive(true);
                    break;

                case 4:
                    PopulationChange(GetRandomGroup(), Random.Range(0, 2), true, 50, 100);
                    popUpBox.SetActive(true);
                    break; 


            }
        }
    }

    int GetRandomGroup()
    {
        int randomGroup = Random.Range(0, 3);
        while (groups[randomGroup].isDead)
        {
            randomGroup = Random.Range(0, 3);
        }

        return randomGroup;
    }

    List<Group> GetOtherGroups(Group group, int numOfGroups)
    {
        List<Group> newGroup = new List<Group>();

        for(int i = 0; i < groups.Length; i++)
        {
            if(group.number != groups[i].number)
            {
                newGroup.Add(groups[i]);
            }
        }

        List<Group> aggroGroup = new List<Group>();

        for(int i = 0; i < numOfGroups; i++)
        {
            int randomGroup = Random.Range(0, newGroup.Count);

            aggroGroup.Add(newGroup[randomGroup]);
            newGroup.RemoveAt(randomGroup);
        }

        return aggroGroup;
    }

    void PopulationChange(int groupNumber, int type, bool isRandom, int minValue, int maxValue)
    {
        if(isRandom)
        {
            int randomChance = Random.Range(0, 100);
            if (randomChance < 5) {
                if (type == 1)
                {
                    int randomValue = Random.Range(minValue, maxValue);
                    groups[groupNumber].population += randomValue;
                }
                else
                {
                    int randomValue = Random.Range(minValue, maxValue);
                    groups[groupNumber].population -= randomValue;
                }
            }
        }
        else
        {
            if (type == 0)
            {
                int randomValue = Random.Range(minValue, maxValue);
                groups[groupNumber].population += randomValue;
                popUpText.text = groups[groupNumber].color + "'s population has gone up by " + randomValue;
            }
            else
            {
                int randomValue = Random.Range(minValue, maxValue);
                groups[groupNumber].population -= randomValue;
                popUpText.text = groups[groupNumber].color + "'s population has gone down by " + randomValue;
            }
        }
    }

    void DecreaseAggro(int groupNumber)
    {
        groups[groupNumber].aggroGroups.Clear();
        groups[groupNumber].AggroPlayer = false;
    }

    void IncreaseAggro()
    {
        bool isAggroEvent = false;
        for(int i = 0; i < events.Count; i++)
        {
            if(events[i] is AggroEvent)
            {
                isAggroEvent = true;
            }
        }

        if (isAggroEvent)
        {
            foreach (AggroEvent aggroEvent in events)
            {
                switch (aggroEvent.type)
                {
                    case 0:
                        groups[aggroEvent.group].aggroGroups = GetOtherGroups(groups[aggroEvent.group], 2);
                        groups[aggroEvent.group].AggroPlayer = true;
                        popUpText.text = groups[aggroEvent.group].color + "'s will now attack everyone! watch out.";

                        break;

                    case 1:
                        groups[aggroEvent.group].aggroGroups = GetOtherGroups(groups[aggroEvent.group], 1);
                        popUpText.text = groups[aggroEvent.group].color + "'s will now attack " + groups[aggroEvent.group].aggroGroups[0].color + "'s";
                        break;

                    case 2:
                        groups[aggroEvent.group].aggroGroups = GetOtherGroups(groups[aggroEvent.group], 2);
                        popUpText.text = groups[aggroEvent.group].color + "'s will now attack the other gangs!";
                        break;
                }
            }
        }
    }

    void CheckPopulationDistribution()
    {
        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].influence <= 40)
            {
                groups[i].smallEnemyPercentage = 60;
                groups[i].mediumEnemyPercentage = 25;
                groups[i].largeEnemyPercentage = 15;
            }
            else if (groups[i].influence > 40 && groups[i].influence <= 75)
            {
                groups[i].smallEnemyPercentage = 80;
                groups[i].mediumEnemyPercentage = 15;
                groups[i].largeEnemyPercentage = 5;
            }
            else if (groups[i].influence > 75 && groups[i].influence <= 100)
            {
                groups[i].smallEnemyPercentage = 90;
                groups[i].mediumEnemyPercentage = 10;
                groups[i].largeEnemyPercentage = 0;
            }
            else if (groups[i].influence > 100 && groups[i].influence <= 140)
            {
                groups[i].smallEnemyPercentage = 80;
                groups[i].mediumEnemyPercentage = 15;
                groups[i].largeEnemyPercentage = 5;
            }
            else if (groups[i].influence > 140 && groups[i].influence <= 175)
            {
                groups[i].smallEnemyPercentage = 75;
                groups[i].mediumEnemyPercentage = 15;
                groups[i].largeEnemyPercentage = 10;
            }
            else if (groups[i].influence > 175 && groups[i].influence <= 225)
            {
                groups[i].smallEnemyPercentage = 70;
                groups[i].mediumEnemyPercentage = 20;
                groups[i].largeEnemyPercentage = 10;
            }
            else
            {
                groups[i].smallEnemyPercentage = 60;
                groups[i].mediumEnemyPercentage = 25;
                groups[i].largeEnemyPercentage = 15;
            }

            bool isPopulationChange = false;

            for (int j = 0; j < events.Count; j++)
            {
                if (events[j] is PopulationEvent)
                {
                    isPopulationChange = true;
                }
            }

            if (isPopulationChange)
            {
                ChangePopulationDistribution(groups[i]);
            }
        }

    }

    void ChangePopulationDistribution(Group group)
    {
        if (events != null && group.smallEnemyPercentage > 0)
        {
            foreach (PopulationEvent populationEvent in events)
            {
                int changeValue = populationEvent.changeValue;

                switch (populationEvent.type)
                {
                    case 0:
                        if (groups[populationEvent.group].mediumEnemyPercentage + changeValue > 100)
                        {
                            changeValue = 100 - groups[populationEvent.group].mediumEnemyPercentage;
                        }

                        groups[populationEvent.group].mediumEnemyPercentage += changeValue;
                        groups[populationEvent.group].smallEnemyPercentage -= changeValue;

                        popUpText.text = groups[populationEvent.group].color + "'s medium enemy population has gone up by " + changeValue + "\nsmall enemy population has gone down by " + changeValue;

                        break;

                    case 1:
                        if (groups[populationEvent.group].mediumEnemyPercentage - changeValue < 0)
                        {
                            changeValue = groups[populationEvent.group].mediumEnemyPercentage;
                        }

                        groups[populationEvent.group].mediumEnemyPercentage -= changeValue;
                        groups[populationEvent.group].smallEnemyPercentage += changeValue;

                        popUpText.text = groups[populationEvent.group].color + "'s medium enemy population has gone down by " + changeValue + "\nsmall enemy population has gone up by " + changeValue;

                        break;

                    case 2:
                        if (groups[populationEvent.group].largeEnemyPercentage + changeValue > 100)
                        {
                            changeValue = 100 - groups[populationEvent.group].largeEnemyPercentage;
                        }

                        groups[populationEvent.group].largeEnemyPercentage += changeValue;
                        groups[populationEvent.group].smallEnemyPercentage -= changeValue;

                        popUpText.text = groups[populationEvent.group].color + "'s large enemy population has gone up by " + changeValue + "\nsmall enemy population has gone down by " + changeValue;

                        break;

                    case 3:
                        if (groups[populationEvent.group].largeEnemyPercentage - changeValue < 0)
                        {
                            changeValue = groups[populationEvent.group].largeEnemyPercentage;
                        }
                        groups[populationEvent.group].largeEnemyPercentage -= changeValue;
                        groups[populationEvent.group].smallEnemyPercentage += changeValue;

                        popUpText.text = groups[populationEvent.group].color + "'s large enemy population has gone down by " + changeValue + "\nsmall enemy population has gone up by " + changeValue;

                        break;
                }
            }
        }
    
        for (int i = 0; i<groups.Length; i++)
        {
            if(groups[i].smallEnemyPercentage< 0){
                groups[i].smallEnemyPercentage = 0;
            }
            if (groups[i].smallEnemyPercentage > 100){
                groups[i].smallEnemyPercentage = 100;
            }
        }
    }

    void ReduceInfluence(int groupNum, int influenceValue)
    {
        groups[groupNum].influence -= influenceValue;
        groups[groupNum].isAffected = true;

        popUpTextTemp = groups[groupNum].color + "'s influence has gone down by " + influenceValue;

        PopulationChange(groupNum, 1, false, 5, 25);

        if (groups[groupNum].influence <= 0)
        {
            groups[groupNum].influence = 0;
            groups[groupNum].population = 0;
            groups[groupNum].isDead = true;
        }

        AddInfluence(influenceValue);
        groups[groupNum].isAffected = false;
    }

    void AddInfluence(int influenceValue)
    {
        if (numOfAliveGroups < 3)
        {
            int randomGroup = Random.Range(0, 3);
            while (groups[randomGroup].isAffected || groups[randomGroup].isDead)
            {
                randomGroup = Random.Range(0, 3);
            }

            groups[randomGroup].influence += influenceValue;

            popUpTextTemp += "\n" + groups[randomGroup].color + "'s influence has gone up by " + influenceValue;

            PopulationChange(randomGroup, 0, false, 5, 100);

            if (groups[randomGroup].influence > startingInfluence * 3)
            {
                groups[randomGroup].influence = startingInfluence * 3;
                groups[randomGroup].population = startingPopulation * 3;
            }
        }
        else
        {
            int remainingValue = Random.Range(0, influenceValue);
            for(int i = 0; i < groups.Length; i++)
            {
                if(!groups[i].isAffected)
                {
                    groups[i].influence += remainingValue;
                    popUpTextTemp += "\n" + groups[i].color + "'s influence has gone up by " + influenceValue;
                    PopulationChange(i, 0, false, 5, 100);
                    remainingValue = influenceValue - remainingValue;
                }
            }
        }
    }

    public void ClosePopUp()
    {
        popUpText.text = null;
        popUpBox.SetActive(false);
        popUpTextTemp = null;
    }

    [System.Serializable]
    public class Event
    {
        public int group;
        public int time;
    }

    [System.Serializable]
    public class PopulationEvent: Event
    {
        //0 - increase medium pop.
        //1 - decrease medium pop.
        //2 - increase large pop.
        //3 - decrease large pop.
        public int type;
        public int changeValue;
    }

    [System.Serializable]
    public class AggroEvent : Event
    {
        //0 - all aggro (attacks everything)
        //1 - specific aggro (attacks one group)
        //2 - dual aggro (attacks only the other groups)
        public int type;
    }
        [System.Serializable]
    public class Group
    {
        public int number;
        public int influenceStage;

        public string color;

        public int influence;
        public bool isDead;
        public bool isAffected;
        public bool AggroPlayer;

        public int smallEnemyPercentage;
        public int mediumEnemyPercentage;
        public int largeEnemyPercentage;
        
        public int population;

        public List<Group> aggroGroups = new List<Group>();
    }

}
