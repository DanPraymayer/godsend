﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonerMoveset : CharacterMoveset {

    RallyAttack m_normalAttack;
    public RallyAttack NormalAttack { get { return m_normalAttack; } }

    protected override void Start()
    {
        base.Start();

        m_normalAttack = GetComponentInChildren<RallyAttack>();

    }

    public override void TryNormalAttack(LayerMask targetLayer)
    {
        if (NormalAttack.CanAttack)
        {
            CurrentAttackType = AttackType.NormalAttack;
            m_normalAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }

}
