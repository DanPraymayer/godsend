﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMoveset : CharacterMoveset {

    MeleeAttack m_normalAttack;
    public MeleeAttack NormalAttack { get { return m_normalAttack; } }

    protected override void Start()
    {
        base.Start();
        m_normalAttack = GetComponentInChildren<MeleeAttack>();

    }

    public override void TryNormalAttack(LayerMask targetLayer)
    {
        if (NormalAttack.CanAttack)
        {
            CurrentAttackType = AttackType.NormalAttack;
            m_normalAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }

}
