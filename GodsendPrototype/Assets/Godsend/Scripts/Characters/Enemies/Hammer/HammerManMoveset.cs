﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerManMoveset : CharacterMoveset {


    MeleeAttack m_normalAttack;
    public MeleeAttack NormalAttack { get { return m_normalAttack; } }

    GroundSlamAttack m_downAttack;
    public GroundSlamAttack DownAttack { get { return m_downAttack; } }

    public override bool PrerequisitesMetForDownAttack { get { return m_downAttack.PillarIsAlive; } }
    public override Vector3 SpecialConsideration { get { return m_downAttack.PillarPosition; } }

    protected override void Start()
    {
        base.Start();
        m_normalAttack = GetComponentInChildren<MeleeAttack>();
        m_downAttack = GetComponentInChildren<GroundSlamAttack>();

    }

    public override void TryNormalAttack(LayerMask targetLayer)
    {
        if (m_normalAttack.CanAttack)
        {
            CurrentAttackType = AttackType.NormalAttack;
            m_normalAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }

    public override void TryDownAttack(LayerMask targetLayer)
    {
        if (m_downAttack.CanAttack)
        {
            CurrentAttackType = AttackType.DownAttack;
            m_downAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }
}
