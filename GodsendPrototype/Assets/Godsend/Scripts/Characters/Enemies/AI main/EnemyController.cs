﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public enum AIJumpStates
{
    notJumping,
    startingJump,
    inJump,
    endingJump,
    cantJump
}

public enum AIMoveStates
{
    notMoving,
    startedMoving,
    isMoving,
    stoppedMoving
}

public enum AITimedAttackStates
{
    notAttacking,
    startedAttacking,
    isAttacking
}

public enum AIEmotions
{
    none,
    fight,
    follow,
    flee,
    rage,
    stunned,
    thinking
}

[System.Serializable]
public class PossibleAttackStats
{
    public PossibleAttackStats(float delayBeforeStart, float durationOfAttack, float engagementRange, float delayAfterAttack)
    {
        windUp = delayBeforeStart;
        duration = durationOfAttack;
        range = engagementRange;
        cooldown = delayAfterAttack;
    }

    public float windUp, duration, range, cooldown;
}

public class EnemyController : CharacterController
{


    AIStateMachine m_AIStateMachine;
    AIPlaybook m_AIPlaybook;
    public bool HasBody { get; protected set; }
    //public EnemyType myType;
    [Tooltip("What this AI is classified as. Referenced so they can kill other AI. Should have an entry in the AllDenizens asset.")]
    public DenizenType startingDenizenType;
    DenizenInfo currentDenizenInfo;

    [Tooltip("Information about this AI's movement values for AI's decision making")]
    public AIBodyData BodyData;
    [Tooltip("HARDCODED: how close to effectively attack?")]
    public PossibleAttackStats CurrentAttackStats;
    [Tooltip("HARDCODED: how close is melee range")]
    public PossibleAttackStats MeleeAttackStats;

    //attacks
    //run to person, attack with hammer
    //run to pillar, attack with hammer

    //run to person, grab
    //run anywhere, grab


    //evaluate for people
    //check for thing, do thing
    //do normal attack


    //Vector3 jumpHeightLevel { get { return transform.position + BodyData.jumpHeight; } }

    #region Ground/vision checks
    Vector3 visionColliderEdgeForward { get { return new Vector2(Mathf.Abs(BodyData.visionCapsuleForward) * FaceDirection, 2); } }
    Vector3 visionColliderEdgeBackward { get { return new Vector2(-Mathf.Abs(BodyData.visionCapsuleBackward) * FaceDirection, 2); } }
    Vector3 EyePosition { get { return transform.position + BodyData.eyeBallHeight; } }
    public Vector3 DirectionalForward { get { return (Vector3.right * urgencyMultiplier) * FaceDirection; } }
    Ray forwardVisionRay { get { return new Ray(EyePosition, DirectionalForward); } }
    Ray wallCheckRay, holeCheckRay;
    #endregion

    GameObject targetPerson;
    public bool HasTarget { get; protected set; }
    bool CanAttackTarget, TargetIsNearby;
    public GameObject TargetPerson
    {
        get { return targetPerson; }
        set
        {
            HasTarget = (value != null);
            targetPerson = value;
            if (HasTarget)
            {
                string TargetLayerName = LayerMask.LayerToName(targetPerson.layer);
                TargetMask = LayerMask.GetMask(TargetLayerName);
                WallsGroundAndTargetMask = BodyData.ObstacleMask + LayerMask.GetMask(TargetLayerName);
            }
        }
    }
    //person to track non aggressively
    public GameObject TargetOfInterest { get; set; }

    [Tooltip("Based on the classification, what types should this character attack")]
    public DenizenType KillOnSightList;
    [Tooltip("Based on the classification, what types should this character attack when raging")]
    public DenizenType RageKillOnSightList;
    [Tooltip("Based on the classification, what types should this character flee from")]
    public DenizenType FleeOnSightList;
    [Tooltip("Based on the classification, what types should this character follow")]
    public DenizenType FollowOnSightList;
    [Tooltip("Should this AI attack the last person who injured them?")]
    public bool AutoRetailate = true;

    [Header("Movement")]
    Vector2 movementAxis; //Input axis
    public Vector2 MovementAxis
    {
        get { return movementAxis; }
        set
        {
            movementAxis = value;
            SetVisualFaceDirection();
            isWalking = movementAxis.x == 0 ? false : true;
        }
    }
    [Tooltip("HARDCODED: How fast should the AI run. Change in BodyData asset")]
    public float urgencyMultiplier = 1;
    public bool isWalking { get; protected set; }
    [Tooltip("OUTPUT: Walk progress information")]
    public AIMoveStates moveState;
    [Tooltip("OUTPUT: Jump information")]
    public AIJumpStates jumpState;
    [Tooltip("OUTPUT: Attack timers information")]
    public AITimedAttackStates attackState;
    float timedAttackStart;
    public SpriteRenderer emoteBubble;

    public bool WallIsInFront { get { return WallInFront(); } }
    public bool HoleIsInFront { get { return HoleInFront(); } }

    public bool IsActuallyGrounded
    {
        get
        {
            if (!HasBody)
                return false;
            else
            {
                bool x = CurrentBody.PlatformerMovement.PlatformerCollisions.IsGrounded;
                if (x)
                    if (jumpState == AIJumpStates.startingJump || jumpState == AIJumpStates.inJump)
                        jumpState = AIJumpStates.notJumping;
                return x;
            }
        }
    }
    [Tooltip("plz no change: used to get a better raycast angle")]
    public Vector3 AnInchHigherOffset = new Vector3(0, 0.4f);
    public float RandomPlusMinus { get { return Random.Range(0, 2) * 2 - 1; } }

    #region Layer masking
    public LayerMask TargetMask { get; private set; }
    LayerMask WallsGroundAndTargetMask;
    public LayerMask CreatureMask { get; private set; }
    public LayerMask WallsGroundAndCreatureMask { get; private set; }
    #endregion

    Vector3 targetPosition, lastTargetPosition;

    List<Transform> ignoredTargets = new List<Transform>();
    List<float> timeStampToRemoveTargets = new List<float>();
    float removeTargetDelay = 3;

    Coroutine moveRoutine, jumpRoutine, lineVisualizer;
    float hp;
    [Tooltip("Used so Daniel can get a better understanding of what this character is thinking")]
    public WIPVariables DebugVariables;

    [System.Serializable]
    public struct WIPVariables
    {
        public string CurrentState, PriorState;
        //editor info
        public TextMesh monolog;
        public bool DebugMode_drawLines;
        public bool DebugMode_drawGizmos;
        public bool DebugMode_outputStateChanges;
        public bool DebugMode_outputAlerts;
        //script progress variables
        public bool subscribedToEvents;
        //
        public float cachedTimerValue;
        public bool busyWalking;
        public bool NoRoam;
    }

    private void OnEnable()
    {
        SetEnemyManagerEventSubscriptions(true);
    }
    protected void OnDisable()
    {
        SetEnemyManagerEventSubscriptions(false);
        if (HasBody && CurrentBody.CharacterHealth.CurrentHealth <= 0)
            EnemyManager.EM.EnemyDied(this);
    }

    protected override void Start()
    {
        base.Start();
        CreatureMask = LayerMask.GetMask("Player", "Enemy");
        WallsGroundAndCreatureMask = CreatureMask + BodyData.ObstacleMask;
        m_AIStateMachine = new AIStateMachine(this);
        m_AIStateMachine.ChangeState(AIStates.Roaming);
        ChangeDenizenType(startingDenizenType);
        m_AIPlaybook = GetComponent<AIPlaybook>();
        if (m_AIPlaybook == null)
            Debug.LogWarning("AI object does not have a playbook script attached, please add the respecitive script", gameObject);
        else
        {
            m_AIPlaybook.m_EC = this;
            m_AIPlaybook.m_Moveset = CurrentBody.CharacterMoveset;
            MeleeAttackStats = m_AIPlaybook.MeleeAttackStats;
        }
        SetPossessionIndicator(false);
    }

    private void Update()
    {
        DebugVariables.CurrentState = m_AIStateMachine.currentState.ToString();
        DebugVariables.PriorState = m_AIStateMachine.previousState.ToString();

        HasBody = (CurrentBody != null);
        if (!HasBody)
            return;

        m_AIStateMachine.Update();
        HandleTimedAttacks();
        /*if (m_AIStateMachine.currentState == m_AIStateMachine.AllStates[AIStates.Held] && HasBody && IsActuallyGrounded)
        {
            m_AIStateMachine.ChangeState(m_AIStateMachine.AllStates[AIStates.Stunned]);
        }*/

        if (m_AIStateMachine.currentState != m_AIStateMachine.AllStates[AIStates.Held])
            DoPhysics();
        SetAnimations();
    }

    void DoPhysics()
    {
        CurrentBody.PlatformerMovement.GetMovementInput(MovementAxis);
        CurrentBody.PlatformerMovement.ApplyGravity();
        CurrentBody.PlatformerMovement.TryToMoveCharacter();
    }
    void SetAnimations()
    {
        CurrentBody.AnimationController.Move(Mathf.Abs(MovementAxis.x));
        CurrentBody.AnimationController.Rotate(FaceDirection == 1 ? 90 : -90);
        CurrentBody.AnimationController.OnGround(CurrentBody.PlatformerMovement.PlatformerCollisions.Collisions.below);
        CurrentBody.AnimationController.NormalAttack(CurrentBody.CharacterMoveset.DoingNormalAttack);
        CurrentBody.AnimationController.DownAttack(CurrentBody.CharacterMoveset.DoingDownAttack);
    }

    public void SetEmotion()
    {
        SetEmotion(AIEmotions.none);
    }

    public void SetEmotion(AIEmotions emote)
    {
        if (emoteBubble != null)
        {
            emoteBubble.sprite = EnemyManager.EM.AIEmoteList.GetEmote(emote);
        }
    }

    public void SetEmotionActive(bool active)
    {
        if (emoteBubble != null)
            emoteBubble.gameObject.SetActive(active);
    }

    void HandleTimedAttacks()
    {
        if (attackState == AITimedAttackStates.startedAttacking)
        {
            attackState = AITimedAttackStates.isAttacking;
            timedAttackStart = Time.time;
        }
        if (attackState == AITimedAttackStates.isAttacking && Time.time > timedAttackStart + 3)
        {
            UseAttack();
            attackState = AITimedAttackStates.notAttacking;
        }
    }

    public void StopWalkRoutine()
    {
        MovementAxis = Vector2.zero;
        if (moveRoutine != null)
            StopCoroutine(moveRoutine);
    }

    public void GoToState(AIStates aIState)
    {
        m_AIStateMachine.ChangeState(aIState);
    }

    #region Positioning
    public void RunToLocation(Vector3 location)
    {
        targetPosition = GroundedPosition(location);

        StopWalkRoutine();

        moveRoutine = StartCoroutine(GoToPosition());
    }
    public void TailTransform(Transform t)
    {
        TargetOfInterest = t.gameObject;
        m_AIStateMachine.ChangeState(AIStates.Following);
    }


    IEnumerator GoToPosition(bool ignoreYAxis = false, float duelStartDist = 1)
    {
        //do i use a grid system so enemies know where you are?
        //do enemies jump to fight you? do they jump up nearby steps to pathfind around problems?
        //maybe this is what an ai manager would do, track nearby areas to use
        moveState = AIMoveStates.startedMoving;
        float distanceAwayFromGoal = Mathf.Infinity;

        MovementAxis = new Vector2((targetPosition - new Vector3(transform.position.x, targetPosition.y)).normalized.x * urgencyMultiplier, 0);
        SetMonologue("moving");
        while (distanceAwayFromGoal > duelStartDist)
        {
            Debug.DrawLine(transform.position, targetPosition, Color.green);

            if (IsActuallyGrounded) //is grounded
            {
                if (WallInFront())
                {
                    //yield return jumpRoutine = StartCoroutine(JumpOnce());
                    AttemptJump();

                }
                else
                {
                    //if (HoleInFront() && (!ignoreYAxis && targetPosition.y >= transform.position.y))
                    //    yield return jumpRoutine = StartCoroutine(JumpOnce());
                }
            }

            if (ignoreYAxis)
            {
                distanceAwayFromGoal = Mathf.Abs(targetPosition.x - transform.position.x);
                MovementAxis = new Vector2((targetPosition - new Vector3(transform.position.x, targetPosition.y)).normalized.x * urgencyMultiplier, 0);
            }
            else
            {
                distanceAwayFromGoal = Vector3.Distance(transform.position, targetPosition);

                if ((FaceDirection > 0 && transform.position.x >= targetPosition.x) || (FaceDirection < 0 && transform.position.x <= targetPosition.x))
                {
                    SetMonologue("close enough to my dest");
                    break;
                }
            }
            yield return null;
            moveState = AIMoveStates.isMoving;
        }
        SetMonologue("arrived");
        moveState = AIMoveStates.stoppedMoving;
        MovementAxis = Vector2.zero;
        StartCoroutine(ChangeToNotMoving());
    }

    IEnumerator ChangeToNotMoving()
    {
        yield return null;
        moveState = AIMoveStates.notMoving;
    }

    public Vector3 SafeAttackPositionOffset()
    {
        return new Vector3(CurrentAttackStats.range + (Random.Range(0, CurrentAttackStats.range / 2) * -RandomPlusMinus), 0);
    }


    #endregion
    #region jumping

    public void AttemptJump()
    {
        //TODO: check object normal to see if it's a wall
        RaycastHit obstacle;
        if (Physics.Raycast(new Ray(transform.position + AnInchHigherOffset + AnInchHigherOffset, DirectionalForward), out obstacle, 2, BodyData.ObstacleMask) && obstacle.normal.y != 0)
            return;

        //if ledge is only 1 block high, this jump might be doable
        if (!ThereIsAnObjectInFrontOfEyes())
        {
            if (ThereIsAGapICanEnter())//if there is a ceiling
            {
                //do min jump
                // allowed to roam jump, wall to jump, there is no ceiling, grounded
                if (jumpRoutine == null)
                    jumpRoutine = StartCoroutine(MinimalJump());
            }
            else
            {
                jumpState = AIJumpStates.cantJump;
            }
        }
        else
        {
            Ray minJumpRay = new Ray(EyePosition, DirectionalForward);

            for (float i = 0; i < BodyData.jumpHeightMax.y; i += 0.5f)
            {
                if (!Physics.Raycast(minJumpRay, 2, BodyData.ObstacleMask))
                {
                    Debug.DrawRay(EyePosition + new Vector3(0, i), DirectionalForward, Color.magenta);
                    break;
                }
                else
                    Debug.DrawRay(EyePosition + new Vector3(0, i), DirectionalForward, Color.black);
                minJumpRay = new Ray(EyePosition + new Vector3(0, i), DirectionalForward);
            }
            Debug.DrawRay(minJumpRay.GetPoint(2), Vector3.up, Color.cyan);
            //Debug.Break();

            if (!Physics.Raycast(new Ray(minJumpRay.GetPoint(2), Vector3.up), BodyData.characterHeight + 0.5f, BodyData.ObstacleMask))
            {
                if (jumpRoutine == null)
                    jumpRoutine = StartCoroutine(CalculatedJump((minJumpRay.origin.y - transform.position.y) / 10));
            }
            else
            {
                jumpState = AIJumpStates.cantJump;
            }

            /* if (!ThereIsAWallAtMaxJumpHeight())
             {
                 Debug.DrawRay(forwardVisionRay.GetPoint(BodyData.wallCheckDistance) + BodyData.jumpHeightMax, Vector3.up * 2, Color.magenta);
                 if (ThereIsAGapICanReach())//if there is a ceiling
                 {

                     Debug.Break();
                     //do max jump
                     if (jumpRoutine == null)
                         jumpRoutine = StartCoroutine(MinimalJump());
                 }
                 else
                 {
                     jumpState = AIJumpStates.cantJump;
                 }
             }*/
        }
    }

    IEnumerator<float> JumpOnce_RandomHeight()
    {
        CurrentBody.PlatformerMovement.StartJump();
        for (int i = 0; i < Random.Range(0, 20); i++)
        {
            yield return Timing.WaitForSeconds(0.1f);
        }
        CurrentBody.PlatformerMovement.ReleaseJump();
        yield return Timing.WaitForSeconds(Random.Range(0, 5));
    }

    bool ThereIsAnObjectInFrontOfEyes()
    {
        return Physics.Raycast(forwardVisionRay, 2, BodyData.ObstacleMask);
    }
    public bool ThereIsAPersonInFrontOfEyes()
    {
        return Physics.Raycast(forwardVisionRay, 2, CreatureMask);
    }

    bool ThereIsAGapICanEnter()
    {
        Ray t_rayMeToMinJumpCeiling = new Ray(forwardVisionRay.GetPoint(BodyData.wallCheckDistance), Vector3.up);
        return !Physics.Raycast(t_rayMeToMinJumpCeiling, 2, BodyData.ObstacleMask);
    }

    bool ThereIsAWallAtMaxJumpHeight()
    {
        Debug.DrawRay(EyePosition + BodyData.jumpHeightMax, DirectionalForward * 2, Color.blue);
        Debug.Break();
        return Physics.Raycast(new Ray(EyePosition + BodyData.jumpHeightMax, DirectionalForward), 2, BodyData.ObstacleMask);
    }
    bool ThereIsAGapICanReach()
    {
        Ray t_rayMeToMinJumpCeiling = new Ray(forwardVisionRay.GetPoint(BodyData.wallCheckDistance) + BodyData.jumpHeightMax, Vector3.up);
        return !Physics.Raycast(t_rayMeToMinJumpCeiling, 2, BodyData.ObstacleMask);
    }

    IEnumerator MinimalJump()
    {
        jumpState = AIJumpStates.startingJump;
        float oldUrgency = urgencyMultiplier;
        urgencyMultiplier = BodyData.runUrgency;
        yield return new WaitForSeconds(0.05f);
        CurrentBody.PlatformerMovement.StartJump();
        CurrentBody.AnimationController.Jump(true);
        yield return new WaitForSeconds(0.06f);
        jumpState = AIJumpStates.inJump;

        //yield return Timing.WaitForSeconds(0.5f);
        CurrentBody.PlatformerMovement.ReleaseJump();
        CurrentBody.AnimationController.Jump(false);
        urgencyMultiplier = oldUrgency;
        jumpRoutine = null;
        yield return new WaitForSeconds(Random.Range(0, 5));
    }
    IEnumerator CalculatedJump(float jumpFor = 0.06f)
    {
        jumpState = AIJumpStates.startingJump;
        float oldUrgency = urgencyMultiplier;
        urgencyMultiplier = BodyData.runUrgency;
        yield return new WaitForSeconds(0.05f);
        CurrentBody.PlatformerMovement.StartJump();
        CurrentBody.AnimationController.Jump(true);
        yield return new WaitForSeconds(jumpFor);
        jumpState = AIJumpStates.inJump;

        //yield return Timing.WaitForSeconds(0.5f);
        CurrentBody.PlatformerMovement.ReleaseJump();
        CurrentBody.AnimationController.Jump(false);
        urgencyMultiplier = oldUrgency;
        jumpRoutine = null;
        yield return new WaitForSeconds(Random.Range(0, 5));
    }
    #endregion
    #region Vision Tests
    public void LookAtTarget()
    {
        ConfirmTargetIsAlive();
        if (HasTarget)
            FaceDirection = Mathf.Sign(TargetPerson.transform.position.x - transform.position.x);
        CurrentBody.AnimationController.Rotate(FaceDirection == 1 ? 90 : -90);
    }
    public bool LookingAtTarget()
    {
        return FaceDirection == Mathf.Sign(TargetPerson.transform.position.x - transform.position.x);
    }

    void LookForPlayer()
    {
        //if (EnemyManager.EM.WantedLevel > 0)
        {
            if (!HasTarget)
                TargetPerson = EnemyManager.EM.KillTarget;

            ConfirmTargetIsAlive();

            if (HasTarget)
                TargetIsNearby = TargetWithinVisionCapsule();
        }
    }

    public Collider[] CheckAreaViaCapsule(LayerMask f_LM)
    {
        return Physics.OverlapCapsule(transform.position + visionColliderEdgeForward, transform.position + visionColliderEdgeBackward, BodyData.visionCapsuleRadius, f_LM);
    }

    public Ray CanTargetReasonablyBeSeen(Vector3 target)
    {
        return CanTargetReasonablyBeSeen(target, EyePosition);
    }
    public Ray CanTargetReasonablyBeSeen(Vector3 target, Vector3 startingPoint)
    {
        Debug.DrawRay(startingPoint, ((target + BodyData.eyeBallHeight) - startingPoint), Color.cyan);
        return new Ray(startingPoint, ((target + BodyData.eyeBallHeight) - startingPoint));
    }

    public void CheckForTargets()
    {
        foreach (Collider c in CheckAreaViaCapsule(CreatureMask))
        {
            if ((c.transform.root != transform.root))
            {
                if (m_AIStateMachine.currentState == m_AIStateMachine.AllStates[AIStates.Raging] || !ignoredTargets.Contains(c.transform.root))
                {
                    Transform spotLine = HandleRaycastall(c.transform.root);

                    if (spotLine != null) //hits
                    {
                        if (spotLine.root != c.transform.root)
                        {
                            SetMonologue("That thing's a loser");
                            ignoredTargets.Add(c.transform.root);
                            timeStampToRemoveTargets.Add(Time.time + removeTargetDelay);
                            return;
                        }
                        DenizenInfo t_DInfo = spotLine.GetComponentInParent<CharacterBody>().DenizensInfo;

                        if (m_AIStateMachine.currentState == m_AIStateMachine.AllStates[AIStates.Raging] && IsValidRageTarget(t_DInfo))
                        {
                            //Debug.Log("b");
                            SetMonologue("U GON DIE!");
                            TargetPerson = c.transform.root.gameObject;
                            m_AIStateMachine.ChangeState(AIStates.DecidingAttack);
                            return;
                        }
                        else if (IsValidFollowTarget(t_DInfo))
                        {
                            //Debug.Log("b");
                            SetMonologue("I like you!!");
                            //TODO: state where they chase that thing they saw
                            TargetOfInterest = c.transform.root.gameObject;
                            m_AIStateMachine.ChangeState(AIStates.Following);
                            return;
                        }
                        else if (IsValidFleeTarget(t_DInfo))
                        {
                            //Debug.Log("b");
                            SetMonologue("SHRIEK!");
                            //TODO: state where they flee from that thing they saw
                            TargetOfInterest = c.transform.root.gameObject;
                            m_AIStateMachine.ChangeState(AIStates.Fleeing);
                            return;
                        }
                        else if (IsValidAttackTarget(t_DInfo)) //if it doesn't hit what it aimed at or it's not on the kill list
                        {
                            //Debug.Log("c");
                            if (DebugVariables.DebugMode_outputAlerts)
                                Debug.Log("OH BOY HERE I GO KILLING AGAIN");
                            TargetPerson = c.transform.root.gameObject;
                            m_AIStateMachine.ChangeState(AIStates.DecidingAttack);
                            return;
                        }
                        else
                        {
                            //Debug.Log("a");
                            SetMonologue("That guy's a loser");
                            ignoredTargets.Add(c.transform.root);
                            timeStampToRemoveTargets.Add(Time.time + removeTargetDelay);
                            return;
                        }
                    }
                }
            }
            /* else
             {
                 Debug.Log("wrong number");
                 ignoredTargets.Add(c.transform.root);
                 timeStampToRemoveTargets.Add(Time.time + removeTargetDelay);
                 break;
             }*/
        }

        ClearOldTargets();
    }

    public bool IsValidAttackTarget(DenizenInfo DD)
    {
        return (DD != null && DD.denizenType != 0 && KillOnSightList != 0 && (DD.denizenType & KillOnSightList) != 0);
    }
    bool IsValidFleeTarget(DenizenInfo DD)
    {
        return (DD != null && DD.denizenType != 0 && FleeOnSightList != 0 && (DD.denizenType & FleeOnSightList) != 0);
    }
    bool IsValidFollowTarget(DenizenInfo DD)
    {
        return (DD != null && DD.denizenType != 0 && FollowOnSightList != 0 && (DD.denizenType & FollowOnSightList) != 0);
    }
    bool IsValidRageTarget(DenizenInfo DD)
    {
        return (DD != null && DD.denizenType != 0 && RageKillOnSightList != 0 && (DD.denizenType & RageKillOnSightList) != 0);
    }

    void ClearOldTargets()
    {
        if (ignoredTargets.Count > 0)
        {
            if (Time.time > timeStampToRemoveTargets[0])
            {
                ignoredTargets.RemoveAt(0);
                timeStampToRemoveTargets.RemoveAt(0);
            }
        }
    }

    Transform HandleRaycastall(Transform colliderRoot)
    {
        foreach (RaycastHit RH in Physics.RaycastAll(CanTargetReasonablyBeSeen(colliderRoot.position), 10, WallsGroundAndCreatureMask).OrderBy(h => h.distance).ToArray())
        {
            if (RH.transform.root != transform.root)
            {
                if ((BodyData.ObstacleMask.value & LayerMask.GetMask(LayerMask.LayerToName(RH.transform.gameObject.layer))) != 0)//(RH.transform.root == c.transform.root)
                {
                    break;
                }
                else
                {
                    return RH.transform;
                }
            }
        }
        return null;
    }

    public bool TargetWithinVisionCapsule() // Vision cylinder
    {
        ConfirmTargetIsAlive();

        Transform targetTest = null;
        Collider[] overlapPoints = Physics.OverlapCapsule(transform.position + visionColliderEdgeForward, transform.position + visionColliderEdgeBackward, BodyData.visionCapsuleRadius, TargetMask);
        if (overlapPoints.Length > 0)
            targetTest = overlapPoints[0].transform;
        else
            return false;

        if (targetTest != null)
        {
            Ray rara = CanTargetReasonablyBeSeen(targetTest.position);

            RaycastHit spotLine;
            if (Physics.Raycast(rara, out spotLine, 10, WallsGroundAndTargetMask))
            {
                //Debug.DrawLine(rara.origin, rara.GetPoint(10), Color.red);
                if (spotLine.transform.root == TargetPerson.transform.root)
                    return true;
            }
        }
        return false;
    }

    public bool TargetStillWorthChasing(bool constrainToFaceDirection = true)
    {
        if (constrainToFaceDirection)
        {
            if (!LookingAtTarget())
                return false;
        }

        Transform targetTest = null;
        RaycastHit[] AllHits = Physics.RaycastAll(CanTargetReasonablyBeSeen(TargetPerson.transform.root.position), 10, WallsGroundAndCreatureMask).OrderBy(h => h.distance).ToArray();
        if (AllHits.Length > 0)
        {
            foreach (RaycastHit RH in AllHits)
            {
                if (RH.transform.root != transform.root)
                {
                    if ((BodyData.ObstacleMask.value & LayerMask.GetMask(LayerMask.LayerToName(RH.transform.gameObject.layer))) != 0)//(RH.transform.root == c.transform.root)
                    {
                        return false;
                    }
                    else
                    {
                        targetTest = RH.transform;
                        if (targetTest == null)
                            return false;
                        else
                            return true;
                    }
                }
            }
        }
        return false;
    }

    bool IsPlayerVeryClose(Vector3 visionCheckingPoint) //re evaluate 
    {

        Ray rara = new Ray(EyePosition, ((visionCheckingPoint + BodyData.eyeBallHeight) - EyePosition));
        float t_visionCheckDistance = Mathf.Abs(visionCheckingPoint.x - transform.position.x);
        RaycastHit spotLine;
        if (Physics.Raycast(rara, out spotLine, t_visionCheckDistance, WallsGroundAndTargetMask))
        {
            //hit something
            if (spotLine.transform.root.CompareTag(TargetPerson.tag))
                return true;
            else
                return false;
        }
        else
        {
            //didn't hit something
            {
                //use biggevisionCapsuleRadius collider
                Collider[] overlapPoints = Physics.OverlapSphere(rara.GetPoint(t_visionCheckDistance), CurrentAttackStats.range + 0.5f, TargetMask);
                if (overlapPoints.Length > 0)
                {
                    //sphere got a hit
                    rara = new Ray(EyePosition, ((overlapPoints[0].transform.position + BodyData.eyeBallHeight) - EyePosition));

                    if (Physics.Raycast(rara, out spotLine, CurrentAttackStats.range, WallsGroundAndTargetMask))
                    {
                        if (spotLine.transform.root.CompareTag(TargetPerson.tag))
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                {
                    //sphere got nothing
                    return false;
                }
            }
        }
    }

    #endregion
    #region ground check raycasts    
    bool WallInFront()
    {
        wallCheckRay = new Ray(transform.position + AnInchHigherOffset + AnInchHigherOffset, DirectionalForward);
        Debug.DrawLine(wallCheckRay.origin, wallCheckRay.GetPoint(BodyData.wallCheckDistance), Color.blue);
        return Physics.Raycast(wallCheckRay, BodyData.wallCheckDistance, BodyData.ObstacleMask);
    }
    bool HoleInFront()
    {
        holeCheckRay = new Ray(wallCheckRay.GetPoint(BodyData.wallCheckDistance), -Vector3.up);
        Debug.DrawLine(holeCheckRay.origin, holeCheckRay.GetPoint(BodyData.holeCheckDepth), Color.blue);
        return !Physics.Raycast(holeCheckRay, BodyData.holeCheckDepth, BodyData.ObstacleMask);
    }
    bool ObjectAtHeadHeight(LayerMask checkLayer)
    {
        Ray fvr = forwardVisionRay;
        Debug.DrawLine(fvr.origin, fvr.GetPoint(CurrentAttackStats.range + 0.5f), Color.blue);
        return Physics.Raycast(fvr, CurrentAttackStats.range + 0.5f, checkLayer);
    }
    #endregion
    #region body states
    public override void TookDamage(CharacterBody damageSource)
    {
        if (damageSource == CurrentBody || !AutoRetailate || (m_AIStateMachine.currentState == m_AIStateMachine.AllStates[AIStates.Chasing] || m_AIStateMachine.currentState == m_AIStateMachine.AllStates[AIStates.Attacking]))
            return;

        base.TookDamage(damageSource);
        TargetPerson = damageSource.gameObject;
        m_AIStateMachine.ChangeState(AIStates.Retaliating);
    }

    public void OnPossess()
    {
        //CurrentAIState = AIStates.none;
        if (jumpRoutine != null)
            StopCoroutine(jumpRoutine);


        m_AIStateMachine.ChangeState(AIStates.None);
        SetEnemyManagerEventSubscriptions(false);
        hp = CurrentBody.CharacterHealth.CurrentHealth;

        SetPossessionIndicator(true);
        SetEmotionActive(false);
        //remove from enemy manager list
        //StopAllCoroutines();
    }
    public void OnDepossess()
    {
        CurrentBody.CharacterHealth.CurrentHealth = hp;
        //health = hp;
        //re-add to enemy manager list if not dead
        SetPossessionIndicator(false);
        SetEmotionActive(true);
    }

    public void OnRegainBody()
    {
        SetEnemyManagerEventSubscriptions(true);
    }

    void SetPossessionIndicator(bool active)
    {
        if (CurrentBody.possessionIndicator != null)
        {
            if (active)
            {
                CurrentBody.possessionIndicator.gameObject.SetActive(true);
                CurrentBody.possessionIndicator.Play();
            }
            else
            {
                CurrentBody.possessionIndicator.gameObject.SetActive(false);
                CurrentBody.possessionIndicator.Pause();
            }
        }
    }

    public override void BodyControllerReset(CharacterBody cb)
    {
        base.BodyControllerReset(cb);
        m_AIStateMachine.ChangeState(AIStates.Stunned);
    }
    public override void Died()
    {
        //CurrentAIState = AIStates.none;
        StopAllCoroutines();
        Destroy(this.gameObject);
    }
    #endregion
    #region Debug Functions
    private void OnDrawGizmosSelected()
    {
        if (DebugVariables.DebugMode_drawGizmos)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position + visionColliderEdgeForward, BodyData.visionCapsuleRadius);
            Gizmos.DrawWireSphere(transform.position + visionColliderEdgeBackward, BodyData.visionCapsuleRadius);
        }
    }

    void SetMonologue(string s)
    {
        if (DebugVariables.monolog != null)
            DebugVariables.monolog.text = s;
    }

    IEnumerator DrawDebugLines(Vector3 a, Vector3 b, int time, string message = "")
    {
        if (!DebugVariables.DebugMode_drawLines)
            yield break;

        if (message != "")
            Debug.Log(message, this.gameObject);

        if (time <= 0)
        {
            Debug.DrawLine(a, b, Color.red);
            Debug.Break();
        }
        else
            for (int i = 0; i < time; i++)
            {
                Debug.DrawLine(a, b, Color.red);
                yield return new WaitForSeconds(0.05f);
            }
        if (lineVisualizer != null)
            lineVisualizer = null;
    }
    #endregion
    #region QuikMafs
    void SetVisualFaceDirection()
    {
        if (Mathf.Abs(movementAxis.x) > 0.01)
            FaceDirection = Mathf.Sign(movementAxis.x);
    }

    Vector3 GroundedPosition(Vector3 startPos)
    {
        RaycastHit t_RCH;
        Physics.Raycast(startPos + (Vector3.up), -Vector3.up, out t_RCH, 20, BodyData.ObstacleMask);
        if (t_RCH.transform != null)
            return t_RCH.point;
        else
            return startPos;
    }
    #endregion
    #region event subscriptions
    void SetEnemyManagerEventSubscriptions(bool subscribeToEvents)
    {
        if (subscribeToEvents)
        {
            Possession.EnteredBodyFromSelf += LoseSightOfPlayer;
            EnemyManager.wantedLevelRaised += IncreaseWantedRating;
            EnemyManager.wantedLevelCleared += ClearWantedRating;
            EnemyManager.PlayerEscaped += ForgetAboutPlayer;
            EnemyManager.EnemyKilled += ConfirmTargetIsAlive;
            EnemyManager.ConfirmKillTarget += RealignKillTarget;
            DebugVariables.subscribedToEvents = true;
        }
        else
        {
            Possession.EnteredBodyFromSelf -= LoseSightOfPlayer;
            EnemyManager.wantedLevelRaised -= IncreaseWantedRating;
            EnemyManager.wantedLevelCleared -= ClearWantedRating;
            EnemyManager.PlayerEscaped -= ForgetAboutPlayer;
            EnemyManager.EnemyKilled -= ConfirmTargetIsAlive;
            EnemyManager.ConfirmKillTarget -= RealignKillTarget;
            DebugVariables.subscribedToEvents = false;
        }
    }
    void LoseSightOfPlayer()
    {
        CanAttackTarget = false;
    }

    void IncreaseWantedRating()
    {
        //CanAttackTarget = (EnemyManager.EM.WantedLevel > 0);
        RealignKillTarget();
        //if (CurrentAIState == AIStates.roaming)
        //    CurrentAIState = AIStates.patrolling;
    }

    void ClearWantedRating()
    {
        CanAttackTarget = false;
        //AttackingTarget = false;
    }
    void ForgetAboutPlayer()
    {
        //AttackingTarget = false;
    }

    public void ConfirmTargetIsAlive()
    {
        if (TargetPerson == null) //TargetIsNearby && 
        {
            TargetPerson = null;
            //TargetIsNearby = false;
            if (DebugVariables.DebugMode_outputAlerts)
                Debug.Log("nyeh see");
        }
    }
    public void ConfirmTargetIsAlive(BodyType enemyType)
    {
        ConfirmTargetIsAlive();
    }
    void RealignKillTarget()
    {
        TargetPerson = EnemyManager.EM.KillTarget;
        if (DebugVariables.DebugMode_outputAlerts)
            Debug.Log("target updated");
    }
    #endregion

    public void SelectAttackStrategy()
    {
        m_AIPlaybook.SetPlannedAttack();
    }
    public void PrepMeleeAttack()
    {
        m_AIPlaybook.ReplacePlannedAttackWithMelee();
    }
    public void UseAttack()
    {
        if (DebugVariables.DebugMode_outputAlerts)
            Debug.Log("attack using selected attack");
        // CurrentBody.CharacterMoveset.AITryAttack(); //.TryNormalAttack(TargetMask);
        //  m_EC.CurrentBody.CharacterMoveset.TryDownAttack(m_EC.TargetMask);
        m_AIPlaybook.DoPlannedAttack(TargetMask);

    }


    #region Gang stuff
    public void ChangeDenizenType(DenizenType newType)
    {
        //TODO: if newType is not compound type, then change "gang" info

        currentDenizenInfo = EnemyManager.EM.DenizenClassInfoByReference(newType);

        if (currentDenizenInfo != null)
            UpdateInfoBasedOnGang();
        else
            Debug.LogWarning("Denizen info not assigned; material and interactions will be incorrect", gameObject);
    }

    public void UpdateInfoBasedOnGang()
    {
        if (CurrentBody.CharacterMeshes.Length > 0)
            foreach (SkinnedMeshRenderer smr in CurrentBody.CharacterMeshes)
            {
                smr.material = currentDenizenInfo.denizenMat;

                //need gang asset
                //name, color
            }
        CurrentBody.ChangeDenizenType(currentDenizenInfo);
        //TransformHelper.SetTargetLayerRecursively(this.gameObject, gameObject.layer, LayerMask.NameToLayer( currentDenizenInfo.gangLayer));
    }

    #endregion
}


