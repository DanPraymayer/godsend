﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruntAIPlaybook : AIPlaybook {

    RaycastHit rch;

    private void Awake()
    {
        MeleeAttackStats = new PossibleAttackStats(0, 1, 1, 1);
    }

    public override void SetPlannedAttack() {
        plannedAttack = AttackTypes.Normal;
        PrepAttack();
    }
    public override void DoPlannedAttack(LayerMask targetMask) {
        base.DoPlannedAttack(targetMask);
    }

    void PrepAttack()
    {
        m_EC.CurrentAttackStats = m_EC.MeleeAttackStats;
        Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
        if (Physics.Raycast(safeDistFromTarget, out rch, 1.5f, ~m_EC.BodyData.ObstacleMask))
        {
            m_EC.RunToLocation(rch.point);
        }
        else
        {
            m_EC.RunToLocation(safeDistFromTarget.GetPoint(1.5f));
        }
        m_EC.GoToState(AIStates.Chasing);
    }

    public override void ReplacePlannedAttackWithMelee()
    {
        plannedAttack = AttackTypes.Normal;
    }

    protected override void DoDownAttackWithVariant() { }
    protected override void DoUpAttackWithVariant() { }
    protected override void DoNormalAttackWithVariant() {
        m_Moveset.TryNormalAttack(plannedLayerMask);
    }
    protected override void DoMeleeAttackWithVariant() {
        DoNormalAttackWithVariant();
    }
}
