﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowerAIPlaybook : AIPlaybook
{
    RaycastHit rch;
    WaitForSeconds waitForGrab = new WaitForSeconds(1);
    PossibleAttackStats PluckBombStats;
    PossibleAttackStats ThrowAttackStats;
    PossibleAttackStats GrabAttackStats;
    [Range(0, 10)]
    public float ChanceToUseAlt;

    private void Awake()
    {
        MeleeAttackStats = new PossibleAttackStats(0, 1, 1.5f, 1);
        PluckBombStats = new PossibleAttackStats(0, 1, 0, 2);
        GrabAttackStats = new PossibleAttackStats(0, 1, 1.5f, 1);
        ThrowAttackStats = new PossibleAttackStats(0, 1, 5, 1);
    }

    public override void SetPlannedAttack()
    {
        plannedAttack = AttackTypes.Normal;

        if (m_Moveset.PrerequisitesMetForNormAttack)
        {
            m_EC.CurrentAttackStats = ThrowAttackStats;
            PrepThrow();
        }
        else
        {
            if (Random.Range(0, 10) < ChanceToUseAlt)
            {
                plannedAttack = AttackTypes.Down;
                m_EC.CurrentAttackStats = PluckBombStats;
                m_EC.GoToState(AIStates.ImmediateAttack);
            }
            else
            {
                m_EC.CurrentAttackStats = GrabAttackStats;
                PrepGrab();
            }
        }
    }

    void PrepThrow()
    {
        if (!m_EC.HasTarget)
            return;
        Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
        if (Physics.Raycast(safeDistFromTarget, out rch, 5, ~m_EC.BodyData.ObstacleMask))
        {
            m_EC.RunToLocation(rch.point);
        }
        else
        {
            m_EC.RunToLocation(safeDistFromTarget.GetPoint(5));
        }
        m_EC.GoToState(AIStates.Positioning);
    }

    void PrepGrab()
    {
        Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
        if (Physics.Raycast(safeDistFromTarget, out rch, 1.5f, ~m_EC.BodyData.ObstacleMask))
        {
            m_EC.RunToLocation(rch.point);
        }
        else
        {
            m_EC.RunToLocation(safeDistFromTarget.GetPoint(1.5f));
        }
        m_EC.GoToState(AIStates.Chasing);
    }

    public override void ReplacePlannedAttackWithMelee()
    {
        plannedAttack = AttackTypes.Normal;
    }

    protected override void DoDownAttackWithVariant() {
        m_Moveset.TryDownAttack(plannedLayerMask);
    }
    protected override void DoUpAttackWithVariant() { }
    protected override void DoNormalAttackWithVariant()
    {
        m_Moveset.TryNormalAttack(plannedLayerMask);
        StartCoroutine(checkIfHolding());
    }
    IEnumerator checkIfHolding()
    {
        yield return waitForGrab;
        Debug.Log(m_Moveset.PrerequisitesMetForNormAttack);
        if (m_Moveset.PrerequisitesMetForNormAttack)
            m_EC.attackState = AITimedAttackStates.startedAttacking;
    }


    protected override void DoMeleeAttackWithVariant()
    {
        DoNormalAttackWithVariant();
    }
}
