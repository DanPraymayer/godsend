﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackTypes
{
    None,
    Down,
    Up,
    Normal
}
public class AIPlaybook : MonoBehaviour
{
    //when in chase state, decide which attack to use
    //have priority list for each
    public AttackTypes plannedAttack = AttackTypes.None;

    public EnemyController m_EC { get; set; }
    public CharacterMoveset m_Moveset { get; set; }
    public LayerMask plannedLayerMask;
    public PossibleAttackStats MeleeAttackStats { get; protected set; }

    public virtual void SetPlannedAttack() { }
    public virtual void ReplacePlannedAttackWithMelee() { }
    public virtual void DoPlannedAttack(LayerMask targetMask)
    {
        plannedLayerMask = targetMask;
        switch (plannedAttack)
        {
            case (AttackTypes.Down):
                DoDownAttackWithVariant();
                break;
            case (AttackTypes.Up):
                DoUpAttackWithVariant();
                break;
            case (AttackTypes.Normal):
                DoNormalAttackWithVariant();
                break;
        }
    }

    protected virtual void DoDownAttackWithVariant() { }
    protected virtual void DoUpAttackWithVariant() { }
    protected virtual void DoNormalAttackWithVariant() { }
    protected virtual void DoMeleeAttackWithVariant() { }
}
