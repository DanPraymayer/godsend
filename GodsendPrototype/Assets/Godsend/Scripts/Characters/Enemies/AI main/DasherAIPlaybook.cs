﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherAIPlaybook : AIPlaybook
{

    RaycastHit rch;
    PossibleAttackStats DashAttackStats;

    private void Awake()
    {
        MeleeAttackStats = new PossibleAttackStats(0, 1, 1, 1);
        DashAttackStats = new PossibleAttackStats(1, 2, 3, 2);
    }

    public override void SetPlannedAttack()
    {
        plannedAttack = AttackTypes.Normal;
        PrepAttack();
    }
    /*public override void DoPlannedAttack(LayerMask targetMask) {
        base.DoPlannedAttack(targetMask);
    }*/

    void PrepAttack()
    {
        //m_EC.urgencyMultiplier = 1;
        m_EC.CurrentAttackStats = DashAttackStats;

        Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
        if (Physics.Raycast(safeDistFromTarget, out rch, 2f, ~m_EC.BodyData.ObstacleMask))
        {
            m_EC.RunToLocation(rch.point);
        }
        else
        {
            m_EC.RunToLocation(safeDistFromTarget.GetPoint(2f));
        }
        m_EC.GoToState(AIStates.Chasing);
    }

    public override void ReplacePlannedAttackWithMelee()
    {
        plannedAttack = AttackTypes.Normal;
    }

    protected override void DoNormalAttackWithVariant()
    {
        m_Moveset.TryNormalAttack(plannedLayerMask);
    }
    protected override void DoMeleeAttackWithVariant()
    {
        DoNormalAttackWithVariant();
    }
}
