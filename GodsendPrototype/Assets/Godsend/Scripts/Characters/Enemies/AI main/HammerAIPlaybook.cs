﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerAIPlaybook : AIPlaybook
{
    bool attackAnEnemyNow;
    PossibleAttackStats SummonAttackStats;
    bool planningToAttackPillar;
    [Range(0, 10)]
    public float ChanceToUseAlt = 5;

    private void Awake()
    {
        MeleeAttackStats = new PossibleAttackStats(0, 1, 1.5f, 1);
        SummonAttackStats = new PossibleAttackStats(1, 2, 1.5f, 2);
    }

    public override void SetPlannedAttack()
    {
        //test whether it's safe to use pillar
        Debug.Log("locked and loaded " + m_Moveset.PrerequisitesMetForDownAttack);

        attackAnEnemyNow = false;
        if (m_Moveset.PrerequisitesMetForDownAttack)
        {
            m_EC.CurrentAttackStats = MeleeAttackStats;
            plannedAttack = AttackTypes.Normal;
        }
        else
        {
            if (Random.Range(0, 10) < ChanceToUseAlt)//Random.Range(0, 2)
            {
                m_EC.CurrentAttackStats = SummonAttackStats;
                plannedAttack = AttackTypes.Down;
            }
            else
            {
                m_EC.CurrentAttackStats = MeleeAttackStats;
                plannedAttack = AttackTypes.Normal;
            }
        }

        switch (plannedAttack)
        {
            case (AttackTypes.Down):
                PrepPillarSummon();
                break;
            case (AttackTypes.Normal):
                PrepNormalAttack();
                break;
        }


        m_EC.CurrentAttackStats = MeleeAttackStats;

        //is pillar spawned
        //is person close



        /*if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.DirectionalForward), m_EC.MeleeAttackStats.range, ~LayerMask.GetMask("Hammerable")))
        {
            //
            attackAnEnemyNow = true;
            plannedAttack = AttackTypes.Normal;
        }
        else if (!m_Moveset.PrerequisitesMetForDownAttack)
        { //pillar is inactive
            
        }
        else
        {
            m_EC.CurrentAttackStats = MeleeAttackStats;
            plannedAttack = AttackTypes.Normal;
        }*/


    }
    public override void ReplacePlannedAttackWithMelee()
    {
        m_EC.CurrentAttackStats = MeleeAttackStats;
        plannedAttack = AttackTypes.Normal;
    }

    void PrepPillarSummon()
    {
        m_EC.ConfirmTargetIsAlive();
        if (!m_EC.HasTarget)
        {
            m_EC.GoToState(AIStates.ImmediateAttack);
        }
        else
        {
            RaycastHit rch;

            Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
            if (Physics.Raycast(safeDistFromTarget, out rch, 4, ~m_EC.BodyData.ObstacleMask))
            {
                m_EC.RunToLocation(rch.point);
            }
            else
            {
                m_EC.RunToLocation(safeDistFromTarget.GetPoint(4));
            }

            m_EC.GoToState(AIStates.Positioning);
        }
    }
    //void PrepUpAttack() { }
    void PrepNormalAttack()
    {
        //run to attack position
        planningToAttackPillar = false;
        if (attackAnEnemyNow)
        {
            m_EC.GoToState(AIStates.ImmediateAttack);
            return;
        }

        RaycastHit rch;
        if (m_Moveset.PrerequisitesMetForDownAttack && Physics.OverlapSphere(transform.position, 4, m_EC.CurrentBody.BreakableLayer).Length > 0)
        {
            Debug.Log("attacking a box now lol");
            //decide whether it's worth hitting the boxes or not

            //plannedLayerMask = LayerMask.GetMask("Hammerable");
            planningToAttackPillar = true;
            Ray safeDistFromTarget = new Ray(m_Moveset.SpecialConsideration + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_Moveset.SpecialConsideration.x));
            if (Physics.Raycast(safeDistFromTarget, out rch, 1.5f, ~m_EC.BodyData.ObstacleMask))
            {
                m_EC.RunToLocation(rch.point);
            }
            else
            {
                m_EC.RunToLocation(safeDistFromTarget.GetPoint(1.5f));
            }
            m_EC.GoToState(AIStates.Positioning);
        }
        else
        {
            m_EC.ConfirmTargetIsAlive();
            if (!m_EC.HasTarget)
            {
                m_EC.GoToState(AIStates.Roaming);
            }
            else
            {
                Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
                if (Physics.Raycast(safeDistFromTarget, out rch, 1.5f, ~m_EC.BodyData.ObstacleMask))
                {
                    m_EC.RunToLocation(rch.point);
                }
                else
                {
                    m_EC.RunToLocation(safeDistFromTarget.GetPoint(1.5f));
                }
                m_EC.GoToState(AIStates.Chasing);
            }

        }
    }

    public override void DoPlannedAttack(LayerMask targetMask)
    {
        base.DoPlannedAttack(targetMask);
    }

    protected override void DoDownAttackWithVariant()
    {
        //Debug.Log("pilla");
        m_Moveset.TryDownAttack(plannedLayerMask);
    }
    //protected override void DoUpAttackWithVariant() { }
    protected override void DoNormalAttackWithVariant()
    {
        if (planningToAttackPillar)
        {
            if(m_EC.HasTarget)
                m_EC.LookAtTarget(); 
            m_Moveset.TryNormalAttack(m_EC.CurrentBody.BreakableLayer);
        }
        else
            m_Moveset.TryNormalAttack(plannedLayerMask);
    }
    protected override void DoMeleeAttackWithVariant()
    {
        m_Moveset.TryNormalAttack(plannedLayerMask);
    }
}
