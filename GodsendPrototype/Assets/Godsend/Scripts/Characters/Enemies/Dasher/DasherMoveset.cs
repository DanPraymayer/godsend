﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherMoveset : CharacterMoveset
{


    DashAttack m_normalAttack;
    public DashAttack NormalAttack { get { return m_normalAttack; } }

    public float targetRadius;
    public LayerMask layerMask;
    public Transform currentTarget;

    protected override void Start()
    {
        base.Start();

        m_normalAttack = GetComponentInChildren<DashAttack>();

    }

    public override void TryNormalAttack(LayerMask targetLayer)
    {
        if (m_normalAttack.CanAttack)
        {
            if (m_characterBody.PlatformerMovement.PlatformerCollisions.IsGrounded || currentTarget == null)
            {
                m_normalAttack.SetDashDirection(Vector3.right * m_characterBody.CurrentController.FaceDirection);
            }
            else
            {
                m_normalAttack.SetDashDirection((currentTarget.transform.position - transform.position).normalized);
            }

            CurrentAttackType = AttackType.NormalAttack;
            m_normalAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
            if (m_characterBody != null && m_characterBody.CurrentController.gameObject.layer == NamingConstants.PLAYER_LAYER)
            {
                CanvasManager.CM.movesetUI.AbilityStandard.SetAbilityDepleted(true);
            }
        }
    }

    public override void UpdateTargets()
    {

        if (!m_characterBody.PlatformerMovement.PlatformerCollisions.IsGrounded)
        {

            Collider[] targets = Physics.OverlapSphere(m_characterBody.transform.position, targetRadius, layerMask);

            Debug.Log(targets.Length);

            foreach (Collider target in targets)
            {
                if (currentTarget == null || Vector3.Distance(currentTarget.transform.position, m_characterBody.transform.position) > Vector3.Distance(target.transform.position, m_characterBody.transform.position))
                {
                    if (currentTarget != null)
                        currentTarget.GetChild(0).gameObject.SetActive(false);
                    currentTarget = target.transform;
                    currentTarget.GetChild(0).gameObject.SetActive(true);
                }
            }

            if (currentTarget != null && targets.Length < 1)
            {
                currentTarget.transform.GetChild(0).gameObject.SetActive(false);
                currentTarget = null;
            }
        }
        else if (currentTarget != null && (Vector3.Distance(currentTarget.transform.position, m_characterBody.transform.position) < targetRadius))
        {
            currentTarget.transform.GetChild(0).gameObject.SetActive(false);
            currentTarget = null;
        }

    }

    void ChangeTarget()
    {

    }

}
