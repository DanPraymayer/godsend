﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherAnimation : AnimationController {


    public override void Move(float speed)
    {
        Animator.SetFloat(SpeedBool, speed);
    }

    public override void Jump(bool jumped)
    {
        Animator.SetBool(JumpBool, jumped);
    }

    public override void OnGround(bool onGround)
    {
        Animator.SetBool(GroundedBool, onGround);
    }

    public override void WasHurt()
    {
        Animator.SetTrigger(HurtTrigger);
    }

    public override void NormalAttack(bool attack)
    {
        Animator.SetBool(NormalAttackBool, attack);
    }

    public void SpecialDash(bool attack)
    {
        Animator.SetBool(DownAttackBool, attack);
    }

}
