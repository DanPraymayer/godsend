﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Retreating : AIState
{
    public AI_Retreating(AIStateMachine stateMachine) {
        m_StateMachine = stateMachine;
    }

    public override void UpdateState()
    {
        base.UpdateState();

    }

    public override void Enter()
    {
        base.Enter();
        m_StateMachine.AIController.SetEmotion(AIEmotions.flee);
    }

    public override void Exit()
    {
        base.Exit();
    }

   /* IEnumerator Flee()
    {
        if (CurrentAIState == AIStates.retreating)
        {
            if (HasTarget && Mathf.Abs(transform.position.x - TargetPerson.transform.position.x) <= Mathf.Abs(transform.position.x - lastTargetPosition.x))
            {
                Ray rara = new Ray(EyePosition, Mathf.Sign(transform.position.x - TargetPerson.transform.position.x) * Vector3.right);
                RaycastHit spotLine;
                float ffffff = Random.Range(2, 5);
                if (Physics.Raycast(rara, out spotLine, ffffff, WallsAndGroundMask))
                {
                    targetPosition = spotLine.point;
                }
                else
                {
                    targetPosition = rara.GetPoint(ffffff);
                }

                yield return moveToPositionRoutine = StartCoroutine(GoToPosition(true, 0.5f));
            }
        }
        CurrentAIState = AIStates.reevaluating;
    }*/
}
