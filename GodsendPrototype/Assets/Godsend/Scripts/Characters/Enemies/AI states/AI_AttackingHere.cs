﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_AttackingHere : AIState
{
    EnemyController m_EC;
    float timeSinceLastAttack;

    public AI_AttackingHere(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if (Time.time > timeSinceLastAttack + m_EC.BodyData.globalAttackCooldown)
        {
            if (Time.time > timeSinceLastAttack + 2) //TODO m_EC.CurrentAttack.attackCooldown
            {
                m_StateMachine.ChangeState(AIStates.DecidingAttack);
            }
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.fight);
        m_EC.urgencyMultiplier = 0;

        m_EC.UseAttack();
        timeSinceLastAttack = Time.time;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
