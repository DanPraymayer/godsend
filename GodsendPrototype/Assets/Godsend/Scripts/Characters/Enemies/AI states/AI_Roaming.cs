﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Roaming : AIState
{
    EnemyController m_EC;
    enum WalkSubstates
    {
        prepWalk,
        midWalk,
        endWalk
    }
    WalkSubstates walkSubstate;
    float walkStartTimestamp;
    Vector3 roamStartPos;

    //Prefer to wander only in area
    public AI_Roaming(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        if (m_EC == null)
            m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if (!m_EC.DebugVariables.NoRoam)
            switch (walkSubstate)
            {
                case (WalkSubstates.prepWalk):
                    StartWalk();
                    break;
                case (WalkSubstates.midWalk):
                    DoWalk();
                    break;
                case (WalkSubstates.endWalk):
                    EndWalk();
                    break;
            }

        m_EC.CheckForTargets();
    }

    void StartWalk()
    {
        if (m_StateMachine.AIController.WallIsInFront)
            m_EC.FaceDirection *= -1;
        else if (Vector3.Distance(m_EC.transform.position, roamStartPos) > m_EC.BodyData.Roam_preferredRadius - (m_EC.BodyData.Roam_preferredRadius / 5))
            m_EC.FaceDirection = Mathf.Sign(roamStartPos.x - m_EC.transform.position.x);
        else
            m_EC.FaceDirection = m_EC.RandomPlusMinus;

        m_EC.urgencyMultiplier = m_EC.BodyData.walkUrgency;
        m_EC.MovementAxis = new Vector2(m_EC.urgencyMultiplier * m_EC.FaceDirection, 0);
        walkStartTimestamp = Time.time + Random.Range(m_EC.BodyData.Roam_minWalkTime, m_EC.BodyData.Roam_maxWalkTime);
        //cantJump = false;
        walkSubstate = WalkSubstates.midWalk;
    }

    void DoWalk()
    {
        if (Time.time > walkStartTimestamp)
        {
            SetRoamTimestamps();
        }
        else if (m_EC.IsActuallyGrounded)
        {
            if (m_EC.WallIsInFront)
            {
                if (m_EC.jumpState == AIJumpStates.notJumping && m_EC.BodyData.Roam_allowJumping)
                {
                    m_EC.AttemptJump();
                }
                else
                    SetRoamTimestamps();
            }
            else if (!m_EC.BodyData.Roam_allowFalling && m_EC.HoleIsInFront)
            {
                SetRoamTimestamps();
            }
        }
    }

    void EndWalk()
    {
        if (Time.time > walkStartTimestamp)
        {
            walkSubstate = WalkSubstates.prepWalk;
        }
    }

    void SetRoamTimestamps()
    {
        walkStartTimestamp = Time.time + Random.Range(m_EC.BodyData.Roam_minWalkTime, m_EC.BodyData.Roam_maxWalkTime + (m_EC.BodyData.Roam_minWalkTime / 2));
        walkSubstate = WalkSubstates.endWalk;
        m_EC.MovementAxis = Vector3.zero;
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion();
        m_EC.urgencyMultiplier = m_EC.BodyData.walkUrgency;
        roamStartPos = m_EC.transform.position;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
