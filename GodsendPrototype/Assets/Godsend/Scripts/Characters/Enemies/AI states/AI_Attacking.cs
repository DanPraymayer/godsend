﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Attacking : AIState
{
    EnemyController m_EC;
    float timeSincePrepStart, timeSinceLastAttack;

    bool chargingAttack;

    void TryMyNormalAttack()
    {
        m_EC.CurrentBody.CharacterMoveset.TryNormalAttack(m_EC.TargetMask);
    }

    public AI_Attacking(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        m_EC.ConfirmTargetIsAlive();
        if (!m_EC.HasTarget)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }

        if(chargingAttack && Time.time > timeSincePrepStart + 1)
        {
            Attack();
        }

        if (Time.time > timeSinceLastAttack + m_EC.BodyData.globalAttackCooldown)
        {
            if (Time.time > timeSinceLastAttack + m_EC.CurrentAttackStats.cooldown) //TODO m_EC.CurrentAttack.attackCooldown
            {
                RaycastHit hitTest;
                {
                    if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.TargetPerson.transform.position), out hitTest, m_EC.CurrentAttackStats.range + 0.25f, m_EC.BodyData.ObstacleMask + m_EC.TargetMask))
                    {
                        if (hitTest.transform.root == m_EC.TargetPerson.transform.root)
                            Attack();
                        else
                            m_StateMachine.ChangeState(AIStates.Chasing);
                    }
                    else
                        m_StateMachine.ChangeState(AIStates.Chasing);
                }
            }
        }
    }

    void Prep()
    {
        chargingAttack = true;
        m_EC.LookAtTarget();
        timeSincePrepStart = Time.time;
    }

    void Attack()
    {
        chargingAttack = false;
        m_EC.UseAttack();
        timeSinceLastAttack = Time.time;
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.fight);
        m_EC.urgencyMultiplier = 0;
        Prep();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
