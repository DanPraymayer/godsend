﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Held : AIState {

    public AI_Held(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        m_StateMachine.AIController.SetEmotion(AIEmotions.stunned);
        m_StateMachine.AIController.MovementAxis = Vector2.zero;
    }
    }
