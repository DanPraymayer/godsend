﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AIState
{

    AIStateMachine StateMachine;
    protected AIStateMachine m_StateMachine
    {
        get { return StateMachine; }
        set
        {
            StateMachine = value;
        }
    }
    public AIState() { }

    public virtual void UpdateState() { }
    public virtual void Enter()
    {
        if (m_StateMachine.AIController.DebugVariables.DebugMode_outputStateChanges)
            Debug.Log("entered " + this.ToString() + " state");
    }
    public virtual void Exit()
    {
        if (m_StateMachine.AIController.DebugVariables.DebugMode_outputStateChanges)
            Debug.Log("exited " + this.ToString() + " state");
    }

}
