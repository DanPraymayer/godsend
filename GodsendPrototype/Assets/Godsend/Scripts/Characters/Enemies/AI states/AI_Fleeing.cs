﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Fleeing : AIState
{
    EnemyController m_EC;
    RaycastHit hitTest;
    float timeStamp, runFor = 4;

    public AI_Fleeing(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (Time.time > timeStamp + runFor)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }
        else
        {
            if (m_EC.IsActuallyGrounded && m_EC.WallIsInFront && m_EC.jumpState == AIJumpStates.notJumping)
                m_EC.AttemptJump();
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.flee);

        timeStamp = Time.time;
        m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
        m_EC.MovementAxis = new Vector2((new Vector3(m_EC.transform.position.x, m_EC.TargetOfInterest.transform.position.y) - m_EC.TargetOfInterest.transform.position).normalized.x * m_EC.urgencyMultiplier, 0);
        m_EC.TargetOfInterest = null;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
