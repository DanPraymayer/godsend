﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Reevaluating : AIState
{
    public AI_Reevaluating(AIStateMachine stateMachine) {
        m_StateMachine = stateMachine;
    }

    public override void UpdateState()
    {
        base.UpdateState();

    }

    public override void Enter()
    {
        base.Enter();
        m_StateMachine.AIController.SetEmotion(AIEmotions.thinking);
    }

    public override void Exit()
    {
        base.Exit();
    }

    IEnumerator Reevaluate()
    {
        //LookAtPlayer();
        yield return null;
        /*yield return StartCoroutine(DrawDebugLines(transform.position, lastTargetPosition, 10, "me to goal postion"));
        if (IsPlayerVeryClose(lastTargetPosition))
            CurrentAIState = AIStates.chasing;
        else
        {
            targetPosition = lastTargetPosition;
            yield return moveToPositionRoutine = StartCoroutine(GoToPosition());
            CurrentAIState = AIStates.patrolling;
        }*/

        //RaycastHit rch;
        //Ray fvr = forwardVisionRay;
        //yield return StartCoroutine(DrawDebugLines(fvr.origin, fvr.GetPoint(visionCapsuleForward), -1, "me to foward vision"));

        //AttackingTarget = false;
        //if (TargetIsNearby)
        //    CurrentAIState = AIStates.chasing;
        //else
        //    CurrentAIState = AIStates.patrolling;
    }
}
