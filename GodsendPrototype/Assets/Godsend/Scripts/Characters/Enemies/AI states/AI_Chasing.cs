﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Chasing : AIState
{
    EnemyController m_EC;
    RaycastHit hitTest;
    public AI_Chasing(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        m_EC.ConfirmTargetIsAlive();
        if (!m_EC.HasTarget)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }

        if (m_EC.TargetStillWorthChasing())
        {
            //if (hitTest.transform.root == m_EC.TargetPerson.transform.root)
            {
                m_EC.StopWalkRoutine();
                m_StateMachine.ChangeState(AIStates.Attacking);
            }

        }
        else
        {
            if (m_EC.DebugVariables.DebugMode_outputAlerts)
                Debug.Log("uho");
        }

        if (!m_EC.isWalking && m_EC.TargetPerson != null)
        {
            if (m_EC.TargetStillWorthChasing(false))
                ResetChase();
            else
            {
                m_StateMachine.ChangeState(AIStates.Roaming);
                if (m_EC.DebugVariables.DebugMode_outputAlerts)
                    Debug.Log("alas they are too far");
            }
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.fight);

        m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
    }

    void ResetChase()
    {
        Enter();
        if (m_EC.MovementAxis.x == 0)
        {
            if (m_EC.HasTarget)
            {
                if (m_EC.TargetStillWorthChasing(false))
                {
                    RaycastHit rch;
                    Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
                    if (Physics.Raycast(safeDistFromTarget, out rch, m_EC.CurrentAttackStats.range, ~m_EC.BodyData.ObstacleMask))
                    {
                        m_EC.RunToLocation(rch.point);
                    }
                    else
                    {
                        m_EC.RunToLocation(safeDistFromTarget.GetPoint(m_EC.CurrentAttackStats.range));
                    }
                }
                else
                    Debug.LogError("squack");
            }
            else
                Debug.LogError("squack");
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
