﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Retaliating : AIState
{
    EnemyController m_EC;
    RaycastHit hitTest;
    public AI_Retaliating(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        m_EC.ConfirmTargetIsAlive();
        if (!m_EC.HasTarget)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }

        if (m_EC.moveState == AIMoveStates.stoppedMoving)
            Debug.Log("staphp");

        if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.TargetPerson.transform.position), out hitTest, m_EC.CurrentAttackStats.range + 0.25f, m_EC.CreatureMask))
        {
            if (hitTest.transform.root == m_EC.TargetPerson.transform.root)
            {
                m_EC.StopWalkRoutine();
                m_StateMachine.ChangeState(AIStates.Attacking);
            }
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.fight);
        m_EC.SelectAttackStrategy();

        m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
        RaycastHit rch;
        Ray safeDistFromTarget = new Ray(m_EC.TargetPerson.transform.position + m_EC.AnInchHigherOffset, Vector3.right * Mathf.Sign(m_EC.transform.position.x - m_EC.TargetPerson.transform.position.x));
        if (Physics.Raycast(safeDistFromTarget, out rch, m_EC.CurrentAttackStats.range, ~m_EC.BodyData.ObstacleMask))
        {
            m_EC.RunToLocation(rch.point);
        }
        else
        {
            m_EC.RunToLocation(safeDistFromTarget.GetPoint(m_EC.CurrentAttackStats.range));
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
