﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_PositioningStrategically : AIState
{

    EnemyController m_EC;
    RaycastHit hitTest;
    bool waitingForAttackToEnd;
    float waitTimer;

    public AI_PositioningStrategically(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        if (!waitingForAttackToEnd)
        {
            if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.DirectionalForward), out hitTest, m_EC.MeleeAttackStats.range, m_EC.CreatureMask))
            {
                if (m_EC.IsValidAttackTarget(hitTest.transform.GetComponentInParent<CharacterBody>().DenizensInfo))
                {
                    m_EC.StopWalkRoutine();
                    m_EC.PrepMeleeAttack();
                    m_StateMachine.ChangeState(AIStates.Attacking);
                }
            }

            if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.DirectionalForward), out hitTest, m_EC.MeleeAttackStats.range, ~m_EC.CurrentBody.BreakableLayer))
            {
                Debug.Log("kill box");
                m_EC.StopWalkRoutine();
                m_EC.PrepMeleeAttack();
                m_EC.UseAttack();

                waitingForAttackToEnd = true;
                waitTimer = Time.time + 2;
            }

            if (m_EC.moveState == AIMoveStates.notMoving)
            {
                m_EC.LookAtTarget();
                m_EC.UseAttack();

                waitingForAttackToEnd = true;
                waitTimer = Time.time + 2;
            }
        }
        else
        {
            if (Time.time > waitTimer)
                m_StateMachine.ChangeState(AIStates.DecidingAttack);
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.thinking);
        waitingForAttackToEnd = false;
        m_EC.urgencyMultiplier = m_EC.BodyData.jogUrgency;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
