﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Following : AIState
{
    EnemyController m_EC;
    RaycastHit hitTest;
    bool idling;
    float timeStamp, delay = 5;

    public AI_Following(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if (m_EC.TargetOfInterest == null)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }

        m_EC.MovementAxis = new Vector2((m_EC.TargetOfInterest.transform.position - new Vector3(m_EC.transform.position.x, m_EC.TargetOfInterest.transform.position.y)).normalized.x * m_EC.urgencyMultiplier, 0);

        if (Vector3.Distance(m_EC.transform.position, m_EC.TargetOfInterest.transform.position) < 1.5f)
        {
            if (!idling) {
                idling = true;
                timeStamp = Time.time;
                m_EC.urgencyMultiplier = m_EC.BodyData.jogUrgency;
            }

            if(Time.time > timeStamp + delay)
            {
                m_StateMachine.ChangeState(AIStates.Roaming);
            }
        }
        else
        {
            if (idling)
            {
                idling = false;
                m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
            }

            if (m_EC.IsActuallyGrounded)
            {
                if (m_EC.WallIsInFront)
                {
                    if (m_EC.jumpState == AIJumpStates.notJumping)
                    {
                        m_EC.AttemptJump();
                    }
                }
            }
        }

        m_EC.CheckForTargets();
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.follow);

        m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
