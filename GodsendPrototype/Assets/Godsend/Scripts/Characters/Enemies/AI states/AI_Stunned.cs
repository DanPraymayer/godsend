﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Stunned : AIState
{
    float stunStart;
    float maxStunDuration = 4, currentStunDuration;
    public AI_Stunned(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if (Time.time > stunStart + currentStunDuration)
        {
            m_StateMachine.AIController.OnRegainBody();
            EnemyManager.EM.EnemyRecoveredFromStun(m_StateMachine.AIController);
            m_StateMachine.ChangeState(AIStates.Roaming);
        }
    }

    public void DecreaseStunDuration()
    {
        currentStunDuration -= 1;
        Debug.Log("healz");
    }

    public override void Enter()
    {
        base.Enter();
        m_StateMachine.AIController.SetEmotion(AIEmotions.stunned);
        m_StateMachine.AIController.OnDepossess();
        stunStart = Time.time;
        currentStunDuration = maxStunDuration;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
