﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_DecidingAttack : AIState
{
    EnemyController m_EC;
    public AI_DecidingAttack(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.thinking);
        m_EC.SelectAttackStrategy();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
