﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Raging : AIState
{
    EnemyController m_EC;
    RaycastHit hitTest;
    Vector2 awayFromGossiper;
    bool decidingHowToRespondToGossip;
    float contemplationStart, rageStart; //TODO duration should increase with chaos

    public AI_Raging(AIStateMachine stateMachine)
    {
        m_StateMachine = stateMachine;
        m_EC = m_StateMachine.AIController;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (decidingHowToRespondToGossip)
        {
            ThinkingTime();
        }
        else
        {
            if (Time.time > rageStart + m_EC.BodyData.Rage_rageDuration)
            {
                m_StateMachine.ChangeState(AIStates.Roaming);
                return;
            }
            else
            {
                //look for enemies
                m_EC.CheckForTargets();

                if (m_EC.IsActuallyGrounded && m_EC.WallIsInFront && m_EC.jumpState == AIJumpStates.notJumping)
                    m_EC.AttemptJump();
            }
        }

        /*m_EC.ConfirmTargetIsAlive();
        if (!m_EC.HasTarget)
        {
            m_StateMachine.ChangeState(AIStates.Roaming);
            return;
        }

        if (Physics.Raycast(m_EC.CanTargetReasonablyBeSeen(m_EC.TargetPerson.transform.position), out hitTest, m_EC.CurrentAttackStats.range + 0.25f, m_EC.CreatureMask))
        {
            if (hitTest.transform.root == m_EC.TargetPerson.transform.root)
            {
                m_EC.StopWalkRoutine();
                m_StateMachine.ChangeState(AIStates.Attacking);
            }
        }*/
    }

    void ThinkingTime()
    {
        if (Time.time > contemplationStart + m_EC.BodyData.Rage_rageStartDelay)
        {
            //contemplation over
            //pick outcome
            //1. pause, attack someone else
            //2. pause, attack gossiper

            //varients: delay time, line of sight.


            rageStart = Time.time;
            m_EC.SetEmotion(AIEmotions.rage);
            m_EC.urgencyMultiplier = m_EC.BodyData.runUrgency;
            m_EC.MovementAxis = awayFromGossiper.normalized * m_EC.urgencyMultiplier;
            decidingHowToRespondToGossip = false;
        }
    }

    public override void Enter()
    {
        base.Enter();
        m_EC.SetEmotion(AIEmotions.stunned);
        m_EC.MovementAxis = Vector2.zero;
        awayFromGossiper = new Vector2((new Vector3(m_EC.transform.position.x, m_EC.TargetOfInterest.transform.position.y) - m_EC.TargetOfInterest.transform.position).normalized.x * m_EC.urgencyMultiplier, 0); 
        decidingHowToRespondToGossip = true;
        contemplationStart = Time.time;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
