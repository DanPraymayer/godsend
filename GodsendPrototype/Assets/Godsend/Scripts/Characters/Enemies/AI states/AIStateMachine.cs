﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIStates
{
    None,
    Held,
    Stunned,
    Roaming,
    Patrolling,
    DecidingAttack,
    Positioning,
    Chasing,
    Attacking,
    ImmediateAttack,
    Retreating,
    FollowingUp,
    Reevaluating,
    Following,
    Fleeing,
    Retaliating,
    Raging
}

public class AIStateMachine : StateMachine
{
    public Dictionary<AIStates, AIState> AllStates { get; protected set; }

    public new AIState currentState { get; protected set; }
    public new AIState previousState { get; protected set; }

    public EnemyController AIController { get; protected set; }

    public AIStateMachine(EnemyController EC)
    {
        AIController = EC;
        AllStates = new Dictionary<AIStates, AIState>();
        AddStatesToDictionary();
        currentState = AllStates[AIStates.None];
    }

    void AddStatesToDictionary()
    {
        AllStates.Add(AIStates.None, new AI_None(this));
        AllStates.Add(AIStates.Held, new AI_Held(this));
        AllStates.Add(AIStates.Stunned, new AI_Stunned(this));
        AllStates.Add(AIStates.Roaming, new AI_Roaming(this));
        AllStates.Add(AIStates.DecidingAttack, new AI_DecidingAttack(this));
        AllStates.Add(AIStates.Positioning, new AI_PositioningStrategically(this));
        AllStates.Add(AIStates.Patrolling, new AI_Patrolling(this));
        AllStates.Add(AIStates.Chasing, new AI_Chasing(this));
        AllStates.Add(AIStates.Attacking, new AI_Attacking(this));
        AllStates.Add(AIStates.ImmediateAttack, new AI_AttackingHere(this));
        AllStates.Add(AIStates.Retreating, new AI_Retreating(this));
        AllStates.Add(AIStates.Reevaluating, new AI_Reevaluating(this));
        AllStates.Add(AIStates.Following, new AI_Following(this));
        AllStates.Add(AIStates.Fleeing, new AI_Fleeing(this));
        AllStates.Add(AIStates.Retaliating, new AI_Retaliating(this));
        AllStates.Add(AIStates.Raging, new AI_Raging(this));
    }

    public override void Update() { currentState.UpdateState(); }

    public virtual void ChangeState(AIStates nextState)
    {
        previousState = currentState;
        currentState = AllStates[nextState];

        currentState.Enter();
        previousState.Exit();
    }

    public virtual void ChangeState(AIState nextState)
    {
        previousState = currentState;
        currentState = nextState;

        currentState.Enter();
        previousState.Exit();
    }

    public virtual void ChangeState(AIState nextState, AIState oldState)
    {
        currentState = nextState;
        previousState = oldState;

        currentState.Enter();
        oldState.Exit();
    }

    public virtual void ReturnToPreviousState(AIState nextPreviousState)
    {
        this.ChangeState(previousState, nextPreviousState);
    }

    //public new virtual AIState GetState(int key){ return AllStates[key]; }
}
