﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Patrolling : AIState
{
    public AI_Patrolling(AIStateMachine stateMachine) {
        m_StateMachine = stateMachine;
    }

    public override void UpdateState()
    {
        base.UpdateState();

    }

    public override void Enter()
    {
        base.Enter();
        m_StateMachine.AIController.SetEmotion();
    }

    public override void Exit()
    {
        base.Exit();
    }

   /* IEnumerator PaceIdly()
    {
        shouldPatrol = true;
        urgencyMultiplier = 0.3f;
        float timeStamp = Time.time;
        //option to jump over holes if encountered midway into pace?
        while (CurrentAIState == AIStates.patrolling)
        {
            if (Time.time > timeStamp + 10)
                CurrentAIState = AIStates.roaming;

            //pace right
            {
                if (paceByRadius)
                {
                    targetPosition = new Vector3(paceRadius, 0);
                    yield return moveToPositionRoutine = StartCoroutine(GoToPosition());
                }
                else
                {
                    MovementAxis = new Vector2(urgencyMultiplier, 0); //Random.Range(0, 0.5f)
                    float tat = Time.time;
                    while (Time.time < tat + 3)
                    {
                        if (WallInFront() || (!roam_allowFalling && HoleInFront()))
                            break;
                        yield return null;
                    }
                    MovementAxis = Vector2.zero;
                }
                yield return new WaitForSeconds(Random.Range(0.5f, 3));
                yield return Timing.RunCoroutine(JumpOnce_RandomHeight(), Segment.FixedUpdate);
                yield return new WaitForSeconds(Random.Range(0.5f, 3));
            }
            //pace left
            {
                if (paceByRadius)
                {
                    targetPosition = new Vector3(-paceRadius, 0);
                    yield return moveToPositionRoutine = StartCoroutine(GoToPosition());
                }
                else
                {
                    MovementAxis = new Vector2(-urgencyMultiplier, 0);
                    float tat = Time.time;
                    while (Time.time < tat + 3)
                    {
                        if (WallInFront() || (!roam_allowFalling && HoleInFront()))
                            break;
                        yield return null;
                    }
                    MovementAxis = Vector2.zero;
                }
                yield return new WaitForSeconds(Random.Range(0.5f, 3));
                yield return Timing.RunCoroutine(JumpOnce_RandomHeight(), Segment.FixedUpdate);
                yield return new WaitForSeconds(Random.Range(0.5f, 3));
            }
        }

        patrollingRoutine = null;
        shouldPatrol = false;
    }*/
}
