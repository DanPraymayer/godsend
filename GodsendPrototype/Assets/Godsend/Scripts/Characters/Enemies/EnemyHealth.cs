﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : CharacterHealth {

    [Range(0,1)]
    public float precentHealthBeforeSwapable;

    public bool CanSwapInto { get { return CurrentPercentHealth <= precentHealthBeforeSwapable; } }

    public override void TakeDamage(float damage)
    {
        //CharacterBody.ta
        CharacterBody.AnimationController.WasHurt();
        base.TakeDamage(damage);
    }

    public override void TakeDamage(float damage, CharacterBody damageSource)
    {
        CharacterBody.CurrentController.TookDamage(damageSource);
        TakeDamage(damage);
    }

}
