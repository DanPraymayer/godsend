﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberMoveset : CharacterMoveset {

    IThrowable m_grabTarget;
    public IThrowable GrabTarget
    {
        get { return m_grabTarget; }
        set
        {
            if (CharacterBody.CurrentController.gameObject.layer == NamingConstants.PLAYER_LAYER)
            {
                if (value != null)
                    CanvasManager.CM.movesetUI.AbilityStandard.SetAbilityActive(true);
                else
                    CanvasManager.CM.movesetUI.AbilityStandard.SetAbilityActive(false);
            }
            m_grabTarget = value;
            holdingSomething = value != null;
        }
    }
    bool holdingSomething;
     
    GrabAttack m_grabAttack;
    public GrabAttack GrabAttack { get { return m_grabAttack; } }

    ThrowAttack m_throwAttack;
    public ThrowAttack throwAttack { get { return m_throwAttack; } }

    SpawnBombAttack m_spawnBombAttack;
    public SpawnBombAttack SpawnBombAttack { get { return m_spawnBombAttack; } }

    public override bool PrerequisitesMetForNormAttack { get { return GrabTarget != null; } }
    protected override void Start()
    {
        base.Start();

        m_grabAttack = GetComponentInChildren<GrabAttack>();
        m_throwAttack = GetComponentInChildren<ThrowAttack>();
        m_spawnBombAttack = GetComponentInChildren<SpawnBombAttack>();

    }

    public override void TryNormalAttack(LayerMask targetLayer)
    {
        if (m_grabAttack.CanAttack && GrabTarget == null)
        {
            CurrentAttackType = AttackType.NormalAttack;
            m_grabAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
        else if (m_throwAttack.CanAttack && GrabTarget != null)
        {
            CurrentAttackType = AttackType.NormalAttack;
            m_throwAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }

    public override void TryDownAttack(LayerMask targetLayer)
    {
        if (m_spawnBombAttack.CanAttack)
        {
            CurrentAttackType = AttackType.DownAttack;
            m_spawnBombAttack.StartAttack(targetLayer, m_characterBody.CurrentController.FaceDirection);
        }
    }

    public override void UpdateTargets()
    {
        if (GrabTarget != null)
        {
            GrabTarget.UpdatePosition(transform.position + Vector3.up * 2.5f);
        }
    }
}
