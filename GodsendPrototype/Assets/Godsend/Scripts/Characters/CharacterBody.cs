﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BodyType
{
    None,
    Player,
    Grunt,
    Hammer,
    Dasher,
    Thrower
}

public class CharacterBody : MonoBehaviour {

    public BodyType bodyType;
    public EnemyGangAffiliations EnemyGangAfiliation;

    CharacterController m_defaultController;
    int m_defaultLayer;

    CharacterController m_currentController;
    public CharacterController CurrentController { get { return m_currentController; } }

    int CurrentLayer { get { return m_currentController.gameObject.layer; } }

    public LayerMask m_breakableLayer;
    public LayerMask BreakableLayer { get { return m_breakableLayer; } }

    CharacterHealth m_characterHealth;
    public CharacterHealth CharacterHealth { get { return m_characterHealth; } }

    protected PlatformerMotor m_platformerMotor;
    public PlatformerMotor PlatformerMovement { get { return m_platformerMotor; } }

    protected AnimationController m_animatorController;
    public AnimationController AnimationController { get { return m_animatorController; } }

    CharacterMoveset m_characterMoveset;
    public CharacterMoveset CharacterMoveset { get { return m_characterMoveset; } }

    protected SkinnedMeshRenderer[] m_characterMeshes;
    public SkinnedMeshRenderer[] CharacterMeshes { get { return m_characterMeshes; } }

    protected DenizenInfo m_denizensInfo;
    public DenizenInfo DenizensInfo { get { return m_denizensInfo; } }

    List<BoxCollider> m_hitColliders = new List<BoxCollider>();
    public List<BoxCollider> HitColldiers { get { return m_hitColliders; } }

    public ParticleSystem possessionIndicator;

    protected virtual void Awake()
    {
        m_characterHealth = GetComponentInChildren<CharacterHealth>();
        m_animatorController = GetComponentInChildren<AnimationController>();
        m_platformerMotor = GetComponentInChildren<PlatformerMotor>();
        m_characterMoveset = GetComponentInChildren<CharacterMoveset>();
        m_characterMeshes = GetComponentsInChildren<SkinnedMeshRenderer>();


        m_hitColliders.AddRange(GetComponentsInChildren<BoxCollider>());

        m_currentController = m_defaultController = GetComponentInParent<CharacterController>();
        m_defaultLayer = CurrentLayer;
    }

    public void MoveEntity(Vector2 velocity)
    {
        m_currentController.transform.Translate(velocity);
    }


    public void ChangeController(CharacterController newController)
    {
        newController.transform.position = transform.position;

        transform.parent = newController.transform;

        m_currentController.CurrentBody = null;

        TransformHelper.SetTargetLayerRecursively(this.gameObject, CurrentLayer, newController.gameObject.layer);

        m_currentController = newController;
    }

    public void ResetController()
    {
        TransformHelper.SetTargetLayerRecursively(this.gameObject, CurrentLayer, m_defaultLayer);

        m_currentController = m_defaultController;

        m_defaultController.transform.position = transform.position;

        transform.parent = m_defaultController.transform;

        m_currentController.BodyControllerReset(this);
    }

    public void ChangeDenizenType(DenizenInfo newInfo)
    {
        m_denizensInfo = newInfo;
    }

}
