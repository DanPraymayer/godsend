﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveset : MonoBehaviour { 

    public enum AttackType
    {
        NotAttacking,
        NormalAttack,
        DownAttack,
        UpAttack
    }

    AttackType m_currentAttackType;
    public AttackType CurrentAttackType { get { return m_currentAttackType; }  set { m_currentAttackType = value; } }

    public bool DoingNormalAttack { get { return m_currentAttackType == AttackType.NormalAttack; } }
    public bool DoingDownAttack { get { return m_currentAttackType == AttackType.DownAttack; } }
    protected CharacterBody m_characterBody;
    public CharacterBody CharacterBody { get { return m_characterBody; } }

    public virtual bool PrerequisitesMetForDownAttack { get { return true; } }
    public virtual bool PrerequisitesMetForUpAttack { get { return true; } }
    public virtual bool PrerequisitesMetForNormAttack { get { return true; } }
    public virtual Vector3 SpecialConsideration { get { return m_characterBody.transform.position; } }

    protected virtual void Start()
    {
        m_characterBody = GetComponentInParent<CharacterBody>();
    }

    public virtual void TryNormalAttack(LayerMask targetLayer)
    {

    }

    public virtual void TryDownAttack(LayerMask targetLayer)
    {

    }

    public virtual void UpdateTargets()
    {

    }

}
