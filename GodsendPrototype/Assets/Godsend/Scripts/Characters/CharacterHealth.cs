﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour, ITakeDamage {

    CharacterBody m_characterBody;
    protected CharacterBody CharacterBody { get { return m_characterBody; } }

    [SerializeField]
    protected float m_maxHealth;
    public float MaxHealth { get { return m_maxHealth; } }


    public  float m_currentHealth;
    public float CurrentHealth { get { return m_currentHealth; } set { m_currentHealth = value; } }

    public float CurrentPercentHealth { get { return m_currentHealth / m_maxHealth; } }

    public delegate void HealthEvent();
    public event HealthEvent TakenDamage;


    protected virtual void Start()
    {
        m_characterBody = GetComponentInParent <CharacterBody>();

        ResetHealth();
    }

    public virtual void Die()
    {
        CharacterBody.CurrentController.Died();
    }

    public virtual void ResetHealth()
    {
        m_currentHealth = m_maxHealth;
    }

    public virtual void TakeDamage(float damage)
    {
        m_currentHealth -= damage;

        if(TakenDamage != null)
            TakenDamage();

        if (m_currentHealth <= 0)
            Die();

    }
    public virtual void TakeDamage(float damage, CharacterBody damageSource)
    {
        TakeDamage(damage);
    }

    public virtual void KnockBack(Vector2 knockBack)
    {
        CharacterBody.PlatformerMovement.AddForce(knockBack);
    }
}

