﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour, IControlCharacters
{

    public LayerMask m_targetLayer;
    public LayerMask TargetLayer { get { return m_targetLayer; } }

    CharacterBody m_currentBody;
    public CharacterBody CurrentBody { get { return m_currentBody; } set { m_currentBody = value; } }

    float m_faceDirection;
    public float FaceDirection { get { return m_faceDirection; } set { m_faceDirection = value; } }

    protected virtual void Start()
    {
        m_currentBody = GetComponentInChildren<CharacterBody>();
    }

    public virtual void TookDamage(CharacterBody damageSource) { }

    public virtual void Died() { }

    public virtual void Respawn() { }

    public virtual void BodyControllerReset(CharacterBody newBody)
    {
        CurrentBody = newBody;
    }
}
