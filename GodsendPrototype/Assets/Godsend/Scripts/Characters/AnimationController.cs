﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    Animator m_animator;
    public Animator Animator { get { return m_animator; } }


    public string SpeedBool;
    public string JumpBool;
    public string GroundedBool;
    public string HurtTrigger;
    public string NormalAttackBool;
    public string DownAttackBool;


    protected virtual void Start()
    {
        m_animator = GetComponentInChildren<Animator>();
    }

    public virtual void Move(float speed)
    {

    }

    public virtual void Jump(bool jumped)
    {

    }

    public virtual void OnGround(bool onGround)
    {

    }

    public virtual void WasHurt()
    {

    }

    public virtual void NormalAttack(bool attack)
    {

    }

    public virtual void DownAttack(bool attack)
    {

    }



    public virtual void Rotate(float angle)
    {
        Animator.transform.eulerAngles = new Vector3(0, angle, 0);
    }

}
