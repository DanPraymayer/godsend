﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharacterController
{

    PlayerInput m_playerInput;

    Possession m_possession;

    public Vector3 HookPos { get { return m_possession.hook.transform.position; } }

    CharacterBody m_ghostBody;
    public CharacterBody GhostBody { get { return m_ghostBody; } }

    public bool preventMovement;

    public static event Possession.PlayerEvent AcquiredBodyAtGameStart;

    HealthUI healthUI;

    public float timeScale;
    float previousTimeSinceStartup;

    IEnumerator StopTime;
    IEnumerator ResetTime;

    protected override void Start()
    {
        base.Start();

        m_ghostBody = CurrentBody;
        CurrentBody.ChangeDenizenType(EnemyManager.EM.DenizenClassInfoByReference(DenizenType.Player));


        m_playerInput = new PlayerInput(this);

        m_possession = GetComponentInChildren<Possession>();

        healthUI = GetComponent<HealthUI>();


        Possession.EnteredBody += EnteredBody;
        Possession.ExittedBody += LeftBody;

        SubscribeToBodyEvents();

        timeScale = Time.timeScale;
        StopTime = ChangeTime(0);
        ResetTime = ChangeTime(1);

        //AcquiredBodyAtGameStart();
        if (SpawnPointManager.SPM != null)
            SpawnPointManager.SPM.RequestSpawnPoint(this);

        if (GameObject.FindObjectOfType<InControl.InControlManager>() == null)
            Debug.LogError("InControl Input Manager not found. Player movement will not work until one is added to the scene.");
    }

    public void SubscribeToBodyEvents()
    {
        CurrentBody.CharacterHealth.TakenDamage += TakenDamage;
    }

    public void UnSubscribeToBodyEvents()
    {
        CurrentBody.CharacterHealth.TakenDamage -= TakenDamage;
    }

    private void OnDisable()
    {
        m_ghostBody.CharacterHealth.TakenDamage -= TakenDamage;
        UnSubscribeToBodyEvents();
    }

    void Update()
    {



        if (GameSceneManager.GamePaused || DebugGameSceneManager.DebugPaused)
            return;


        m_playerInput.Update();

        FindFaceDirection();

        CurrentBody.PlatformerMovement.GetMovementInput(m_playerInput.movementAxis);


        if (!preventMovement)
        {
            HandleJumping();
            HandleSoulshift();
            RotateSwapIndicator();
            HandleSlow();
            HandleBodyExit();
            HandleAttacks();

            CurrentBody.PlatformerMovement.ApplyGravity();
            CurrentBody.PlatformerMovement.TryToMoveCharacter();

            CurrentBody.AnimationController.Move(Mathf.Abs(CurrentBody.PlatformerMovement.velocity.x));
            CurrentBody.AnimationController.OnGround(CurrentBody.PlatformerMovement.PlatformerCollisions.Collisions.below);
            CurrentBody.AnimationController.Jump(m_playerInput.Jump.WasPressed);
            CurrentBody.AnimationController.NormalAttack(CurrentBody.CharacterMoveset.DoingNormalAttack);
            CurrentBody.AnimationController.DownAttack(CurrentBody.CharacterMoveset.DoingDownAttack);

            CurrentBody.AnimationController.Rotate(FaceDirection == 1 ? 90 : -90);

            if (Input.GetKeyDown(KeyCode.Keypad0))
                CurrentBody.CharacterHealth.TakeDamage(1);
        }

    }


    void HandleJumping()
    {
        if (m_playerInput.Jump.WasPressed)
            CurrentBody.PlatformerMovement.StartJump();

        if (m_playerInput.Jump.WasReleased)
            CurrentBody.PlatformerMovement.ReleaseJump();
    }

    void HandleSoulshift()
    {
        if (m_playerInput.SoulShift.WasPressed || (m_playerInput.Aim.Value.magnitude > .2f && !m_possession.TryingToSwap))
        {
            m_possession.StartSoulShiftAttempt();

            StartTimeStop();
        }
        else if (m_playerInput.SoulShift.IsPressed || m_playerInput.Aim.Value.magnitude > .2f)
        {
            m_possession.HoldSoulShiftAttempt();
        }
        else if (m_playerInput.SoulShift.WasReleased || (m_playerInput.Aim.Value.magnitude < .2f && m_possession.TryingToSwap))
        {
            m_possession.EndSoulShiftAttempt(m_playerInput.dirToAim);
            EndTimeStop();
            m_possession.TimeStopped = false;
        }
    }

    void RotateSwapIndicator()
    {
        if (m_possession.TryingToSwap)
        {
            if (m_playerInput.LastInputType == InControl.BindingSourceType.DeviceBindingSource)
            {
                if (m_playerInput.Aim.Value.magnitude < .2f)
                    m_possession.RotateIndicator(-90 * FaceDirection);
                else
                    m_possession.RotateIndicator(m_playerInput.angleToAim - 90);
            }
            else
            {
                m_possession.RotateIndicator(m_playerInput.angleToAim + 90);
            }

        }
    }

    void HandleSlow()
    {
        if (m_playerInput.SlowTime.WasPressed)
        {
            StartTimeStop();
        }
        else if (m_playerInput.SlowTime.WasReleased)
        {
            EndTimeStop();
        }
    }

    void HandleBodyExit()
    {
        if (m_playerInput.ExitBody.WasPressed)
            m_possession.ExitBody();
    }

    void HandleAttacks()
    {
        CurrentBody.CharacterMoveset.UpdateTargets();

        if (m_playerInput.Attack.WasPressed)
        {
            if (m_playerInput.HeldDown)
                CurrentBody.CharacterMoveset.TryDownAttack(TargetLayer | CurrentBody.BreakableLayer);
            else
                CurrentBody.CharacterMoveset.TryNormalAttack(TargetLayer | CurrentBody.BreakableLayer);
        }
        if (m_playerInput.Attack2.WasPressed)
        {
            CurrentBody.CharacterMoveset.TryDownAttack(TargetLayer | CurrentBody.BreakableLayer);
        }
    }

    void HandleAnimation() { }

    void FindFaceDirection()
    {
        if (Mathf.Abs(m_playerInput.movementAxis.x) > 0.01 && !CurrentBody.PlatformerMovement.preventXVelocity)
            FaceDirection = Mathf.Sign(m_playerInput.movementAxis.x);
    }


    void StartTimeStop()
    {
        StopAllCoroutines();
        StartCoroutine(ChangeTime(0));

        m_possession.TimeStopped = true;
    }

    void EndTimeStop()
    {
        StopAllCoroutines();
        StartCoroutine(ChangeTime(1));

        m_possession.TimeStopped = false;
    }

    IEnumerator ChangeTime(float target)
    {
        previousTimeSinceStartup = Time.realtimeSinceStartup;
        float elapsedTime = 0f;
        float startScale = timeScale;

        while (Mathf.Abs(target - timeScale) > .01f)
        {
            float realtimeSinceStartup = Time.realtimeSinceStartup;
            float deltaTime = realtimeSinceStartup - previousTimeSinceStartup;
            elapsedTime += deltaTime;
            previousTimeSinceStartup = realtimeSinceStartup;

            float t = Mathf.Clamp(elapsedTime / .25f, 0f, 1f);

            timeScale += target == 1 ? deltaTime : -deltaTime;


            if (target == 0)
            {
                t = 1 - Mathf.Cos(t * Mathf.PI * 0.5f);
            }
            else
            {
                t = Mathf.Sin(t * Mathf.PI * 0.5f);

            }

            timeScale = Mathf.Lerp(startScale, target, t);

            Time.timeScale = timeScale;

            yield return null;
        }

        timeScale = target;
        Time.timeScale = timeScale;

        yield return null;
    }

    public override void Died()
    {
        if (m_possession.HasBody)
        {
            CharacterBody previousBody = CurrentBody;

            m_possession.ExitBody();

            previousBody.CurrentController.Died();
        }
        else
        {
            SpawnPointManager.SPM.RequestRespawnPoint(this);
            Debug.Log("Game Over");
        }

    }

    public override void Respawn()
    {
        base.Respawn();
        CurrentBody.PlatformerMovement.CancelExtraForce();
        CurrentBody.PlatformerMovement.velocity = Vector3.zero;
        CurrentBody.CharacterHealth.CurrentHealth = CurrentBody.CharacterHealth.MaxHealth;
        healthUI.UpdateHealth(CurrentBody.CharacterHealth.CurrentPercentHealth);
    }

    public void EnteredBody()
    {
        healthUI.UpdateArmour(CurrentBody.CharacterHealth.CurrentPercentHealth);
    }

    public void LeftBody()
    {
        healthUI.HideArmour();
    }

    public void TakenDamage()
    {
        float newPercent = CurrentBody.CharacterHealth.CurrentPercentHealth;

        if (m_possession.HasBody)
            healthUI.UpdateArmour(newPercent);
        else
            healthUI.UpdateHealth(newPercent);
    }

}
