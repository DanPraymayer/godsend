﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Possession : MonoBehaviour
{

    public GameObject hook;

    public float m_swapTimer;

    public float possessionRadius;
    float possessionDistance;
    public float minDistance = 1;
    public float maxDistance = 5;
    public float timeToReachMaxSize = 1;

    float distanceGrowthRate;

    public float timeToExitBody;
    float exitTimer;

    public bool HasTimeBeforeExit { get { return exitTimer < timeToExitBody; } }

    public Vector2 leaveForce;

    bool m_tryingToSwap;
    public bool TryingToSwap { get { return m_tryingToSwap; } }

    bool m_timeStopped;
    public bool TimeStopped { get { return m_timeStopped; } set { m_timeStopped = value; } }

    bool insideBody;
    public bool HasBody { get { return insideBody; } }

    PlayerController m_player;

    CharacterBody m_targetBody;


    public delegate void PlayerEvent();
    public static event PlayerEvent ExittedBody;
    public static event PlayerEvent EnteredBody;
    public static event PlayerEvent EnteredBodyFromSelf;

    public static event PlayerEvent EnteredBodyFromBody;

    public event PlayerEvent UpdateGhostModeUIBorder;

    private void Start()
    {
        m_player = GetComponentInParent<PlayerController>();

        hook.transform.localScale = new Vector3(hook.transform.localScale.x, possessionDistance * 2.2f, hook.transform.localScale.z);

        distanceGrowthRate = (maxDistance - minDistance) / timeToReachMaxSize;

        if (CanvasManager.CM != null)
        {
            CanvasManager.CM.movesetUI.SetBody(BodyType.Player);
            CanvasManager.CM.movesetUI.SetGang(EnemyGangAffiliations.none);
        }
    }


    public void StartSoulShiftAttempt()
    {
        SetIndicator(true);
        possessionDistance = minDistance;
        hook.transform.localScale = new Vector3(hook.transform.localScale.x, possessionDistance * 2.2f, hook.transform.localScale.z);
    }

    public void HoldSoulShiftAttempt()
    {
        IncreasePossesionDistance();
    }

    public void EndSoulShiftAttempt(Vector2 direction)
    {
        if (m_tryingToSwap)
        {
            if (possessionDistance > maxDistance)
            {
                possessionDistance = maxDistance;
                hook.transform.localScale = new Vector3(hook.transform.localScale.x, possessionDistance * 2.2f, hook.transform.localScale.z);
            }

            SetIndicator(false);
            FirePossession(direction);
        }
    }

    void SetIndicator(bool value)
    {
        hook.SetActive(value);
        m_tryingToSwap = value;
    }

    void IncreasePossesionDistance()
    {
        if (possessionDistance < maxDistance)
        {
            possessionDistance += distanceGrowthRate * Time.deltaTime;
            hook.transform.localScale = new Vector3(hook.transform.localScale.x, possessionDistance * 2.2f, hook.transform.localScale.z);
        }
    }

    public void RotateIndicator(float angle)
    {
        hook.transform.eulerAngles = new Vector3(0, 0, angle);
    }

    void FirePossession(Vector2 direction)
    {
        RaycastHit hit;

        if (Physics.SphereCast(hook.transform.position, possessionRadius, direction, out hit, possessionDistance, m_player.TargetLayer))
        {
            m_targetBody = hit.transform.GetComponentInParent<CharacterBody>();

            if ((m_targetBody.CharacterHealth as EnemyHealth).CanSwapInto)
            {

                StartCoroutine(BodySwapAfterSwapTimer());

                EnemyController enemyController = m_targetBody.CurrentController.GetComponent<EnemyController>();

                if (enemyController != null)
                    enemyController.OnPossess();
            }
        }
    }

    IEnumerator BodySwapAfterSwapTimer()
    {
        m_player.preventMovement = true;
        yield return new WaitForSeconds(m_swapTimer);
        BodySwap(m_targetBody);
        m_player.preventMovement = false;
    }

    void BodySwap(CharacterBody newBody)
    {
        m_player.UnSubscribeToBodyEvents();
        if (!insideBody)
        {
            insideBody = true;
            m_player.GhostBody.gameObject.SetActive(false);
            // EnteredBodyFromSelf();

        }
        else
        {
            CharacterBody oldbody = m_player.CurrentBody;
            oldbody.ResetController();
        }

        m_player.CurrentBody = newBody;
        newBody.ChangeController(m_player);


        m_targetBody = null;
        EnteredBody();
        m_player.SubscribeToBodyEvents();
        CanvasManager.CM.movesetUI.SetBodyInfo(newBody.CharacterMoveset);
    }

    public void ExitBody()
    {
        if (insideBody)
        {
            insideBody = false;
            m_player.GhostBody.gameObject.SetActive(true);
            CharacterBody oldbody = m_player.CurrentBody;

            oldbody.ResetController();

            m_player.CurrentBody = m_player.GhostBody;

            m_player.CurrentBody.PlatformerMovement.AddForce(leaveForce);

            ExittedBody();
            m_player.SubscribeToBodyEvents();
            CanvasManager.CM.movesetUI.SetBody(BodyType.Player);
            CanvasManager.CM.movesetUI.SetGang(EnemyGangAffiliations.none);
        }
    }
}
