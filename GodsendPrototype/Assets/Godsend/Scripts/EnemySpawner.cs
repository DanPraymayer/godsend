﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    Vector3 highEnd, lowEnd;
    int UserMask;
    [System.Serializable]
    public struct EnemySpawn
    {
        public BodyType enemyType;
        public KeyCode keyThatSpawns;
    }
    public EnemySpawn[] SpawnOptions;


    void Start()
    {
        if (EnemyManager.EM == null)
            gameObject.SetActive(false);

        if (SpawnOptions.Length == 0)
            this.enabled = false;

        RaycastHit RH;
        Physics.Raycast(transform.position - Vector3.up, -Vector3.up, out RH, 50, LayerMask.GetMask("Ground"));
        lowEnd = RH.point;
        UserMask = LayerMask.GetMask("Player", "Enemy");
        highEnd = transform.position - Vector3.up;
    }

    void Update()
    {
        if (Input.anyKeyDown && Physics.OverlapCapsule(highEnd, lowEnd, 3, UserMask).Length > 0)
        {
            foreach (EnemySpawn e in SpawnOptions)
            {
                if (Input.GetKeyDown(e.keyThatSpawns))
                    EnemyManager.EM.SpawnCreature(e.enemyType, highEnd);
            }
        }
    }
}
