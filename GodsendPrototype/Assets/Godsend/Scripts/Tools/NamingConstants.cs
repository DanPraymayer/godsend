﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NamingConstants {

    public static string MIDDLE_GROUND = "Middleground";
    public static string PASSTHROUGH = "Passthrough";


    public static int PLAYER_LAYER = 13;
    public static int ENEMY_LAYER = 14;

    public static int GROUND_LAYER = 10;
    public static int RESETER_LAYER = 19;
}
