﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using System.Xml;


public class LevelLoader : MonoBehaviour {

    public Transform levelHolder;
    public Transform objectHolder;
    Transform middleGroundHolder;

    int TileStartIndex;
    int ObjectStartIndex;
    int SteepSlopeStartIndex;
    int GentleSlopeStartIndex;

    Grid myGrid;


    public enum MapDataFormat
    {
        Base64,
        CSV
    }

    class MapLayer
    {
        public string name;
        public float zPos;

        public string layerString;
        public List<int> layerData;
        public List<Tile> tiles;

        public Transform tileHolder;

    }

    class ObjectLayer
    {
        public string name;
        public float zPos;

        public List<LevelObject> objects;

    }

    class LevelObject
    {
        public int id;

        public Vector3 position;


    }

    class ColliderGroup
    {
        public int startY;
        public int startX;


        public int height;
        public int width;

        public Vector3 center;
        public Vector3 size;


        public List<Tile> tiles = new List<Tile>();

    }

    class ColliderColumn
    {
        public int startY;
        public int X;

        public int height;


        public List<Tile> tiles = new List<Tile>();
    }
    
    class ColliderRow
    {
        public int startX;
        public int Y;

        public int width;


        public List<Tile> tiles = new List<Tile>();
    }

    List<MapLayer> mapLayers = new List<MapLayer>();
    List<ObjectLayer> objectLayers = new List<ObjectLayer>();
    List<ColliderGroup> colliderGroups = new List<ColliderGroup>();

    public MapDataFormat mapDataFormat;

    public GameObject[] tilePrefabs;
    public GameObject[] objectPrefabs;
    public GameObject[] steepSlopes;
    public GameObject[] gentleSlopes;


    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string mapsDirectory;
    string TMXFile;

    public float tileWidth;
    public float tileHeight;

    int pixelsPerUnit = 100;

    int mapColumns;
    int mapRows;

    static void DestroyChildren(Transform parent)
    {
        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }

    public void LoadLevel()
    {

        DestroyChildren(levelHolder);
        DestroyChildren(objectHolder);

        GetMapDirectoryPath();

        ReadLayerData();
        GetMapDataForMapLayers();
        GetTileOffset();
        LoadTilesForAllLayers();

        ReadObjectData();
        CreateAllObjectsFromLayers();

        CreatePathfindingGrid();
        MergeColliders();
    }


    void GetMapDirectoryPath()
    {
        mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
        TMXFile = Path.Combine(mapsDirectory, TMXFilename);

    }

    void ReadLayerData()
    {
        mapLayers.Clear();

        string content = File.ReadAllText(TMXFile);

        using (XmlReader reader = XmlReader.Create(new StringReader(content)))
        {

            reader.ReadToFollowing("map");
            mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
            mapRows = Convert.ToInt32(reader.GetAttribute("height"));


            reader.ReadToFollowing("tileset");
            TileStartIndex = Convert.ToInt32(reader.GetAttribute("firstgid")) - 1;

            reader.ReadToFollowing("tileset");
            ObjectStartIndex = Convert.ToInt32(reader.GetAttribute("firstgid")) - 1;

            reader.ReadToFollowing("tileset");
            SteepSlopeStartIndex = Convert.ToInt32(reader.GetAttribute("firstgid")) - 1;

            reader.ReadToFollowing("tileset");
            GentleSlopeStartIndex = Convert.ToInt32(reader.GetAttribute("firstgid")) - 1;




            while (reader.ReadToFollowing("layer"))
            {

                MapLayer newLayer = new MapLayer();

                newLayer.name = reader.GetAttribute("name");

                if (reader.ReadToFollowing("properties"))
                {
                    if (reader.ReadToDescendant("property"))
                    {
                        newLayer.zPos = Convert.ToSingle(reader.GetAttribute("value"));
                    }
                }

                reader.ReadToFollowing("data");

                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType)
                {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                newLayer.layerString = reader.ReadElementContentAsString().Trim();

                mapLayers.Add(newLayer);
            }
        }
    }

    void GetMapDataForMapLayers()
    {

        for (int i = 0; i < mapLayers.Count; i++)
        {
            List<int> layerData = GetMapData(mapLayers[i].layerString);

            mapLayers[i].layerData = layerData;

        }

    }

    List<int> GetMapData(string dataString)
    {

        List<int> mapData = new List<int>();

        switch (mapDataFormat)
        {

            case MapDataFormat.Base64:

                byte[] bytes = Convert.FromBase64String(dataString);
                int index = 0;
                while (index < bytes.Length)
                {
                    int tileID = BitConverter.ToInt32(bytes, index) -1;
                    mapData.Add(tileID);
                    index += 4;
                }
                break;


            case MapDataFormat.CSV:

                string[] lines = dataString.Split(new string[] { " " }, StringSplitOptions.None);
                foreach (string line in lines)
                {
                    string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                    foreach (string value in values)
                    {
                        int tileID = Convert.ToInt32(value) -1;
                        mapData.Add(tileID);
                    }
                }
                break;

        }

        return mapData;

    }

    void GetTileOffset()
    {
        tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
        mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);
    }

    void LoadTilesForAllLayers()
    {
        for (int i = 0; i < mapLayers.Count; i++)
        {
            LoadTilesForLayer(mapLayers[i]);
        }
    }

    void LoadTilesForLayer(MapLayer layer)
    {

        layer.tiles = new List<Tile>();

        GameObject newHolder = new GameObject(layer.name);
        newHolder.transform.parent = levelHolder;
        layer.tileHolder = newHolder.transform;

        for (int y = 0; y < mapRows; y++)
        {
            for (int x = 0; x < mapColumns; x++)
            {

                int mapDatatIndex = x + (y * mapColumns);
                int tileID = layer.layerData[mapDatatIndex];


                float xPos = 0;
                float yPos = 0;

                bool rotateTile = false;

                Tile newTile = new Tile();
                
                if (tileID > -1)
                {
                    GameObject tilePrefab = null;

                    if(tileID < SteepSlopeStartIndex)
                    {
                        tilePrefab = tilePrefabs[tileID];

                        xPos = x * tileWidth;
                        yPos = -y * tileHeight;

                    }
                    else if (tileID < GentleSlopeStartIndex)
                    {

                        int slopeID = (tileID - SteepSlopeStartIndex)/2;

                        rotateTile = (tileID - SteepSlopeStartIndex) % 2 == 0;

                        tilePrefab = steepSlopes[slopeID];


                        xPos = x * tileWidth + tileWidth;
                        yPos = -y * tileHeight - tileHeight / 2;

                    }
                    else
                    {
                        int slopeID = (tileID - GentleSlopeStartIndex) / 2;

                        rotateTile = (tileID - GentleSlopeStartIndex) % 2 == 0;

                        tilePrefab = gentleSlopes[slopeID];


                        xPos = x * tileWidth + tileWidth;
                        yPos = -y * tileHeight - tileHeight / 2;

                    }

                    GameObject tileObject = Instantiate(tilePrefab, new Vector3(xPos, yPos, -layer.zPos) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    if (rotateTile)
                        tileObject.transform.localEulerAngles = new Vector3(0, 180, 0);

                    tileObject.transform.parent = newHolder.transform;

                    newTile.position = tileObject.transform.position;
                    newTile.gameObject = tileObject;

                    newTile.tileId = tileID;

                    if (newTile.tileId < ObjectStartIndex/2)
                        newTile.tileType = TileType.FullBlock;
                    else if(newTile.tileId < ObjectStartIndex)
                        newTile.tileType = TileType.HalfBlock;



                }


                newTile.gridX = x;
                newTile.gridY = y;

                newTile.tileId = tileID;

                layer.tiles.Add(newTile);
            }
        }
    }

    void ReadObjectData()
    {

        objectLayers.Clear();

        string content = File.ReadAllText(TMXFile);

        using (XmlReader reader = XmlReader.Create(new StringReader(content)))
        {

            while (reader.ReadToFollowing("objectgroup"))
            {

                ObjectLayer newObjectLayer = new ObjectLayer();
                newObjectLayer.objects = new List<LevelObject>();

                newObjectLayer.name = reader.GetAttribute("name");



                if (reader.ReadToFollowing("properties"))
                {
                    if (reader.ReadToDescendant("property"))
                    {
                        newObjectLayer.zPos = Convert.ToSingle(reader.GetAttribute("value"));
                    }
                }


                if (reader.ReadToFollowing("object"))
                {
                    do
                    {
                        LevelObject newLevelObject = new LevelObject();

                        newLevelObject.id = Convert.ToInt32(reader.GetAttribute("gid")) - 1 - tilePrefabs.Length;

                        float x = Convert.ToSingle(reader.GetAttribute("x")) / (pixelsPerUnit);
                        float y = Convert.ToSingle(reader.GetAttribute("y")) / (pixelsPerUnit);
                        float z = -newObjectLayer.zPos;

                        x += Convert.ToSingle(reader.GetAttribute("width")) *.5f / (pixelsPerUnit);
                        y -= Convert.ToSingle(reader.GetAttribute("height")) *.5f / (pixelsPerUnit);


                        newLevelObject.position = new Vector3(x, -y, z) + mapCenterOffset;

                        newObjectLayer.objects.Add(newLevelObject);

                    } while (reader.ReadToNextSibling("object"));


                    objectLayers.Add(newObjectLayer);

                }

            }
        }

    }


    void CreateAllObjectsFromLayers()
    {
        for (int i = 0; i < objectLayers.Count; i++)
        {
            CreateAllObjectsOnLayer(objectLayers[i]);
        }
    }

    void CreateAllObjectsOnLayer(ObjectLayer objectLayer)
    {

        GameObject newHolder = new GameObject(objectLayer.name);
        newHolder.transform.parent = objectHolder;


        foreach (LevelObject levelObject in objectLayer.objects)
        {
            Instantiate(objectPrefabs[levelObject.id], levelObject.position, Quaternion.identity, newHolder.transform);
        }


    }


    void CreatePathfindingGrid()
    {

        MapLayer middleground = new MapLayer();

        foreach (MapLayer layer in mapLayers)
        {
            if (layer.name.Equals(NamingConstants.MIDDLE_GROUND))
            {
                middleground = layer;
                middleGroundHolder = layer.tileHolder;
            }
        }

        Grid newGrid = new Grid(middleground.tiles,mapColumns,mapRows);

        myGrid = newGrid;

    }

    void MergeColliders()
    {
        colliderGroups.Clear();

        GameObject groupsHolder = new GameObject("Groups");
        groupsHolder.transform.parent = middleGroundHolder;

        Grid tileGrid = myGrid;

        //Group Full Tiles IN Columns
        List<ColliderColumn> colliderColumns = new List<ColliderColumn>();
        for (int x = 0; x < mapColumns; x++)
        {
            bool columnGrouping = false;

            ColliderColumn currentColliderColumn = new ColliderColumn();

            for (int y = 0; y < mapRows; y++)
            {
                int index = x + (y * mapColumns);

                if (tileGrid.gridTiles[index].tileType == TileType.FullBlock)
                {
                    if(!columnGrouping)
                    {
                        currentColliderColumn = new ColliderColumn();
                        currentColliderColumn.X = x;
                        currentColliderColumn.startY = y;
                        columnGrouping = true;
                        currentColliderColumn.tiles.Add(tileGrid.gridTiles[index]);
                    }
                    else
                    {
                        currentColliderColumn.height++;
                        currentColliderColumn.tiles.Add(tileGrid.gridTiles[index]);

                    }


                }
                else if (columnGrouping)
                {
                    colliderColumns.Add(currentColliderColumn);

                    columnGrouping = false;
                }
            }
            if (columnGrouping)
            {
                colliderColumns.Add(currentColliderColumn);
            }
            columnGrouping = false;

        }

        //Group Half Tiles Into Rows
        List<ColliderRow> colliderRows = new List<ColliderRow>();
        for (int y = 0; y < mapRows; y++)
        {
            bool columnGrouping = false;

            ColliderRow currentColliderRow = new ColliderRow();

            for (int x = 0; x < mapColumns; x++)
            {
                int index = x + (y * mapColumns);

                if (tileGrid.gridTiles[index].tileType == TileType.HalfBlock)
                {
                    if(!columnGrouping)
                    {
                        currentColliderRow = new ColliderRow();
                        currentColliderRow.Y = y;
                        currentColliderRow.startX = x;
                        columnGrouping = true;
                        currentColliderRow.tiles.Add(tileGrid.gridTiles[index]);
                    }
                    else
                    {
                        currentColliderRow.width++;
                        currentColliderRow.tiles.Add(tileGrid.gridTiles[index]);

                    }


                }
                else if (columnGrouping)
                {
                    colliderRows.Add(currentColliderRow);

                    columnGrouping = false;
                }
            }
            if (columnGrouping)
            {
                colliderRows.Add(currentColliderRow);
            }
            columnGrouping = false;

        }

        //Group Full Tiles Columns Into Groups
        List<ColliderColumn> assignedColumns = new List<ColliderColumn>();
        int currentX = 0;
        foreach (ColliderColumn column in colliderColumns)
        {

            ColliderGroup colliderGroup = new ColliderGroup();

            if (!assignedColumns.Contains(column))
            {
                colliderGroup.height = column.height;
                colliderGroup.startX = currentX = column.X;
                colliderGroup.startY = column.startY;
                colliderGroup.tiles.AddRange(column.tiles);

                assignedColumns.Add(column);

                foreach (ColliderColumn otherColumn in colliderColumns)
                {

                    if (!assignedColumns.Contains(otherColumn))
                    {

                        if (otherColumn.startY == column.startY && otherColumn.height == column.height && otherColumn.X == currentX + 1)
                        {
                            currentX++;
                            colliderGroup.width++;

                            colliderGroup.tiles.AddRange(otherColumn.tiles);
                            assignedColumns.Add(otherColumn);
                        }

                    }

                }

            }

            if (colliderGroup.tiles.Count > 0)
                colliderGroups.Add(colliderGroup);
        }

        //Add Colliders To FullTile Groups
        foreach (ColliderGroup group in colliderGroups)
        {

            GameObject colliderObject = new GameObject("Group");
            colliderObject.transform.parent = groupsHolder.transform;
            colliderObject.layer = NamingConstants.GROUND_LAYER;

            foreach (Tile tile in group.tiles)
            {
              tile.gameObject.transform.parent = colliderObject.transform;

            }

            BoxCollider boxCollider = colliderObject.AddComponent<BoxCollider>();

            float xPos = (group.startX * tileWidth) + mapCenterOffset.x + (tileCenterOffset.x * (group.width+1));
            float yPos = (-group.startY * tileHeight) + mapCenterOffset.y + (tileCenterOffset.y * (group.height + 1)); ;

            boxCollider.center = new Vector3(xPos, yPos, 0);
            boxCollider.size = new Vector3(group.width * tileWidth + .5f, group.height * tileHeight + .5f, 1);


        }

        //Add Colliders To HalfTile Rows
        foreach (ColliderRow row in colliderRows)
        {

            GameObject colliderObject = new GameObject("Group");
            colliderObject.transform.parent = groupsHolder.transform;
            colliderObject.layer = NamingConstants.GROUND_LAYER;
            colliderObject.tag = NamingConstants.PASSTHROUGH;

            foreach (Tile tile in row.tiles)
            {
                tile.gameObject.transform.parent = colliderObject.transform;

            }

            BoxCollider boxCollider = colliderObject.AddComponent<BoxCollider>();

            float xPos = (row.startX * tileWidth) + mapCenterOffset.x + (tileCenterOffset.x * (row.width + 1));
            float yPos = (-row.Y * tileHeight) + mapCenterOffset.y + (tileCenterOffset.y * .5f); ;

            boxCollider.center = new Vector3(xPos, yPos, 0);
            boxCollider.size = new Vector3(row.width * tileWidth + .5f, .25f, 1);


        }

    }


    void MergeMeshes()
    {

    }


}
