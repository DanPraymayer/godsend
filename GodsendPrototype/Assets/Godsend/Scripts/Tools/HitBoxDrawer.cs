﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxDrawer : MonoBehaviour {


    public bool drawHitboxes;
    public bool drawHurtboxes;


    public BoxCollider[] boxColliders;

    public List<HurtBox> hurtBoxes = new List<HurtBox>();

    public void DrawHitBoxes()
    {
        foreach (BoxCollider hitBox in boxColliders)
        {
            DebugExtension.DrawBounds(hitBox.bounds,Color.green);
        }
    }

    public void DrawHurtBoxes()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            DebugExtension.DrawBounds(hurtBox.bounds, Color.red);
        }
    }

    public void OnDrawGizmos()
    {
        if(drawHitboxes)
            DrawHitBoxes();

        if(drawHurtboxes)
            DrawHurtBoxes();

    }







}
