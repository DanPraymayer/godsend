﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformHelper {

    public static GameObject FindHighestParentObject(GameObject obj)
    {
        return obj.transform.root.gameObject;
    }

    public static bool ContainsLayer(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    public static void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (null == obj)
        {
            return;
        }

        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }
    public static void SetTargetLayerRecursively(GameObject obj, int targetLayer, int newLayer)
    {
        if (null == obj)
        {
            return;
        }
        if (obj.layer == targetLayer)
            obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetTargetLayerRecursively(child.gameObject, targetLayer, newLayer);
        }
    }

}
