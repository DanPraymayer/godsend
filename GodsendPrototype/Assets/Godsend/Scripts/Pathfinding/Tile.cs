﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    None = -1,
    FullBlock,
    HalfBlock,
}

public class Tile {

    public int tileId;
    public int gridX;
    public int gridY;
    public Vector3 position;

    public TileType tileType = TileType.None;
    public bool passable;
    public bool passthrough;

    public GameObject gameObject;

}
