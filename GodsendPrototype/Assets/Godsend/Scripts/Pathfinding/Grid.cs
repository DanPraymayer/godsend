﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {

    public List<Tile> gridTiles;

    int gridColumns;
    int gridRows;

    public Grid(List<Tile> tiles, int columns, int rows )
    {
        gridTiles = tiles;
        gridColumns = columns;
        gridRows = rows;
    }
    


}
