﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlatformerCapsule))]
public class PlatformerCollisions : MonoBehaviour
{

    PlatformerCapsule m_platformerCapsule;

    PlatformerMotor m_platformerMotor;

    Vector2 m_playerInput;

    [SerializeField]
    public LayerMask collisionMask;
    public float maxClimbAngle = 60;
    public float maxDescendAngle = 75;

    CollisionInfo collisions;
    public CollisionInfo Collisions { get { return collisions; } }

    public bool IsGrounded { get { return Collisions.below; } }

    public void Start()
    {
        m_platformerMotor = GetComponentInParent<PlatformerMotor>();

        m_platformerCapsule = GetComponentInChildren<PlatformerCapsule>();

        collisions.faceDir = 1;

    }

    public void CheckForCollisions(Vector3 velocity, Vector2 input)
    {
        collisions.Reset();
        collisions.velocityOld = velocity;
        m_playerInput = input;

        if (velocity.y < 0)
        {
            DescendSlope(ref velocity);
        }

        if (velocity.x != 0)
        {
            collisions.faceDir = (int)Mathf.Sign(velocity.x);
            HorizontalCollisions(ref velocity);
        }

        if (velocity.y != 0)
        {
            VerticalCollisions(ref velocity);
        }


        m_platformerMotor.MoveCharacter(velocity);

    }

    void HorizontalCollisions(ref Vector3 velocity)
    {
        float directionX = collisions.faceDir;
        float rayLength = Mathf.Abs(velocity.x) + m_platformerCapsule.SkinWidth;

        if (Mathf.Abs(velocity.x) < m_platformerCapsule.SkinWidth)
        {
            rayLength = 2 * m_platformerCapsule.SkinWidth;
        }

        RaycastHit[] hits = new RaycastHit[5];

        int numberOfHits = Physics.CapsuleCastNonAlloc(m_platformerCapsule.Top, m_platformerCapsule.Bottom, m_platformerCapsule.SkinRadius, Vector2.right * directionX, hits, rayLength, collisionMask);


        if (numberOfHits > 0)
        {

            RaycastHit closestHit = hits[0];

            foreach (RaycastHit hit in hits)
            {
                if (closestHit.distance < hit.distance)
                    closestHit = hit;
            }


            if (closestHit.distance == 0 || (closestHit.collider.tag.Equals(NamingConstants.PASSTHROUGH) && velocity.y > 0))
            {
                return;
            }

            float slopeAngle = Vector2.Angle(closestHit.normal, Vector2.up);

            if (slopeAngle <= maxClimbAngle)
            {
                if (collisions.descendingSlope)
                {
                    collisions.descendingSlope = false;
                    velocity = collisions.velocityOld;
                }

                float distanceToSlopeStart = 0;

                if (slopeAngle != collisions.slopeAngleOld)
                {
                    distanceToSlopeStart = closestHit.distance - m_platformerCapsule.SkinWidth;
                    velocity.x -= distanceToSlopeStart * directionX;
                }

                ClimbSlope(ref velocity, slopeAngle);
                velocity.x += distanceToSlopeStart * directionX;

            }

            if (!collisions.climbingSlope || slopeAngle > maxClimbAngle)
            {
                velocity.x = (closestHit.distance - m_platformerCapsule.SkinWidth) * directionX;

                if (collisions.climbingSlope)
                {
                    velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
                }

                collisions.left = directionX == -1;
                collisions.right = directionX == 1;


            }

        }
    }


    void VerticalCollisions(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y) + m_platformerCapsule.SkinWidth;

        Vector3 xDisplacement = new Vector3(velocity.x, 0);

        Vector3 rayOrigin = directionY == 1 ? m_platformerCapsule.Top : m_platformerCapsule.Bottom;

        RaycastHit hit;

        //if (Physics.CapsuleCast(m_platformerCapsule.Top, m_platformerCapsule.Bottom, m_platformerCapsule.SkinRadius, Vector2.right * directionX, out hit, rayLength, collisionMask))
        if (Physics.SphereCast(rayOrigin /*+ xDisplacement*/, m_platformerCapsule.SkinRadius, Vector2.up * directionY, out hit, rayLength, collisionMask))
        {

            Vector3 normal = hit.normal;
            Vector3 input = Vector3.one;

            input = Vector3.ProjectOnPlane(input, normal).normalized;

            if (hit.collider.tag.Equals(NamingConstants.PASSTHROUGH))
            {
                if (directionY == 1 || hit.distance == 0)
                {
                    return;
                }
                if (m_playerInput.y == -1)
                {
                    return;
                }


            }
            velocity.y = (hit.distance - m_platformerCapsule.SkinWidth) * directionY;

            if (collisions.climbingSlope)
            {
                velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
            }

            collisions.below = directionY == -1;
            collisions.above = directionY == 1;
        }

        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(velocity.x);
            rayLength = Mathf.Abs(velocity.x) + m_platformerCapsule.SkinWidth;
            Vector3 rayDisplacement = Vector3.up * velocity.y;

            if (Physics.CapsuleCast(m_platformerCapsule.Top + rayDisplacement, m_platformerCapsule.Bottom + rayDisplacement, m_platformerCapsule.SkinRadius, Vector2.right * directionX, out hit, rayLength, collisionMask))
            {

                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    velocity.x = (hit.distance - m_platformerCapsule.SkinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                }

            }


        }


    }

    void ClimbSlope(ref Vector3 velocity, float slopeAngle)
    {

        float moveDistance = Mathf.Abs(velocity.x);
        float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (velocity.y <= climbVelocityY)
        {
            velocity.y = climbVelocityY;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
        }

    }

    void DescendSlope(ref Vector3 velocity)
    {
        float directionX = Mathf.Sign(velocity.x);
        RaycastHit hit;
        if (Physics.SphereCast(m_platformerCapsule.Bottom, m_platformerCapsule.SkinRadius, -Vector2.up, out hit, Mathf.Infinity, collisionMask))
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

            if (slopeAngle != 0 && slopeAngle <= maxDescendAngle)
            {
                if (Mathf.Sign(hit.normal.x) == directionX)
                {
                    if (hit.distance - m_platformerCapsule.SkinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x))
                    {
                        float moveDistance = Mathf.Abs(velocity.x);
                        float descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                        velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
                        velocity.y -= descendVelocityY;

                        collisions.slopeAngle = slopeAngle;
                        collisions.descendingSlope = true;
                        collisions.below = true;
                    }
                }
            }
        }
    }


    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public bool climbingSlope;
        public bool descendingSlope;
        public float slopeAngle, slopeAngleOld;
        public Vector3 velocityOld;
        public int faceDir;
        public bool fallingThroughPlatform;
        public Collider2D fallThroughPlatform;
        public void Reset()
        {
            above = below = false;
            left = right = false;
            climbingSlope = false;
            descendingSlope = false;
            fallThroughPlatform = null;
            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }


}


