﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlatformerCollisions))]
public class PlatformerMotor : MonoBehaviour, IThrowable
{
    PlatformerCollisions m_platformerCollisions;

    public PlatformerCollisions PlatformerCollisions { get { return m_platformerCollisions; } }


    CharacterBody m_currentController;
    public CharacterBody CurrentController { get { return m_currentController; } }

    [SerializeField]
    MovementData m_movementData;
    public MovementData MovementData { get { return m_movementData; } }

    public Vector3 velocity;
    Vector3 extraForce;

    Vector2 playerInput;

    float velocityXSmoothing;
    bool minJump;

    public bool preventXVelocity;
    public bool preventYVelocity;

    private void Start()
    {
        m_platformerCollisions = GetComponentInChildren<PlatformerCollisions>();
        m_currentController = GetComponentInParent<CharacterBody>();

        m_movementData.CalulateMovementValues();
    }


    public void GetMovementInput(Vector2 input)
    {

        playerInput = input;

        int wallDirX = (m_platformerCollisions.Collisions.left) ? -1 : 1;
        float targetVelocityX;

        if (!preventXVelocity)
            targetVelocityX = playerInput.x * m_movementData.moveSpeed;
        else
            targetVelocityX = velocity.x * .2f;

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (m_platformerCollisions.Collisions.below) ? m_movementData.groundedAccelerationTime : m_movementData.airborneAccelerationTime);
    }

    public void AddForce(Vector3 extraForce)
    {

        this.extraForce = extraForce;

    }

    public void CancelExtraForce()
    {
        this.extraForce = Vector3.zero;
    }

    public void StartJump()
    {
        if (m_platformerCollisions.Collisions.below)
        {
            minJump = false;
            velocity.y = m_movementData.MaxJumpVelocity;
        }
    }

    public void ReleaseJump()
    {
        minJump = true;
        if (velocity.y > m_movementData.MinJumpVelocity)
        {
            velocity.y = m_movementData.MinJumpVelocity;
        }
    }

    public void ApplyGravity()
    {
        velocity.y += m_movementData.Gravity * Time.deltaTime;

        if (velocity.y < 0)
            velocity.y += m_movementData.Gravity * (m_movementData.fallMultiplier) * Time.deltaTime;
        else if (velocity.y > 0 && minJump)
        {
            velocity.y += m_movementData.Gravity * (m_movementData.lowJumpMultiplier) * Time.deltaTime;
        }
    }

    public void TryToMoveCharacter()
    {

        if (preventYVelocity && velocity.y < 0)
            velocity.y = 0;

        if (extraForce.magnitude != 0)
        {
            if (velocity.y < extraForce.y)
                velocity.y = extraForce.y;

            velocity.x += extraForce.x;
        }


        m_platformerCollisions.CheckForCollisions(velocity * Time.deltaTime, playerInput);


        if (m_platformerCollisions.Collisions.above || m_platformerCollisions.Collisions.below)
        {
            velocity.y = 0;
        }

        extraForce = Vector2.zero;
    }

    public void MoveCharacter(Vector2 velocity)
    {
        m_currentController.MoveEntity(velocity);
    }

    public void SetPosition(Vector3 newPosition)
    {
        m_currentController.transform.root.position = newPosition;
    }

    public void AddThrowForce(Vector3 force)
    {
        EnemyController enemyController = GetComponentInParent<EnemyController>();
        if (enemyController != null)
        {
            enemyController.GoToState(AIStates.Stunned);
        }
        AddForce(force);
    }

    public void UpdatePosition(Vector3 newPosition)
    {
        SetPosition(newPosition);
    }

    public Vector3 ReturnPosition()
    {
        return transform.parent.transform.position;
    }

    public void SetParentInactive()
    {
        EnemyController enemyController = GetComponentInParent<EnemyController>();
        if (enemyController != null)
        {
            enemyController.GoToState(AIStates.Held);
        }
    }

    public void SetParentActive()
    {
        m_currentController.GetComponentInParent<EnemyController>().GoToState(AIStates.Stunned);
        /*//CharacterBody oldbody = m_player.CurrentBody;
        m_currentController.ResetController();

        EnemyController enemyController = GetComponentInParent<EnemyController>();
        if (enemyController != null)
        {
            //enemyController.transform.position = this.transform.position;
            enemyController.GoToState(AIStates.Stunned);
        }*/
    }
}