﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerCapsule : MonoBehaviour {

    CapsuleCollider m_capsuleCollider;
    public CapsuleCollider CapsuleCollider { get { return m_capsuleCollider; } }

    [SerializeField]
    float skinWidth = .015f;
    public float SkinWidth { get { return skinWidth; } }

    public float Radius { get { return m_capsuleCollider.radius; } }
    public float SkinRadius { get { return m_capsuleCollider.radius - skinWidth; } }



    Vector3 m_top;
    public Vector3 Top { get { return transform.position + m_top; } }

    Vector3 m_bottom;
    public Vector3 Bottom { get { return transform.position + m_bottom; } }

    Vector3 m_GroundPoint;
    public Vector3 GroundPoint { get { return transform.position + m_GroundPoint; } }

    private void Start()
    {
        m_capsuleCollider = GetComponentInChildren<CapsuleCollider>();

        SetupPositions();
    }

    void SetupPositions()
    {

        float distanceToPoint = m_capsuleCollider.height * 0.5F - Radius;

        m_top = m_capsuleCollider.center + Vector3.up * distanceToPoint;

        m_bottom = m_capsuleCollider.center - Vector3.up * distanceToPoint;

        m_GroundPoint = new Vector3(m_bottom.x,m_bottom.y - Radius, m_bottom.z);

    }
    

}
