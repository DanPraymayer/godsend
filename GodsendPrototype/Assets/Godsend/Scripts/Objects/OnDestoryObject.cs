﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestoryObject : InteractableObject {


    public override void Die()
    {
        Interaction.Interacted();
        base.Die();
    }

}
