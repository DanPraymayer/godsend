﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetNewPosition : MonoBehaviour {

    public Transform target;

    public Vector3 newPosition;
    public float newRotaion;


    public void MoveToNewPosition()
    {
        transform.position = newPosition;
        transform.eulerAngles = new Vector3(0,0,newRotaion);
    }


}
