﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleObject : MonoBehaviour {

    
    bool on = true;

    public GameObject target;

    public void Toggle()
    {
        on = !on;
        SetActiveTarget();
    }

    void SetActiveTarget()
    {
        target.SetActive(on);
    }

}
