﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePosition : MonoBehaviour
{

    bool on;
    public Transform target;

    Vector3 startPosition;
    Vector3 startRotation;
    Vector3 endPosition;
    Vector3 endRotation;

    public bool changePosition;
    public Vector3 positionOffset;
    public bool changeRotation;
    public float rotationOffset;
    Vector3 altRotationVal;
    public bool OnlyClickOnce;
    bool clickedOnce;

    // Use this for initialization
    void Start()
    {
        on = true;
        clickedOnce = false;
        startPosition = target.localPosition;
        startRotation = target.eulerAngles;

        endPosition = startPosition + positionOffset;

        if (changeRotation)
            altRotationVal = new Vector3(0, 0, rotationOffset);
        else
            altRotationVal = new Vector3(0, 0, target.rotation.eulerAngles.z);

    }

    public void Toggle()
    {
        on = !on;
        SetPosition();
    }

    void SetPosition()
    {
        /*if (OnlyClickOnce && clickedOnce)
            return;

        clickedOnce = true;*/

        if (changePosition)
            target.localPosition = on ? startPosition : endPosition;
        if (changeRotation)
            target.eulerAngles = on ? startRotation : altRotationVal;
    }
}
