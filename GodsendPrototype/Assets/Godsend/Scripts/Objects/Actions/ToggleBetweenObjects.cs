﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleBetweenObjects : MonoBehaviour {

    bool on = true;

    public GameObject onObject;
    public GameObject offObject;

    public void Toggle()
    {
        on = !on;
        SetActiveTarget();
    }

    void SetActiveTarget()
    {
        onObject.SetActive(on);
        offObject.SetActive(!on);
    }



}
