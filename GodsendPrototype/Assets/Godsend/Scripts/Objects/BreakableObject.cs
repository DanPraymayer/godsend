﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour, ITakeDamage
{

    public float maxHealth;
    protected float m_health;

    protected virtual void Start()
    {
        ResetHealth();
    }

    public virtual void Die()
    {
        gameObject.SetActive(false);
    }

    public void KnockBack(Vector2 knockBack) { }

    public void ResetHealth()
    {
        m_health = maxHealth;
    }

    public virtual void TakeDamage(float damage)
    {
        m_health -= damage;

        if (m_health <= 0)
            Die();
    }

    public virtual void TakeDamage(float damage, CharacterBody source)
    {
        TakeDamage(damage);
    }
}
