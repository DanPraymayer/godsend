﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnHitObject : InteractableObject {

    public override void TakeDamage(float damage)
    {
        Interaction.Interacted();
    }

}
