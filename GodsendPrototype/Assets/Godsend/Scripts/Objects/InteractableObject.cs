﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : BreakableObject {

    public Interactable Interaction;

    protected override void Start()
    {
        base.Start();

        if (Interaction == null)
        {
            Interaction = GetComponentInParent<Interactable>();
            if (Interaction == null)
                Interaction = GetComponentInChildren<Interactable>();
        }
    }

}
