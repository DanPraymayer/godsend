﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerPillar : BreakableObject
{

    public GameObject[] parts;
    public GameObject stoneProjectile;

    public CharacterBody ownerBody;


    protected override void Start()
    {
        base.Start();
    }

    private void OnEnable()
    {
        ResetPillar();
        ResetHealth();
    }

    public override void TakeDamage(float damage)
    {
        m_health -= 1;
        if (m_health <= 0)
        {
            if (ownerBody.CurrentController.gameObject.layer == NamingConstants.PLAYER_LAYER)
            {
                CanvasManager.CM.movesetUI.AbilityDown.SetAbilityActive(false);
            }
            Die();
        }
    }

    public override void TakeDamage(float damage, CharacterBody source)
    {
        if (source == ownerBody && source.CharacterMoveset.CurrentAttackType != CharacterMoveset.AttackType.DownAttack)
        {
            SpawnProjectile(source.CurrentController.FaceDirection, source.CurrentController.TargetLayer);
            TakeDamage(damage);
        }
    }

    public void SpawnProjectile(float direction, LayerMask targetLayer)
    {

        parts[(int)m_health - 1].SetActive(false);

        Projectile stone = Instantiate(stoneProjectile, parts[(int)m_health - 1].transform.position + Vector3.up * .2f, Quaternion.identity).GetComponent<Projectile>();

        stone.Setup(new Vector2(direction, 0));

    }

    public void SetPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    public void ResetPillar()
    {
        m_health = maxHealth;
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i].SetActive(true);
        }
        if (ownerBody != null && ownerBody.CurrentController.gameObject.layer == NamingConstants.PLAYER_LAYER)
        {
            CanvasManager.CM.movesetUI.AbilityDown.SetAbilityActive(true);
        }
    }

}
