﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{
    public Material hitMaterial;

    Material originalMaterial;
    Material objectMaterial;

    public float flashTime;
    float timer;

    bool isHit;

    // Use this for initialization
    void Start()
    {
        objectMaterial = this.GetComponent<Renderer>().material;
        originalMaterial = objectMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Renderer>().material = objectMaterial;

        if (Input.GetMouseButtonDown(0)){
            isHit = true;
        }

        if(isHit){
            objectMaterial = hitMaterial;
            timer += Time.deltaTime;
        }

        if(timer >= flashTime){
            timer = 0;
            isHit = false;
            objectMaterial = originalMaterial;
        }
    }
}

