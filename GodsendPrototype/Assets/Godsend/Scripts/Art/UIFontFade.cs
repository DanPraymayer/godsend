﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIFontFade : MonoBehaviour,IPointerEnterHandler, IPointerExitHandler{

    Button buttonReference;
    RectTransform textRec;
    Text text;
    public Color[] textColors;

    public Vector3 highlightedTextScale;
    Vector3 originalTextScale;
    Vector3 desiredScale;

    public bool scaleWithButton;

    float lerpDuration = .75f;
    float lerp;

    int state;
    bool change;

    // Use this for initialization
    void Start () {
        buttonReference = this.GetComponent<Button>();
        text = this.GetComponentInChildren<Text>();

        if (scaleWithButton){
            textRec = this.GetComponent<RectTransform>();
        }

        else{
            textRec = text.GetComponent<RectTransform>();
        }

        text.color = textColors[0];
        originalTextScale = textRec.localScale;
        state = 0;
	}
	
	// Update is called once per frame
	void Update () {

        lerp += Time.deltaTime / lerpDuration;

        if (change)
        {
            if (state == 1)
            {
                desiredScale = highlightedTextScale;
                text.color = Color.Lerp(text.color, textColors[1], Mathf.SmoothStep(0, 1, lerp));
                textRec.localScale = Vector3.Lerp(textRec.localScale, desiredScale, Mathf.SmoothStep(0,1,lerp));
            }

            else
            {
                desiredScale = originalTextScale;
                text.color = Color.Lerp(text.color, textColors[0], Mathf.SmoothStep(0, 1, lerp));
                textRec.localScale = Vector3.Lerp(textRec.localScale, desiredScale, Mathf.SmoothStep(0, 1, lerp));
            }

            if(textRec.localScale == desiredScale)
            {
                change = false;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData){
        if (!buttonReference.IsInteractable())
            return;

        change = true;
        state = 1;
        lerp = 0;
    }

    public void OnPointerExit(PointerEventData eventData){
        if (!buttonReference.IsInteractable())
            return;

        change = true;
        state = 0;
        lerp = 0;
    }
}
