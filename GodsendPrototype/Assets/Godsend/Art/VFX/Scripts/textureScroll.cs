﻿using UnityEngine;
using System.Collections;

public class textureScroll : MonoBehaviour {

    public float scrollSpeedX;
    public float scrollSpeedY;

    public Material trail;

	void FixedUpdate()
    {
        float offsetX = Time.deltaTime * scrollSpeedX;
        float offsetY = Time.deltaTime * scrollSpeedY;

       trail.mainTextureOffset = new Vector2(offsetX, offsetY);
    }
}
