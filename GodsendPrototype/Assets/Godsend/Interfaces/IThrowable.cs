﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IThrowable {

    void AddThrowForce(Vector3 force);
    void UpdatePosition(Vector3 newPosition);
    Vector3 ReturnPosition();
    void SetParentInactive();
    void SetParentActive();
}
