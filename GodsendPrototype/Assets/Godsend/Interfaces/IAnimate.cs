﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimate {

    void Move(float speed);

    void Jump(bool jumped);

    void OnGround(bool onGround);

    void NormalAttack();

    void Rotate(float angle);


}
