﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITakeDamage {

    void TakeDamage(float damage);
    void TakeDamage(float damage, CharacterBody source);
    void KnockBack(Vector2 knockBack);
    void ResetHealth();
    void Die();

}
