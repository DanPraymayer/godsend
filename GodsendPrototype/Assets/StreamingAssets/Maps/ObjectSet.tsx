<?xml version="1.0" encoding="UTF-8"?>
<tileset name="ObjectSet" tilewidth="700" tileheight="700" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="75" height="100" source="TileSets/Objects/Break.png"/>
 </tile>
 <tile id="1">
  <image width="100" height="150" source="TileSets/Objects/Grunt.png"/>
 </tile>
 <tile id="2">
  <image width="100" height="200" source="TileSets/Objects/Player.png"/>
 </tile>
 <tile id="3">
  <image width="700" height="700" source="TileSets/Objects/House.png"/>
 </tile>
</tileset>
